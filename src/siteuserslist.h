/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef SITEUSERSLIST_H
#define SITEUSERSLIST_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "contactlist.h"


class SiteUsersList : public QWidget
{
    Q_OBJECT

public:
    explicit SiteUsersList(PumpController *pumpController,
                           GlobalObject *globalObject,
                           QWidget *parent = 0);
    ~SiteUsersList();

signals:

public slots:
    void getList();
    void setListContents(QVariantList userList, int totalUsers);
    void closeList();


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_middleLayout;

    QLabel *m_explanationLabel;
    QPushButton *m_getListButton;
    QLabel *m_infoLinksLabel;

    QLabel *m_serverInfoLabel;
    QPushButton *m_closeListButton;
    ContactList *m_userList;

    PumpController *m_pumpController;
};

#endif // SITEUSERSLIST_H
