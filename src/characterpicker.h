/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CHARACTERPICKER_H
#define CHARACTERPICKER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <KCharSelect>
#include <QTableView>
#include <QPushButton>
#include <QToolButton>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QAction>
#include <QCloseEvent>
#include <QSettings>

#include <QDebug>


class CharacterPicker : public QWidget
{
    Q_OBJECT
public:
    explicit CharacterPicker(QWidget *parent = nullptr);
    ~CharacterPicker();


signals:
    void characterSelected(QString character);


public slots:
    void onCodePointSelected();
    void onRecentButtonClicked();


protected:
    virtual void closeEvent(QCloseEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    KCharSelect *m_charSelect;
    QTableView *m_charSelectTableView;

    QHBoxLayout *m_recentLayout;
    QCheckBox *m_makeBigCheckbox;
    QList<QToolButton *> m_recentButtons;

    QVariantList m_recentCodepoints;

    QAction *m_closeAction;
    QDialogButtonBox *m_buttonBox;
    QPushButton *m_insertButton;
    QPushButton *m_cancelButton;
};

#endif // CHARACTERPICKER_H
