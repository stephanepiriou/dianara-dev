/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef LOGVIEWER_H
#define LOGVIEWER_H

#include <QWidget>
#include <QIcon>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextBrowser>
#include <QPushButton>
#include <QTime>
#include <QAction>
#include <QCloseEvent>
#include <QHideEvent>
#include <QSettings>

#include <QDebug>

#include "mischelpers.h"


class LogViewer : public QWidget
{
    Q_OBJECT

public:
    explicit LogViewer(QWidget *parent = 0);
    ~LogViewer();


signals:


public slots:
    void addToLog(QString message, QString url="");
    void toggleVisibility();


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent(QHideEvent *event);
    virtual void showEvent(QShowEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QTextBrowser *m_logTextBrowser;

    QHBoxLayout *m_buttonsLayout;
    QPushButton *m_clearButton;
    QPushButton *m_closeButton;

    QAction *m_closeAction;
};

#endif // LOGVIEWER_H
