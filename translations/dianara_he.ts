<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he" sourcelanguage="en">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="89"/>
        <location filename="../src/asactivity.cpp" line="135"/>
        <source>Public</source>
        <translation>ציבורי</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="422"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 מאת %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="275"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translatorcomment>איגרת</translatorcomment>
        <translation>מברק</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="280"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>מאמר</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="285"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>תמונה</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="290"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>אודיו</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="295"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>וידאו</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="300"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>קובץ</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="305"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>תגובה</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="310"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>קבוצה</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="315"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>אוסף</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="320"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>אחר</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="376"/>
        <source>No detailed location</source>
        <translation>אין מיקום מפורט</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="402"/>
        <source>Deleted on %1</source>
        <translation>נמחק ב%1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="578"/>
        <location filename="../src/asobject.cpp" line="648"/>
        <source>and one other</source>
        <translation>ועוד אחד נוסף</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="582"/>
        <location filename="../src/asobject.cpp" line="652"/>
        <source>and %1 others</source>
        <translation>ועוד %1 אחרים</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="173"/>
        <source>Hometown</source>
        <translation>עיר מגורים</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>תצורת חשבון</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="50"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>ראשית, הזן מזהה Webfinger, כתובת pump.io שלך.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="53"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>כתובתך נראת כמו username@pumpserver.org, ובאפשרותך למצוא אותה בתוך הפרופיל שלך, בממשק רשת.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="58"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>אם הפרופיל של נמצא בכתובת https://pump.example/yourname, אזי כתובתך היא yourname@pump.example</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="62"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>אם אין לך חשבון עדיין, באפשרותך לרשום אחד אצל %1. קישור זה יוביל אותך לשרת פומבי אקראי.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="70"/>
        <source>If you need help: %1</source>
        <translation>אם אתה צריך עזרה: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="73"/>
        <source>Pump.io User Guide</source>
        <translation>מדריך משתמש Pump.io</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="85"/>
        <source>Your Pump.io address:</source>
        <translation>כתובת Pump.io שלך:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="88"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation>כתובתך, כמו username@pumpserver.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="131"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation>הזן או הדבק קוד מאמת המסופק על ידי שרת Pump שלך כאן</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="95"/>
        <source>Get &amp;Verifier Code</source>
        <translation>השג קוד &amp;מאמת</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="98"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>לאחר לחיצה על לחצן זה, דפדפן רשת יפתח, תוך כדי בקשה לאימות עבור Dianara</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="109"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>ברגע שהסמכת את Dianara מתוך ממשק רשת של שרת Pump, אתה תקבל קוד אשר נקרא VERIFIER.
העתק והדבק אותו לתוך השדה מטה.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="128"/>
        <source>Verifier code:</source>
        <translation>קוד מאמת:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="141"/>
        <source>&amp;Authorize Application</source>
        <translation>&amp;התר יישום</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="168"/>
        <source>&amp;Save Details</source>
        <translation>&amp;שמור פרטים</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="176"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="188"/>
        <source>Your account is properly configured.</source>
        <translation>חשבונך מוגדר כראוי.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="192"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>הקש בטל נעילה אם ברצונך להגדיר חשבון אחר.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="200"/>
        <source>&amp;Unlock</source>
        <translation>בטל &amp;נעילה</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="327"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>עכשיו יעלה דפדפן רשת, כעת תהא באפשרותך להשיג קוד מאמת</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="336"/>
        <source>Your Pump address is invalid</source>
        <translation>כתובת Pump הינה שגויה</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="355"/>
        <source>Verifier code is empty</source>
        <translation>קוד מאמת הינו ריק</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="382"/>
        <source>Dianara is authorized to access your data</source>
        <translation>‏Dianara הינה מורשית לגשת למידע שלך</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="401"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>אם הדפדפן לא נפתח אוטומטית, העתק את כתובת זו ידנית</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="410"/>
        <source>Unable to open web browser!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="33"/>
        <source>&apos;To&apos; List</source>
        <translation>רשימת &apos;לכבוד&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="37"/>
        <source>&apos;Cc&apos; List</source>
        <translation>רשימת &apos;העתק&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="58"/>
        <source>&amp;Add to Selected</source>
        <translation>הוסף ל&amp;נבחרים</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="73"/>
        <source>All Contacts</source>
        <translation>אנשי קשר</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="79"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <comment>ON THE LEFT should change to ON THE RIGHT in RTL languages</comment>
        <translation>בחר אנשים מתוך הרשימה שמימינך.
באפשרותך לגרור אותם בעזרת העכבר, לחץ עליהם פעם או פעמיים (לחיצה-כפולה), או בחר אותם והשתמש בלחצן מטה.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="96"/>
        <source>Clear &amp;List</source>
        <translation>טהר &amp;רשימה</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="120"/>
        <source>&amp;Done</source>
        <translation>&amp;סיים</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="127"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="168"/>
        <location filename="../src/audienceselector.cpp" line="386"/>
        <source>Public</source>
        <translation type="unfinished">ציבור</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="173"/>
        <location filename="../src/audienceselector.cpp" line="392"/>
        <source>Followers</source>
        <translation type="unfinished">עוקבים</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="178"/>
        <source>Lists</source>
        <translation type="unfinished">רשימות</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="188"/>
        <source>People...</source>
        <translation type="unfinished">אנשים...</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="108"/>
        <source>Selected People</source>
        <translation>אנשים נבחרים</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="143"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>פתח את הפרופיל של %1 בתוך דפדפן רשת</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="139"/>
        <source>Open your profile in web browser</source>
        <translation>פתח את הפרופיל שלי בתוך דפדפן רשת</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="165"/>
        <source>Send message to %1</source>
        <translatorcomment>לכבוד</translatorcomment>
        <translation>שלח הודעה אל %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="174"/>
        <source>Browse messages</source>
        <translation>עיין בהודעות</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="230"/>
        <source>Stop following</source>
        <translation>הפסק לעקוב</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="241"/>
        <source>Follow</source>
        <translation>עקוב</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="287"/>
        <source>Stop following?</source>
        <translation>להפסיק לעקוב?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="288"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>האם אתה בטוח כי ברצונך להפסיק לעקוב אחר %1?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="291"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;כן, הפסק לעקוב</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="292"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
</context>
<context>
    <name>BannerNotification</name>
    <message>
        <location filename="../src/bannernotification.cpp" line="38"/>
        <source>Timelines were not automatically updated to avoid interruptions.</source>
        <translation>צרי זמן לא עודכנו אוטומטית כדי להימנע מהפרעות.</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="47"/>
        <source>This happens when it is time to autoupdate the timelines, but you are not at the top of the first page, to avoid interruptions while you read</source>
        <translation>זה מתרחש כאשר זהו זמן לעדכן את צירי הזמן, אולם אינך בראש העמוד הראשון, כדי להימנע מן הפרעות במהלך קריאה</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="55"/>
        <source>Update now</source>
        <translation>עדכן עכשיו</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="65"/>
        <source>Hide this message</source>
        <translation>הסתר את הודעה זו</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="35"/>
        <source>Change...</source>
        <translation>התאם...</translation>
    </message>
    <message>
        <location filename="../src/colorpicker.cpp" line="114"/>
        <source>Choose a color</source>
        <translation>בחירת צבע</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="251"/>
        <source>Posted on %1</source>
        <translation>פורסם ב%1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="256"/>
        <source>Modified on %1</source>
        <translation>שונה ב%1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="114"/>
        <source>Like or unlike this comment</source>
        <translation>סמן או בטל סימון תגובה זו</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="121"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>צטט</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="129"/>
        <source>Reply quoting this comment</source>
        <translation>השב תוך ציטוט תגובה זו</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="136"/>
        <source>Edit</source>
        <translation>ערוך</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="142"/>
        <source>Modify this comment</source>
        <translation>שנה את תגובה זו</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="148"/>
        <source>Delete</source>
        <translation>מחק</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="154"/>
        <source>Erase this comment</source>
        <translation>מחק את תגובה זו</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="287"/>
        <source>Unlike</source>
        <translation>בטל סימון</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="291"/>
        <source>Like</source>
        <translation>אהבתי</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="305"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>%1 אוהב(ת) את תגובה זו</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="311"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>%1 אוהבים את תגובה זו</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="547"/>
        <source>WARNING: Delete comment?</source>
        <translation>ﬡזﬣﬧﬣ: למחוק תגובה?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="548"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>האם אתה בטוח כי ברצונך למחוק את תגובה זו?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;כן, מחק זאת</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="42"/>
        <source>Reload comments</source>
        <translation>טען מחדש תגובות</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="123"/>
        <location filename="../src/commenterblock.cpp" line="510"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>הגב</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="127"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>באפשרותך ללחוץ Control+Enter כדי לשלוח את התגובה בעזרת המקלדת</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="134"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="137"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>הקש ESC כדי לבטל את התגובה אם אין שום טקסט</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="363"/>
        <source>Check for comments</source>
        <translation>בדוק אם ישנן תגובות חדשות</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="367"/>
        <source>Show all %1 comments</source>
        <translation>הצג את כל %1 התגובות</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="393"/>
        <source>Comments are not available</source>
        <translation>תגובות אינן זמינות</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="558"/>
        <source>Error: Already composing</source>
        <translation>שגיאה: כבר מלחין</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="559"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>אין באפשרותך לערוך תגובה בזמן זה, מכיוון שתגובה אחרת כבר מצויה בשלבי כתיבה.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="574"/>
        <source>Editing comment</source>
        <translation>עריכת תגובה</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="583"/>
        <source>Loading comments...</source>
        <translation>כעת טוען תגובות...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="650"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>פרסום תגובה נכשל.

נסה שוב.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="667"/>
        <source>An error occurred</source>
        <translation>אירעה שגיאה</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="691"/>
        <source>Sending comment...</source>
        <translation>כעת שולח תגובה...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="696"/>
        <source>Updating comment...</source>
        <translation>כעת מעדכן תגובה...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="705"/>
        <source>Comment is empty.</source>
        <translation>תגובה הינה ריקה.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="33"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>הקש כאן או לחץ Control+N כדי לפרסם מברק...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="37"/>
        <source>Symbols</source>
        <translation>סמלים</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="55"/>
        <source>Formatting</source>
        <translation>עיצוב</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="56"/>
        <source>Normal</source>
        <translation>רגיל</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Bold</source>
        <translation>בולט</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="65"/>
        <source>Italic</source>
        <translation>נטוי</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="70"/>
        <source>Underline</source>
        <translation>קו תחתון</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="75"/>
        <source>Strikethrough</source>
        <translation>קו חוצה</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="82"/>
        <source>Header</source>
        <translation>תקורה</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="87"/>
        <source>List</source>
        <translation>רשימה</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="91"/>
        <source>Table</source>
        <translation>טבלה</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="95"/>
        <source>Preformatted block</source>
        <translation>קטע טקסט מעוצב מראש</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="99"/>
        <source>Quote block</source>
        <translation>קטע ציטוט</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="107"/>
        <source>Make a link</source>
        <translation>הפוך לקישור</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="112"/>
        <source>Insert an image from a web site</source>
        <translation>הכנס תמונה מתוך אתר רשת</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="117"/>
        <source>Insert line</source>
        <translation>הכנס שורה</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="129"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;עיצוב</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="134"/>
        <source>Text Formatting Options</source>
        <translation>אפשרויות עיצוב טקסט</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="139"/>
        <source>Paste Text Without Formatting</source>
        <translation>הדבק טקסט בלי עיצוב</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="175"/>
        <source>Type a message here to post it</source>
        <translation>הקלד הודעה כאן כדי לפרסמה</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="181"/>
        <source>Type a comment here</source>
        <translation>הקלד תגובה כאן</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="342"/>
        <source>You can attach only one file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="350"/>
        <source>You cannot drop folders here, only a single file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="500"/>
        <source>Insert as image?</source>
        <translation>להכניס בתור תמונה?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="501"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>נראה כי הקישור אותו אתה מדביק מצביע לתמונה.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="504"/>
        <source>Insert as visible image</source>
        <translation>הכנס בתור תמונה נראית</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="505"/>
        <source>Insert as link</source>
        <translation>הכנס בתור קישור</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="734"/>
        <source>Table Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="738"/>
        <source>How many rows (height)?</source>
        <translation>כמה שורות (גובה)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="749"/>
        <source>How many columns (width)?</source>
        <translation>כמה טורים (רוחב)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="832"/>
        <source>Insert a link</source>
        <translation>הכנס קישור</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="833"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>הקלד או הדבק כתובת רשת כאן.
באפשרותך גם לבחור קטע טקסט תחילה, כדי להפוך אותו לקישור.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="848"/>
        <source>Make a link from selected text</source>
        <translation>הכן קישור מתוך טקסט נבחר</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="849"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>הקלד או הדבק כתובת רשת כאן.
הטקסט הנבחר (%1) יומר לקישור.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="878"/>
        <source>Invalid link</source>
        <translation>קישור לא תקין</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="879"/>
        <source>The text you entered does not look like a link.</source>
        <translation>הטקסט אשר הזנת לא נראה כמו קישור.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="882"/>
        <source>It should start with one of these types:</source>
        <comment>It = the link, from previous sentence</comment>
        <translation>עליו להתחיל עם אחד מטיפוסים אלו:</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="896"/>
        <source>&amp;Use it anyway</source>
        <translation>ה&amp;שתמש בזה בכל זאת</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="897"/>
        <source>&amp;Enter it again</source>
        <translation>ה&amp;זן אותו שוב</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="898"/>
        <source>&amp;Cancel link</source>
        <translation>&amp;בטל קישור</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="931"/>
        <source>Insert an image from a URL</source>
        <translation>הכנס תמונה מתוך URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="932"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>הקלד או הדבק כתובת תמונה כאן.
על הקישור להצביע ישירות על קובץ תמונה.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="949"/>
        <source>Error: Invalid URL</source>
        <translation>שגיאה: URL לא כשר</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="950"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation>הכתובת אשר הזנת (%1) אינה תקינה.
כתובות תמונה צריכות להתחיל עם http://‎ או https://‎</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1052"/>
        <source>Yes, but saving a &amp;draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1057"/>
        <source>Cancel message?</source>
        <translation>לבטל הודעה?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1058"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation>האם אתה בטוח כי ברצונך לבטל את הודעה זו?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1060"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;כן, בטל זאת</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1062"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>תצורת תוכנית</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="245"/>
        <source>minutes</source>
        <translation>דקות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="289"/>
        <source>Timeline &amp;update interval</source>
        <translation>תדירות &amp;עדכון ציר זמן</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="255"/>
        <source>Left side</source>
        <comment>tabs on left side/west; RTL not affected</comment>
        <translation>צד שמאל</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="258"/>
        <source>Right side</source>
        <comment>tabs on right side/east; RTL not affected</comment>
        <translation>צד ימין</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="322"/>
        <source>Minor Feeds</source>
        <translation>ערוצים שוליים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="475"/>
        <source>Show snippets in minor feeds</source>
        <translation>הצג קטעים בתוך ערוצים שוליים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="515"/>
        <source>Only for images inserted from web sites.</source>
        <translation>רק לתמונות מוכנסות מתוך אתר רשת.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="518"/>
        <source>Use with care.</source>
        <translation>יש לעשות שימוש זהיר.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="566"/>
        <source>Public posts as &amp;default</source>
        <translation>פוסטים פומביים כ&amp;שגרה</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Top</source>
        <translation>עליון</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="253"/>
        <source>Bottom</source>
        <translation>תחתון</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="292"/>
        <source>&amp;Tabs position</source>
        <translation>מיקום &amp;כרטיסיות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="294"/>
        <source>&amp;Movable tabs</source>
        <translation>כרטיסיות ניתנות לה&amp;זזה</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="268"/>
        <source>Pro&amp;xy Settings</source>
        <translation>הגדרות &amp;Proxy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="297"/>
        <source>Network configuration</source>
        <translation>תצורת רשת</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="275"/>
        <source>Set Up F&amp;ilters</source>
        <translation>הגדר &amp;מסננים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="299"/>
        <source>Filtering rules</source>
        <translation>כללי סינון</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="313"/>
        <source>Post Titles</source>
        <translation>כותרות פוסט</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="316"/>
        <source>Post Contents</source>
        <translation>תגובות פוסט</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="319"/>
        <source>Comments</source>
        <translation>תגובות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="347"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>אתה הוא אחד מבין הנמענים של הפעילות, כגון תגובה אשר מוענה אליך.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="351"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation>בשימוש גם בהדגשת פוסטים אשר מכוונים אליך בצירי זמן.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="356"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>הפעילות מצויה בתוך מענה למשהו אשר נעשה על ידיך, כגון תגובה אשר פורסמה בתשובה לאחד מן המברקים שלך.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="362"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation>אתה הוא היעד של הפעילות, כמו מישהו שמוסיף אותך לרשימה.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="367"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>הפעילות הינה קשורה לאחד מהיעדים שלך, כמו מישהו שאוהב את אחד הפוסטים שלך.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="371"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>בשימוש גם בהדגשת הפוסטים שלך בצירי זמן.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="376"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>פריט מודגש בשל כללי סינון.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="381"/>
        <source>Item is new.</source>
        <translation>פריט הינו חדש.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="464"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>&amp;פוסטים לכל עמוד, ציר זמן ראשי</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="407"/>
        <location filename="../src/configdialog.cpp" line="414"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>פוסטים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="466"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>פוסטים לכל עמוד, &amp;צירי זמן אחרים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="430"/>
        <source>Highlighted activities, except mine</source>
        <translation>פעילויות מודגשות, למעט שלי</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="431"/>
        <source>Any highlighted activity</source>
        <translation>כל פעילות מודגשת</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="432"/>
        <source>Always</source>
        <translation>תמיד</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="433"/>
        <source>Never</source>
        <translation>אף פעם</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="438"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>תווים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="477"/>
        <source>Snippet limit</source>
        <translation>מגבלת קטע</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="468"/>
        <source>Show information for deleted posts</source>
        <translation>הצג מידע עבור פוסטים מחוקים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="451"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="452"/>
        <source>Before avatar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="453"/>
        <source>Before avatar, subtle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="454"/>
        <source>After avatar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="455"/>
        <source>After avatar, subtle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="470"/>
        <source>Hide duplicated posts</source>
        <translation>הסתר פוסטים כפולים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="472"/>
        <source>Jump to new posts line on update</source>
        <translation>קפוץ לשורת פוסטים חדשה בעדכון</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="479"/>
        <source>Snippet limit when highlighted</source>
        <translation>מגבלת קטע כאשר מודגש</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="481"/>
        <source>Minor feed avatar sizes</source>
        <translation>מידות אווטאר ערוץ שולי</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="483"/>
        <source>Show activity icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="527"/>
        <source>Avatar size</source>
        <translation>מידת אווטאר</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="529"/>
        <source>Avatar size in comments</source>
        <translation>מידת אווטאר בתוך תגובות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="531"/>
        <source>Show extended share information</source>
        <translation>הצג מידע שיתוף נוסף</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="533"/>
        <source>Show extra information</source>
        <translation>הצג מידע נוסף</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="535"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>הדגש תגובות של מחבר פוסט</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="537"/>
        <source>Highlight your own comments</source>
        <translation>הדגש את התגובות שלי</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="539"/>
        <source>Ignore SSL errors in images</source>
        <translation>התעלם משגיאות SSL בתמונות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="541"/>
        <source>Show full size images</source>
        <translation>הצג תמונות אווטאר בגודל מלא</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="568"/>
        <source>Use attachment filename as initial post title</source>
        <translation>השתמש בשם קובץ מצורף בתור כותרת פוסט פותחת</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="571"/>
        <source>Show character counter</source>
        <translation>הצג מונה תווים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="596"/>
        <source>Don&apos;t inform followers when following someone</source>
        <translation>אל תיידע עוקבים בעת מעקב אחר מישהו</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="599"/>
        <source>Don&apos;t inform followers when handling lists</source>
        <translation>אל תיידע עוקבים בעת התעסקות ברשימות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="602"/>
        <source>Inform only the author when liking things</source>
        <translatorcomment>help refine this translation</translatorcomment>
        <translation type="unfinished">תיידע רק את המחבר כאשר אני מסמן דברים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="619"/>
        <source>As system notifications</source>
        <translation>בתור התראות מערכת</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="621"/>
        <source>Using own notifications</source>
        <translation>באמצעות התראות עצמיות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="623"/>
        <source>Don&apos;t show notifications</source>
        <translation>אל תציג התראות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="637"/>
        <source>seconds</source>
        <comment>Next to a duration, in seconds</comment>
        <translation>שניות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="675"/>
        <source>Notification Style</source>
        <translation>סגנון התראה</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="679"/>
        <source>Duration</source>
        <translation>משך</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="681"/>
        <source>Persistent Notifications</source>
        <translation type="unfinished">התראות נמשכות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="683"/>
        <source>Also highlight taskbar entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="689"/>
        <source>Notify when receiving:</source>
        <translation>התרע בעת קבלה:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="693"/>
        <source>New posts</source>
        <translation>פוסטים חדשים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="695"/>
        <source>Highlighted posts</source>
        <translation>פוסטים מודגשים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="697"/>
        <source>New activities in minor feed</source>
        <translation>פעילויות חדשות בתוך ערוץ שולי</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="699"/>
        <source>Highlighted activities in minor feed</source>
        <translation>הדגש פעילויות בתוך ערוץ שולי</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="701"/>
        <source>Important errors</source>
        <translation>שגיאות חשובות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="716"/>
        <source>Default</source>
        <translation>שגרתי</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="717"/>
        <source>System iconset, if available</source>
        <translation>מערך סמלי מערכת, אם זמין</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="718"/>
        <source>Show your current avatar</source>
        <translation>הצג את האווטאר הנוכחי שלך</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="719"/>
        <source>Custom icon</source>
        <translation>סמל מותאם</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="753"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>&amp;טיפוס סמל מגש מערכת</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="726"/>
        <source>S&amp;elect...</source>
        <translation>&amp;בחר...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="755"/>
        <source>Custom &amp;Icon</source>
        <translation>סמל &amp;מותאם</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="757"/>
        <source>Hide window on startup</source>
        <translation>הסתר חלון בעת הפעלה</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="113"/>
        <source>General Options</source>
        <translation>אפשרויות כלליות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="117"/>
        <source>Fonts</source>
        <translation>גופנים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="121"/>
        <source>Colors</source>
        <translation>צבעים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="125"/>
        <source>Timelines</source>
        <translation>צירי זמן</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="129"/>
        <source>Posts</source>
        <translation>פוסטים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="133"/>
        <source>Composer</source>
        <translation>מלחין</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="137"/>
        <source>Privacy</source>
        <translation>פרטיות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="141"/>
        <source>Notifications</source>
        <translation>התראות</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="145"/>
        <source>System Tray</source>
        <translation>מגש מערכת</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="175"/>
        <source>Dianara stores data in this folder:</source>
        <translation>‏Dianara מאחסנת מידע בתיקייה זו: ‎</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="186"/>
        <source>&amp;Save Configuration</source>
        <translation>&amp;שמור תצורה</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="193"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="814"/>
        <source>This is a system notification</source>
        <translation>זוהי התראת מערכת</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="818"/>
        <source>System notifications are not available!</source>
        <translation>התראות מערכת אינן זמינות!</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="820"/>
        <source>Own notifications will be used.</source>
        <translation>התראות עצמיות ייכנסו לשימוש.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="830"/>
        <source>This is a basic notification</source>
        <translation>זוהי התראה בסיסית</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1027"/>
        <source>Select custom icon</source>
        <translation>בחירת סמל מותאם</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1029"/>
        <source>Image files</source>
        <translation>קבצי תמונה</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1032"/>
        <source>All files</source>
        <translation>כל הקבצים</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1052"/>
        <source>Invalid image</source>
        <translation>תמונה לא כשרה</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1053"/>
        <source>The selected image is not valid.</source>
        <translation>התמונה הנבחרת אינה תקינה.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="71"/>
        <source>Hometown</source>
        <translation>עיר מגורים</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="78"/>
        <source>Joined: %1</source>
        <translation>הצטרף: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="86"/>
        <source>Updated: %1</source>
        <translation>עודכן: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="106"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>ביו של %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="120"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>למשתמש זה אין ביוגרפיה</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="125"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>אין ביוגרפיה עבור %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="154"/>
        <source>Open Profile in Web Browser</source>
        <translation>פתח פרופיל בתוך דפדפן רשת</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="165"/>
        <source>Send Message</source>
        <translation>שלח הודעה</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="173"/>
        <source>Browse Messages</source>
        <translation>עיין בהודעות</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="197"/>
        <source>User Options</source>
        <translation>אפשרויות משתמש</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="230"/>
        <source>Follow</source>
        <translation>עקוב</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="242"/>
        <source>Stop Following</source>
        <translation>הפסק לעקוב</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="306"/>
        <source>Stop following?</source>
        <translation>להפסיק לעקוב?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="307"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>האם אתה בטוח כי ברצונך להפסיק לעקוב אחר %1?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="310"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;כן, הפסק לעקוב</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="311"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="36"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>הקלד שם או מזהה חלקי כדי למצוא איש קשר...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="67"/>
        <source>F&amp;ull List</source>
        <translation>&amp;רשימה מלאה</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="54"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>username@server.org או https://server.org/username</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="58"/>
        <source>&amp;Enter address to follow:</source>
        <translation>ה&amp;זן כתובת למעקב:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="79"/>
        <source>&amp;Follow</source>
        <translation>&amp;עקוב</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="129"/>
        <source>Reload Followers</source>
        <translation>טען מחדש עוקבים</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="134"/>
        <source>Reload Following</source>
        <translation type="unfinished">טען מחדש עוקב</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="140"/>
        <source>Export Followers</source>
        <translation>יצא עוקבים</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="145"/>
        <source>Export Following</source>
        <translation type="unfinished">יצא עוקב</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="151"/>
        <source>Reload Lists</source>
        <translation>טען מחדש רשימות</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="179"/>
        <source>&amp;Neighbors</source>
        <translation>&amp;שכנים</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="274"/>
        <source>Cannot export to this file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="278"/>
        <source>Please enter another file name, or choose a different folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="519"/>
        <source>The server seems to be a Pump server, but the account does not exist.</source>
        <translation>נראה כי השרת הינו שרת Pump, אולם החשבון לא קיים.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="524"/>
        <source>%1 doesn&apos;t seem to be a Pump server.</source>
        <comment>%1 is a hostname</comment>
        <translation>‏%1 לא נראה כמו שרת Pump.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="534"/>
        <source>Following this account at this time will probably not work.</source>
        <translation>מעקב אחר חשבון זה בעת זו כנראה לא יעבוד.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="543"/>
        <source>The %1 server seems unavailable.</source>
        <comment>%1 is a hostname</comment>
        <translation>נראה כי השרת %1 לא זמין.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="551"/>
        <source>Unknown</source>
        <comment>Refers to server version</comment>
        <translation>לא ידועה</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="273"/>
        <location filename="../src/contactmanager.cpp" line="556"/>
        <source>Error</source>
        <translation>שגיאה</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="558"/>
        <source>The user address %1 does not exist, or the %2 server is down.</source>
        <translation>כתובת משתמש %1 לא קיימת, או שהשרת %2 הינו מושבת.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="566"/>
        <source>Server version</source>
        <translation>גרסת שרת</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="570"/>
        <source>Check the address, and keep in mind that usernames are case-sensitive.</source>
        <translation>בדוק את הכתובת, וזכור כי שמות משתמש רגישים להבדל בין אותיות גדולות וקטנות.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="573"/>
        <source>Do you want to try following this address anyway?</source>
        <translation>האם ברצונך לנסות לעקוב אחר כתובת זו בכל זאת?</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="575"/>
        <source>(not recommended)</source>
        <translation>(לא מומלץ)</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="578"/>
        <source>Yes, follow anyway</source>
        <translation>כן, עקוב בכל זאת</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="579"/>
        <source>No, cancel</source>
        <translation>לא, בטל</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="598"/>
        <source>About to follow %1...</source>
        <translation>כעת עומד לעקוב אחר %1...</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="208"/>
        <source>Follo&amp;wers</source>
        <translation>עו&amp;קבים</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="213"/>
        <source>Followin&amp;g</source>
        <translation>עוק&amp;ב</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="218"/>
        <source>&amp;Lists</source>
        <translation>&amp;רשימות</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="233"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation type="unfinished">יצא רשימה של &apos;עוקב&apos; לקובץ</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="234"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>יצא רשימה של &apos;עוקבים&apos; לקובץ</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Open</source>
        <comment>Verb, as in: Open the downloaded file</comment>
        <translation>פתח</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="58"/>
        <source>Download</source>
        <translation>הורד</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="61"/>
        <source>Save the attached file to your folders</source>
        <translation>שמור אב הקובץ המצורף לתיקייה שלך</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="74"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="144"/>
        <source>Save File As...</source>
        <translation>שמור קובץ בתור...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="147"/>
        <source>All files</source>
        <translation>כל הקבצים</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="198"/>
        <source>File not found!</source>
        <translation>קובץ לא נמצא!</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="209"/>
        <source>Abort download?</source>
        <translation>לנטוש הורדה?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="210"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>האם ברצונך להפסיק להוריד את הקובץ המצורף?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="213"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;כן, עצור</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="214"/>
        <source>&amp;No, continue</source>
        <translation>&amp;לא, המשך</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="222"/>
        <source>Download aborted</source>
        <translation>הורדה בוטלה</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="240"/>
        <source>Download completed</source>
        <translation>הורדה הושלמה</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="244"/>
        <source>Attachment downloaded successfully to %1</source>
        <comment>%1 = filename</comment>
        <translation>תצריף הורד בהצלחה אל %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="250"/>
        <source>Open the downloaded attachment with your system&apos;s default program for this type of file.</source>
        <translatorcomment>Jan: I re-imported the previous translation for the similar sentence; might need checking</translatorcomment>
        <translation type="unfinished">פתח את התצריף המורד באמצעות התוכנית השגרתית של המערכת שלך עבור טיפוס קובץ זה.</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="265"/>
        <source>Download failed</source>
        <translation>הורדה נכשלה</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="272"/>
        <source>Downloading attachment failed: %1</source>
        <comment>%1 = filename</comment>
        <translation>הורדת תצריף נכשלה: %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="305"/>
        <source>Downloading %1 KiB...</source>
        <translation>כעת מוריד %1 קי״ב...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="308"/>
        <source>%1 KiB downloaded</source>
        <translation>%1 קי״ב הורדו</translation>
    </message>
</context>
<context>
    <name>DraftsManager</name>
    <message>
        <location filename="../src/draftsmanager.cpp" line="28"/>
        <source>Draft Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="44"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="53"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="62"/>
        <source>Manage drafts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="87"/>
        <source>&amp;Delete selected draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="94"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;סגור</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="311"/>
        <source>Untitled draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="328"/>
        <source>Delete draft?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="329"/>
        <source>Are you sure you want to delete this draft?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;Yes, delete it</source>
        <translation type="unfinished">&amp;כן, מחק זאת</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;No</source>
        <translation type="unfinished">&amp;לא</translation>
    </message>
</context>
<context>
    <name>EmailChanger</name>
    <message>
        <location filename="../src/emailchanger.cpp" line="28"/>
        <source>Change E-mail Address</source>
        <translation>החלף כתובת דוא״ל</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="68"/>
        <source>Change</source>
        <translation>החלף</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="76"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="97"/>
        <source>E-mail Address:</source>
        <translation>כתובת דוא״ל:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="98"/>
        <source>Again:</source>
        <translation>שוב:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="99"/>
        <source>Your Password:</source>
        <translation>סיסמתך:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="150"/>
        <source>E-mail addresses don&apos;t match!</source>
        <translation>כתובות דוא״ל לא תואמות!</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="159"/>
        <source>Password is empty!</source>
        <translation>סיסמה הינה ריקה!</translation>
    </message>
</context>
<context>
    <name>FDNotifications</name>
    <message>
        <location filename="../src/notifications.cpp" line="209"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>עריכת מסננים</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="44"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1 כאשר %2 מכיל: %3</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="51"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translatorcomment>BUG: stuff &gt; things</translatorcomment>
        <translation>כאן באפשרותך לקבוע מספר כללים לצורך הסתרת או הדגשת דברים. באפשרותך לסנן לפי תוכן, מחבר או יישום.

למשל, באפשרותך לסנן הודעות אשר פורסמו באמצעות היישום Open Farm Game, או אשר מכילות את הצירוף NSFW בתוך הודעה. באפשרותך גם להדגיש הודעות אשר מכילות את שמך.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Hide</source>
        <translation>הסתר</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Highlight</source>
        <translation>הדגש</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Post Contents</source>
        <translatorcomment>translated into singular form due to string contains</translatorcomment>
        <translation>תוכן פוסט</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Author ID</source>
        <translation>מזהה מחבר</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="84"/>
        <source>Application</source>
        <translation>יישום</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="87"/>
        <source>Activity Description</source>
        <translation>תיאור פעילות</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="90"/>
        <source>Keywords...</source>
        <translation>מילות מפתח...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="95"/>
        <source>&amp;Add Filter</source>
        <translation>הוס&amp;ף מסנן</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="108"/>
        <source>Filters in use</source>
        <translation>מסננים בשימוש</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="114"/>
        <source>&amp;Remove Selected Filter</source>
        <translation>הס&amp;ר מסנן נבחר</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="125"/>
        <source>&amp;Save Filters</source>
        <translation>&amp;שמור מסננים</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="132"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="153"/>
        <source>if</source>
        <translation>כאשר</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="157"/>
        <source>contains</source>
        <translation>מכיל</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="163"/>
        <source>&amp;New Filter</source>
        <translation>מסנן &amp;חדש</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="170"/>
        <source>C&amp;urrent Filters</source>
        <translation>מסננים &amp;נוכחיים</translation>
    </message>
</context>
<context>
    <name>FilterMatchesWidget</name>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="37"/>
        <source>Content</source>
        <comment>The contents of the post matched</comment>
        <translation>תוכן</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="45"/>
        <source>Author</source>
        <translation>מחבר</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="52"/>
        <source>App</source>
        <comment>Application, short if possible</comment>
        <translation>אפליקציה</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="60"/>
        <source>Description</source>
        <translation>תיאור</translation>
    </message>
</context>
<context>
    <name>FirstRunWizard</name>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="30"/>
        <source>Welcome Wizard</source>
        <translation>אשף התחלה</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="41"/>
        <source>Welcome to Dianara!</source>
        <translation>ברוך בואך אל Dianara!</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="44"/>
        <source>This wizard will help you get started.</source>
        <translation>אשף זה יעזור לך להתחיל.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="47"/>
        <source>You can access this window again at any time from the Help menu.</source>
        <translation>תוכל לגשת לחלון זה בכל עת מתוך התפריט עזרה.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="50"/>
        <source>The first step is setting up your account, by using the following button:</source>
        <translation>הצעד הראשון הוא להתקין את חשבונך, באמצעות הכפתור הבא:</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="61"/>
        <source>Configure your &amp;account</source>
        <translation>הגדר את ה&amp;חשבון שלי</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="69"/>
        <source>Once you have configured your account, it&apos;s recommended that you edit your profile and add an avatar and some other information, if you haven&apos;t done so already.</source>
        <translation>ברגע שהגדרת את חשבונך, רצוי כי תערוך את הפרופיל שלך ותוסיף אווטאר ועוד נתונים אחרים, אם לא עשית זאת כבר.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="80"/>
        <source>&amp;Edit your profile</source>
        <translation>ערוך את ה&amp;פרופיל שלי</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="89"/>
        <source>By default, Dianara will post only to your followers, but it&apos;s recommended that you post to Public, at least sometimes.</source>
        <translation>באופן שגרתי, Dianara תפרסם רק לעוקבים שלך, אולם רצוי לפרסם לציבור, לפחות לעתים.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="97"/>
        <source>Post to &amp;Public by default</source>
        <translation>פרסם ל&amp;ציבור באופן שגרתי</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="106"/>
        <source>Open general program &amp;help window</source>
        <translation>פתח חלון &amp;עזרה כללית</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="114"/>
        <source>&amp;Show this again next time Dianara starts</source>
        <translation>&amp;הצג זאת שוב בפעם הבאה כאשר Dianara תתחיל</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="120"/>
        <source>&amp;Close</source>
        <translation>&amp;סגור</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change...</source>
        <translation>התאם...</translation>
    </message>
    <message>
        <location filename="../src/fontpicker.cpp" line="97"/>
        <source>Choose a font</source>
        <translation>בחירת גופן</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>עזרה בסיסית</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translatorcomment>מקום להתחלה</translatorcomment>
        <translation>התחלה</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>הגדרות</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>צירי זמן</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>פרסום</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>ניהול אנשי קשר</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>בקרי מקלדת</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>אפשרויות שורת פקודה</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>תכנים</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="73"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation>בפעם הראשונה אשר בה תתחילו את Dianara, אתם צריכים לראות את דו שיח תצורת חשבון. שם, הזינו את כתובת Pump.io שלכם בתור name@server ולחצו על הלחצן השג קוד מאמת.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="78"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation>אחר כך, הדפדפן רשת השגרתי שלכם צריך להעלות עמוד אימות בתוך שרת Pump.io שלכם. שם, יהיה עליכם להעתיק קוד VERIFIER במלואו, ולהדביקו לתוך השדה השני של Dianara. אחר כך לחצו התר יישום, וברגע שזה מאומת, הקישו שמור פרטים.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>בנקודה זו, הפרופיל, רשימות אנשי הקשר וצירי הזמן שלך יוטענו.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="87"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>עליכם לקחת מבט על חלון תצורת תוכנית, תחת התפריט הגדרות - הגדר את Dianara. ישנם מספר אפשרויות שם.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="91"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>לתשומת לבך, ישנם מקומות רבים בתוך Dianara אשר באפשרותך לקבל עוד מידע על ידי העברת סמן העכבר על גבי טקסט או לחצן, ולהמתין להופעתה של תיבה צצה.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="96"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>אם אתם חדשים בפרויקט Pump.io, קחו מבט על מדריך זה:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="98"/>
        <source>Pump.io User Guide</source>
        <translation>מדריך משתמש Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="110"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>באפשרותכם להגדיר מספר דברים לנוחותכם בתוך ההגדרות, כגון תדירות זמן בין עדכוני ציר זמן, על כמה פוסטים להופיע לכל עמוד, צבעי הדגשה, התראות או כיצד סמל מגש מערכת נראה.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="115"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translatorcomment>בתור פומבי</translatorcomment>
        <translation>כאן, באפשרותכם להפעיל את האפשרות לפרסם תמיד את הפוסטים שלך לציבור באופן שגרתי. תמיד תוכלו לשנות זאת בעת פרסום.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="128"/>
        <source>There are seven timelines:</source>
        <translation>ישנם שבעה צירי זמן:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="131"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>בציר זמן הראשי, מקום בו תראה את כל הדברים אשר פורסמו או שותפו על ידי האנשים אחריהם אתה עוקב.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="134"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>ציר זמן הודעות, מקום בו תראה הודעות אשר נשלחו ספיציפית אליך. קיימת אפשרות כי הודעות אלו נשלחו לאנשים אחרים גם כן.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="138"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>ציר זמן פעילות, מקום בו תראה את הפוסטים שלך, או פוסטים אשר שיתפת.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="141"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>ציר זמן מועדפים, מקום בו תראה את הפוסטים והתגובות אשר סימנת. זה יכול לשרת אותך בתור מערכת סימניות.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="146"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <comment>LEFT SIDE should change to RIGHT SIDE on RTL languages</comment>
        <translation>הציר זמן החמישי הוא הציר זמן השולי, מוכר גם בתור ״בינתיים״. זה נראה על הצד הימני, בכל זאת זה ניתן להסתרה. כאן תראו פעילויות שוליות אשר מבוצעות על ידי כל מי שאתם עוקבים, כגון פעולות תגובה, פוסטים אשר סומנו או אנשים אשר נתונים למעקבכם.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>הצירי זמן השישי והשביעי הינם גם צירי זמן שוליים, דומה לציר זמן ״בינתיים״, אך מכיל רק פעילויות אשר מוענו ישירות אליכם (איזכורים) ופעילויות אשר בוצעו על ידיכם (פעולות).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="158"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>לפעילויות אלה אפשרי כי יהיה כפתור &apos;+&apos; בתוכן. לחצו עליו כדי לפתוח את הפוסט אליו הם מפנים. גם כן, כמו במקומות אחרים רבים, באפשרותכם לעבור בעזרת העכבר שלכם כדי לראות מידע רלוונטי בתוך תיבה צצה.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>הודעות חדשות מוופיעות בהדגשה בצבע שונה. באפשרותכם לסמן אותן בתור כאלה שנקראו על ידי הקשה על אזורים ריקים של ההודעה.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="176"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translatorcomment>בגדר רשות</translatorcomment>
        <translation>באפשרותכם לפרסם מברקים על ידי הקשה בשדה טקסט בראש החלון או על ידי לחיצה Control+N. הגדרת כותרת לפוסט שלך הינו אופציונלי, אולם רצוי מאוד, שכן זה יעזור לזהות הפניות לפוסטים שלך בערוץ השולי טוב יותר, התראות דוא״ל, וכו׳.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>אפשר לצרף תמונות, אודיו, וידאו, וגם קבצים כלליים, כגון מסמכי PDF, לפוסטים שלכם.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="192"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>באפשרותכם להשתמש בלחצן עיצוב כדי להוסיף עיצוב לטקסט שלכם, כגון בולט או נטוי. חלק מאפשרויות אלה מצריכות בחירת טקסט בטרם אלה נכנסות לכדי שימוש.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="196"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>באפשרותכם לבחור מי יראה את הפוסטים שלכם באמצעות הלחצנים לכבוד וגם העתק.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="199"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>אם תוסיפו אדם ספיציפי לרשימת &apos;לכבוד&apos;, הם יקבלו את ההודעה שלכם בתוך כרטיסיית הודעות ישירות.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="202"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>באפשרותכם גם להקליד &apos;@&apos; ואת התו הראשון של השם של איש קשר כדי להעלות תפריט קופץ עם אפשרויות תואמות.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="206"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>בחרו אחת בעזרת מקשי חץ ולחצו Enter כדי להשלים את השם. זה יוסיף את אדם זה לרשימת נמענים.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="210"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>באפשרותכם ליצור הודעות פרטיות על ידי הוספת אנשים מסוימים לרשימות אלה, והאפשרויות אי-בחירת העוקבים או פומבי.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="223"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>באפשרותכם לראות את הרשימות של אנשים אשר אחריהם אתם עוקבים, ואלו אשר עוקבים אחריכם מתוך כרטיסיית אנשי קשר.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="226"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>שם, באפשרותכם גם לנהל רשימות אישים, בשימוש בעיקר כדי לשלוח פוסטים לקבוצות מסוימות של אנשים.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="229"/>
        <source>There is a text field at the top, where you can directly enter addresses of new contacts to follow them.</source>
        <translation>ישנו שדה טקסט במעלה, מקום בו באפשרותכם להזין ישירות כתובות של אנשי קשר חדשים כדי לעקוב אחריהם.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="233"/>
        <source>Under the &apos;Neighbors&apos; tab you&apos;ll see some resources to find people, and have the option to browse the latest registered users from your server directly.</source>
        <translation>תחת הכרטיסייה &apos;שכנים&apos; אתם תראו מספר משאבים למציאת אנשים, ולצד זאת תעמוד לרשותכם האפשרות לעיין במשתמשים הרשומים האחרונים מתוך השרת שלכם ישירות.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="237"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>באפשרותכם להקיש על כל אווטאר בפוסטים, בתגובות, ובטור ״בינתיים״, ויופיע לכם תפריט עם מספר אפשרויות, אחת מאלה היא לעקוב או לבטל מעקב אחר אדם זה.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="242"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>באפשרותך גם לשלוח הודעה ישירה (פרטית בהתחלה) לאיש קשר זה מתוך  תפריט זה.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="245"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>באפשרותכם למצוא רשימה עם משתמשי Pump.io מסוימים ומידע אחר כאן:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="249"/>
        <source>Users by language</source>
        <translation>משתמשים לפי שפה</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="251"/>
        <source>Followers of Pump.io Community account</source>
        <translation>עוקבי חשבון קהילת Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="263"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>לפעולות הכי נפוצות אשר מצויים בתפריטים יש קיצורי מקלדת, כגון F5 או Control+N.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="267"/>
        <source>Besides that, you can use:</source>
        <translation>נוסף על כך, באפשרותכם להשתמש באלו:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="270"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation>‏Control+מעלה/מטה/PgUp/PgDown/Home/End כדי לזוז סביב הציר זמן.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="273"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>‏Control+שמאל/ימין כדי לקפוץ עמוד אחד בתוך ציר זמן.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="276"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>‏Control+G כדי לגשת לכל עמוד בתוך ציר זמן ישירות.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="279"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>‏Control+1/2/3 כדי להחליף בין ערוצים שוליים.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="282"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>‏Control+Enter כדי לפרסם, כאשר אתה מסיים לכתוב מברק או תגובה. אם המברק הינו ריק, באפשרותך לבטלה על ידי לחיצה על ESC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="286"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>בזמן כתיבת מברק, לחצו Enter כדי לקפוץ מהכותרת לגוף הודעה. גם כן, לחיצה על החץ מעלה כאשר אתם מצויים בתחילת ההודעה, תקפיץ אתכם בחזרה לכותרת.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="291"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>‏Control+Enter כדי לסיים ליצור רשימת נמענים לפרסום, בתוך הרשימות &apos;לכבוד&apos; או &apos;העתק&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="304"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>באפשרותכם להשתמש בפרמטר config-- כדי להריץ את התוכנית בעזרת תצורה אחרת. זה יכול להועיל כדי להשתמש בשני חשבונות או יותר. באפשרותכם אף להריץ שני תהליכים של Dianara בו זמנית.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="309"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>השתמשו בפרמטר debug-- כדי להקנות לכם מידע נוסף בתוך חלון מסוף, אודות מה התוכנית עושה.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="312"/>
        <source>If your server does not support HTTPS, you can use the --nohttps parameter.</source>
        <translation>אם השרת שלכם לא תומך HTTPS, אתם יכולים להשתמש בפרמטר nohttps--.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="316"/>
        <source>Dianara offers a D-Bus interface that allows some control from other applications.</source>
        <translation>‏Dianara מציעה ממשק D-Bus אשר מתיר בקרה מסוימת מתוך יישומים אחרים.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="319"/>
        <source>The interface is at %1, and you can access it with tools such as %2 or %3. It offers methods like %4 and %5.</source>
        <translation>הממשק נמצא על %1, ובאפשרותכם לקבל גישה אליו בעזרת כלים כגון %2 או %3. זה מציע שיטות כמו %4 וגם %5.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="326"/>
        <source>If you use an alternate configuration, with something like &apos;--config otherconf&apos;, then the interface will be at org.nongnu.dianara_otherconf.</source>
        <translation>אם אתם משתמשים בתצורה חלופית, עם משהו כמו &apos;config otherconf--&apos;, אזי הממשק יהיה בכתובת org.nongnu.dianara_otherconf.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="343"/>
        <source>&amp;Close</source>
        <translation>&amp;סגור</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="31"/>
        <source>Untitled</source>
        <translation>ללא כותרת</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="33"/>
        <source>Image</source>
        <translation>תמונה</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="67"/>
        <source>&amp;Save As...</source>
        <translation>&amp;שמור בתור...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="80"/>
        <source>&amp;Restart</source>
        <comment>Restart animation</comment>
        <translation>התחל &amp;מחדש</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="90"/>
        <source>Fit</source>
        <comment>As in: fit image to window</comment>
        <translation>התאם</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="137"/>
        <source>Rotate image to the left</source>
        <comment>RTL: This actually means LEFT, anticlockwise</comment>
        <translation type="unfinished">סובב תמונה לשמאל</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="147"/>
        <source>Rotate image to the right</source>
        <comment>RTL: This actually means RIGHT, clockwise</comment>
        <translation type="unfinished">סובב תמונה לימין</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="160"/>
        <source>&amp;Close</source>
        <translation>&amp;סגור</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="296"/>
        <source>Save Image...</source>
        <translation>שמור תמונה...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="336"/>
        <source>Close Viewer</source>
        <translation>סגור צופה</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="377"/>
        <source>Downloading full image...</source>
        <translation>כעת מוריד תמונה במלואה...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="523"/>
        <source>Error downloading image!</source>
        <translation>שגיאה בהורדת תמונה!</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="525"/>
        <source>Try again later.</source>
        <translation>נסה שוב מאוחר יותר.</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="536"/>
        <source>Save Image As...</source>
        <translation>שמור תמונה בתור...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="539"/>
        <source>Image files</source>
        <translation>קבצי תמונה</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="540"/>
        <source>All files</source>
        <translation>כל הקבצים</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="551"/>
        <source>Error saving image</source>
        <translation>שגיאה בשמירת תמונה</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="552"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>התרחשה שגיאה במהלך שמירת %1.

שם קובץ צריך להישמר בסיומות jpg. או .png.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="39"/>
        <source>Members</source>
        <translation>חברים</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="52"/>
        <source>Add Mem&amp;ber</source>
        <translation>הוס&amp;ף חבר</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="60"/>
        <source>&amp;Remove Member</source>
        <translation>הס&amp;ר חבר</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="69"/>
        <source>&amp;Delete Selected List</source>
        <translation>&amp;מחק רשימה נבחרת</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="77"/>
        <source>Add New &amp;List</source>
        <translation>הוסף &amp;רשימה חדשה</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="80"/>
        <source>Type a name for the new list...</source>
        <translation>הקלד שם לרשימה החדשה...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="89"/>
        <source>Type an optional description here</source>
        <translation>הקלד תיאור אופציונאלי כאן</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="96"/>
        <source>Create L&amp;ist</source>
        <translation>&amp;צור רשימה</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="104"/>
        <source>&amp;Add to List</source>
        <translation>הוס&amp;ף לרשימה</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="315"/>
        <source>WARNING: Delete list?</source>
        <translation>ﬡזﬣﬧﬣ: למחוק רשימה?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="316"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation>האם אתה בטוח כי ברצונך למחוק את %1?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;כן, מחק זאת</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="451"/>
        <source>Remove person from list?</source>
        <translation>להסיר אדם מתוך רשימה?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="452"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>האם אתה בטוח כי ברצונך להסיר את %1 מתוך הרשימה %2?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;Yes</source>
        <translation>&amp;כן</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>יומן</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>טהר יו&amp;מן</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="61"/>
        <source>&amp;Close</source>
        <translation>&amp;סגור</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="163"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation>פעילויות שוליות אשר נעשו על ידי כולם, כגון משוב על פוסט</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Minor activities addressed to you</source>
        <translation>השג פעילויות שוליות אשר הופנו על ידך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="198"/>
        <source>Minor activities done by you</source>
        <translation>פעילויות שוליות אשר נעשו על ידך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>&amp;Contacts</source>
        <translation>&amp;אנשי קשר</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="429"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>האנשים אחריהם אתה עוקב, אלו שעוקבים אחריך, ורשימות האישים שלך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="723"/>
        <source>Press F1 for help</source>
        <translation>הקש F1 לעזרה</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="738"/>
        <location filename="../src/mainwindow.cpp" line="1387"/>
        <source>Initializing...</source>
        <translation>כעת מאתחל...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="788"/>
        <source>Your account is not configured yet.</source>
        <translation>חשבונך אינו מוגדר עדיין.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="794"/>
        <source>Click here to configure your account</source>
        <translation>הקש כאן כדי להגדיר את חשבונך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>&amp;Session</source>
        <translation>&amp;סשן</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>עדכן &amp;אוטומטית צירי זמן</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1039"/>
        <source>Mark All as Read</source>
        <translation>סמן הכל כנקרא</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;פרסם מברק</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>&amp;Quit</source>
        <translation>י&amp;ציאה</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1072"/>
        <source>&amp;View</source>
        <translation>&amp;תצוגה</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>&amp;Toolbar</source>
        <translation>סרגל &amp;כלים</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1099"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;שורת מצב</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1112"/>
        <source>Full &amp;Screen</source>
        <translation>&amp;מסך מלא</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>&amp;Log</source>
        <translation>יו&amp;מן</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>S&amp;ettings</source>
        <translation>&amp;הגדרות</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Edit &amp;Profile</source>
        <translation>ערוך &amp;פרופיל</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>&amp;Account</source>
        <translation>&amp;חשבון</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>&amp;Filters and Highlighting</source>
        <translation>&amp;מסננים והדגשות</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>&amp;Configure Dianara</source>
        <translation>&amp;הגדר את Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>&amp;Help</source>
        <translation>&amp;עזרה</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Basic &amp;Help</source>
        <translation>&amp;עזרה בסיסית</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>Visit &amp;Website</source>
        <translation>בקר &amp;אתר רשת</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <source>Report a &amp;Bug</source>
        <translation>דווח על &amp;באג</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1241"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>&amp;מדריך משתמש Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1249"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>כמה &amp;טיפים של Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1257"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>רשימה של &amp;משתמשי Pump.io מסוימים</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1265"/>
        <source>Pump.io &amp;Network Status Website</source>
        <translation>אתר רשת מצב &amp;רשת Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1276"/>
        <source>About &amp;Dianara</source>
        <translation>אודות &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>Toolbar</source>
        <translation>סרגל כלים</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1371"/>
        <source>Open the log viewer</source>
        <translation>פתח צופה יומן</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1394"/>
        <source>Auto-updating enabled</source>
        <translation>עדכון אוטומטי מאופשר</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1401"/>
        <source>Auto-updating disabled</source>
        <translation>עדכון אוטומטי מנוטרל</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1953"/>
        <source>Proxy password required</source>
        <translation>סיסמת ציר נדרשת</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1954"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>הגדרת שרת ציר לצד אימות, אך הסיסמה אינה מוגדרת.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1959"/>
        <source>Enter the password for your proxy server:</source>
        <translation>הזן את הסיסמה עבור שרת ציר שלך:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2048"/>
        <source>Your biography is empty</source>
        <translation>הביוגרפיה שלך הינה ריקה</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2083"/>
        <source>Click to edit your profile</source>
        <translation>הקש כדי לערוך את הפרופיל שלך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2108"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>מתחיל עדכון אוטומטי של צירי זמן, אחת לכל %1 דקות.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2115"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>הפסיק עדכון אוטומטי של צירי זמן.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2376"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation>נתקבלו %1 פוסטים ישנים יותר בתוך &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2416"/>
        <source>1 highlighted</source>
        <comment>singular, refers to a post</comment>
        <translation type="unfinished">1 הודגש</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2421"/>
        <source>%1 highlighted</source>
        <comment>plural, refers to posts</comment>
        <translation type="unfinished">%1 הודגשו</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2411"/>
        <source>Direct messages</source>
        <translation>הודעות ישירות</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2412"/>
        <source>By filters</source>
        <translation>לפי מסננים</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2453"/>
        <source>1 more pending to receive.</source>
        <comment>singular, one post</comment>
        <translation>1 נוסף ממתין להתקבל.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2458"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation>%1 נוספים ממתינים להתקבל.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2470"/>
        <location filename="../src/mainwindow.cpp" line="2886"/>
        <source>Also:</source>
        <translation>גם:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2592"/>
        <source>Last update: %1</source>
        <translation>עדכון אחרון: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2385"/>
        <location filename="../src/mainwindow.cpp" line="2822"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>‏&apos;%1&apos; עודכן.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2814"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation>נתקבלו %1 פעילויות ישנות יותר בתוך &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2869"/>
        <source>1 more pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation>1 נוספת ממתינה להתקבל.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2874"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation>%1 נוספות ממתינות להתקבל.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3453"/>
        <source>Dianara is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation>‏Dianara היא תכנה חופשית, רשויה תחת הרשיון GNU GPL, ומשתמשת במספר סמלי Oxygen תחת הרשיון LGPL.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3599"/>
        <source>Shutting down Dianara...</source>
        <translation>כעת מכבה את Dianara...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3678"/>
        <source>System tray icon is not available.</source>
        <translation>סמל מגש מערכת אינו זמין.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3680"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>אי אפשר להסתיר את Dianara בתוך מגש מערכת.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3683"/>
        <source>Do you want to close the program completely?</source>
        <translation>האם ברצונך לסגור את התוכנית לחלוטין?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2391"/>
        <source>Timeline updated at %1.</source>
        <translation>ציר זמן עודכן בשעה %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="934"/>
        <source>Update %1</source>
        <translation>עדכן %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Locked Panels and Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1075"/>
        <source>Side Panel</source>
        <translation type="unfinished">סרגל צד</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="764"/>
        <source>%1 started.</source>
        <comment>1=program name and version</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="767"/>
        <source>Running with Qt v%1 on %2.</source>
        <comment>1=Qt version, 2=OS name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Show Welcome Wizard</source>
        <translation>הצג אשף התחלה</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2397"/>
        <source>There is 1 new post.</source>
        <translation>ישנו פוסט חדש 1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2401"/>
        <source>There are %1 new posts.</source>
        <translation>ישנם %1 פוסטים חדשים.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2477"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 סונן.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2482"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 סוננו.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2495"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 נמחק.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2500"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 נמחקו.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2561"/>
        <source>No new posts.</source>
        <translation>אין פוסטים חדשים.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2645"/>
        <source>Total posts: %1</source>
        <translation>סה״כ פוסטים: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2653"/>
        <source>&amp;Timeline</source>
        <translation>&amp;ציר זמן</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2655"/>
        <source>The main timeline</source>
        <translation>ציר זמן הראשי</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2662"/>
        <source>&amp;Messages</source>
        <translation>&amp;הודעות</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2664"/>
        <source>Messages sent explicitly to you</source>
        <translation>הודעות אשר נשלחו במפורש אליך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2671"/>
        <source>&amp;Activity</source>
        <translation>&amp;פעילות</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2673"/>
        <source>Your own posts</source>
        <translation>הפוסטים שלך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2680"/>
        <source>Favor&amp;ites</source>
        <translation>&amp;מועדפים</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2682"/>
        <source>Your favorited posts</source>
        <translation>הפוסטים המועדפים שלך</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2737"/>
        <source>Your Pump.io account is not configured</source>
        <translation>חשבון Pump.io שלך אינו מוגדר</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2827"/>
        <source>Minor feed updated at %1.</source>
        <translation>ערוץ שולי התעדכן ב%1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2834"/>
        <source>There is 1 new activity.</source>
        <translation>ישנה פעילות חדשה 1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2838"/>
        <source>There are %1 new activities.</source>
        <translation>ישנן %1 פעילויות חדשות.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2851"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation>1 הודגשה.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2856"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation>%1 הודגשו.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2890"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to one activity</comment>
        <translation>1 סוננה.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>%1 filtered out.</source>
        <comment>plural, several activities</comment>
        <translation>%1 סוננו.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2964"/>
        <source>No new activities.</source>
        <translation>אין פעילויות חדשות.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3040"/>
        <source>Error storing image!</source>
        <translation>שגיאה באחסון תמונה!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3042"/>
        <source>%1 bytes</source>
        <translation>%1 בתים</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3086"/>
        <source>Link to: %1</source>
        <translation type="unfinished">קישור אל: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3105"/>
        <source>Marking everything as read...</source>
        <translation>סמן את הכל כנקרא...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>About Dianara</source>
        <translation>אודות Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3432"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>&lt;p dir=rtl&gt;Dianara היא לקוח רשת חברתית pump.io.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3436"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>&lt;p dir=rtl&gt;בעזרת Dianara באפשרותך לראות צירי זמן, ליצור פוסטים חדשים, להעלות תצלומים ומדיות אחרות, להידבר עם פרסומים, לנהל את אנשי הקשר שלך ולעקוב אחר אנשים חדשים.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3442"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>תרגום לעברית מאת GreenLunar.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3449"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>תודה לכל הבוחנים, המתרגמים והאורזים, אשר עזרו להפוך את Dianara לטובה יותר!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3490"/>
        <source>&amp;Hide Window</source>
        <translation>&amp;הסתר חלון</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3501"/>
        <source>&amp;Show Window</source>
        <translation>&amp;הצג חלון</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3549"/>
        <source>Closing due to environment shutting down...</source>
        <translation>כעת סוגר בשל כיבוי סביבה...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3572"/>
        <location filename="../src/mainwindow.cpp" line="3677"/>
        <source>Quit?</source>
        <translation>לצאת?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3573"/>
        <source>You are composing a note or a comment.</source>
        <translation>אתה כעת כותב מברק או תגובה.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3575"/>
        <source>Do you really want to close Dianara?</source>
        <translation>האם אתה באמת רוצה לסגור את Dianara?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3576"/>
        <location filename="../src/mainwindow.cpp" line="3685"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;כן, סגור את תוכנית זו</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3576"/>
        <location filename="../src/mainwindow.cpp" line="3685"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="70"/>
        <source>Older Activities</source>
        <translation>פעילויות ישנות יותר</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="74"/>
        <source>Get previous minor activities</source>
        <translation>השג פעילויות שוליות קודמות</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="102"/>
        <source>There are no activities to show yet.</source>
        <translation>אין פעילויות להצגה עדיין.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="452"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation>השג %1 חדשות</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="69"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>באמצעות %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="83"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>לכבוד: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="90"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>העתק: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="266"/>
        <source>Open referenced post</source>
        <translation>פתח פוסט מאזכר</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="315"/>
        <source>bytes</source>
        <translation>בתים</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="671"/>
        <source>Error: Unable to launch browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="672"/>
        <source>The default system web browser could not be executed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="675"/>
        <source>You might need to install the XDG utilities.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>קפוץ אל עמוד</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="35"/>
        <source>Page number:</source>
        <translation>עמוד מספר:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="50"/>
        <source>&amp;First</source>
        <comment>As in: first page</comment>
        <translation>&amp;ראשון</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="57"/>
        <source>&amp;Last</source>
        <comment>As in: last page</comment>
        <translation>&amp;אחרון</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="64"/>
        <source>Newer</source>
        <comment>As in: newer pages</comment>
        <translation>חדש יותר</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="85"/>
        <source>Older</source>
        <comment>As in: older pages</comment>
        <translation>ישן יותר</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="96"/>
        <source>&amp;Go</source>
        <translation>&amp;עבור</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>&amp;חפש:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>הזן שם לחיפוש כאן</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="102"/>
        <source>Add a contact to a list</source>
        <translation>הוסף איש קשר לרשימה</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="114"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>לחץ על התמונה כדי לראותה בגודל מלא</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>לחץ כדי להוריד את התצריף</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="140"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>פוסט</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="205"/>
        <source>Via %1</source>
        <translation>דרך %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="215"/>
        <location filename="../src/post.cpp" line="436"/>
        <source>To</source>
        <translation>לכבוד</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="221"/>
        <location filename="../src/post.cpp" line="453"/>
        <source>Cc</source>
        <translation>העתק</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="259"/>
        <source>Shared on %1</source>
        <translation>שותף ב%1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="263"/>
        <location filename="../src/post.cpp" line="952"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>באמצעות %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="313"/>
        <source>Open post in web browser</source>
        <translation>פתח פוסט בתוך דפדפן רשת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="322"/>
        <source>Copy post link to clipboard</source>
        <translation>העתק קישור פוסט ללוח גזירה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="341"/>
        <source>Normalize text colors</source>
        <translatorcomment>הפוך צבעי טקסט לצבע אחיד</translatorcomment>
        <translation type="unfinished">נרמל צבעי טקסט</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="359"/>
        <source>&amp;Close</source>
        <translation>&amp;סגור</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="942"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>פורסם ב%1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="944"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>טיפוס</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="959"/>
        <source>Modified on %1</source>
        <translation>שונה ב%1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1000"/>
        <source>In</source>
        <translation type="unfinished">בתוך</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="522"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>הורה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="531"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>פתח פוסט הורה, אשר לו זה מגיב</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="627"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>הגב</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>Reply to this post.</source>
        <translation>השב לפוסט זה.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="632"/>
        <source>If you select some text, it will be quoted.</source>
        <translation>אם תבחר טקסט מסוים, זה יצוטט.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="652"/>
        <source>Share</source>
        <translation>שתף</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="654"/>
        <source>Share this post with your contacts</source>
        <translation>שתף את פוסט זה עם אנשי קשר</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="660"/>
        <source>Unshare</source>
        <translation>בטל שיתוף</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="662"/>
        <source>Unshare this post</source>
        <translation>בטל שיתוף פוסט זה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="678"/>
        <source>Edit</source>
        <translation>ערוך</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="681"/>
        <source>Modify this post</source>
        <translation>שנה את פוסט זה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="690"/>
        <source>Delete</source>
        <translation>מחק</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="693"/>
        <source>Erase this post</source>
        <translation>מחק את פוסט זה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="767"/>
        <source>Join Group</source>
        <translation>הצטרף לקבוצה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="773"/>
        <source>%1 members in the group</source>
        <translation>%1 חברים בקבוצה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1123"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>תמונה הינה מונפשת. לחץ כדי להריץ אותה.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1133"/>
        <source>Size</source>
        <comment>Image size (resolution)</comment>
        <translation>מידה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1150"/>
        <source>Couldn&apos;t load image!</source>
        <translation>לא מסוגל לטעון תמונה!</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1155"/>
        <source>Loading image...</source>
        <translation>כעת טוען תמונה...</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1173"/>
        <source>Attached Audio</source>
        <translation>אודיו מצורף</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1183"/>
        <source>Attached Video</source>
        <translation>וידאו מצורף</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1217"/>
        <source>Attached File</source>
        <translation>קובץ מצורף</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1381"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>%1 אוהב(ת) זאת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1386"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation>%1 אהבו זאת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1407"/>
        <source>1 like</source>
        <translation>סימון 1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1412"/>
        <source>%1 likes</source>
        <translation>%1 סימונים</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1476"/>
        <source>1 comment</source>
        <translation>תגובה 1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1481"/>
        <source>%1 comments</source>
        <translation>%1 תגובות</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1537"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 שיתף(ה) זאת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1542"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 שיתפו זאת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1571"/>
        <source>Shared once</source>
        <translation>שותף פעם אחת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1576"/>
        <source>Shared %1 times</source>
        <translation>שותף %1 פעמים</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1709"/>
        <source>Edited: %1</source>
        <translation>נערך: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1795"/>
        <source>You like this</source>
        <translation>אהבת זאת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1796"/>
        <source>Unlike</source>
        <translation>בטל סימון</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1801"/>
        <source>Like this post</source>
        <translation>סמן את פוסט זה</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1802"/>
        <source>Like</source>
        <translation>אהבתי</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1957"/>
        <source>Are you sure you want to share your own post?</source>
        <translation>האם אתה בטוח כי ברצונך לשתף את הפוסט של עצמך?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1962"/>
        <source>Share post?</source>
        <translation>לשתף פוסט?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1952"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>האם ברצונך לשתף את הפוסט של %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1965"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;כן, שתף זאת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1966"/>
        <location filename="../src/post.cpp" line="1986"/>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1982"/>
        <source>Unshare post?</source>
        <translation>לבטל שיתוף פוסט?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1983"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>האם ברצונך לבטל שיתוף של הפוסט של %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1985"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;כן, אל תשתף זאת</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2027"/>
        <source>WARNING: Delete post?</source>
        <translation>ﬡזﬣﬧﬣ: למחוק פוסט?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2028"/>
        <source>Are you sure you want to delete this post?</source>
        <translation>האם אתה בטוח כי ברצונך למחוק את פוסט זה?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;כן, מחק זאת</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>עריכת פרופיל</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="51"/>
        <source>This is your Pump address</source>
        <translation>זוהי כתובת Pump שלך</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="53"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>זוהי הכתובת דוא״ל אשר משוייכת עם חשבונך, לצורך דברים כגון התראות או השבת סיסמה</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="70"/>
        <source>Change &amp;E-mail...</source>
        <translation>החלף &amp;דוא״ל...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="82"/>
        <source>Change &amp;Avatar...</source>
        <translation>החלף &amp;אווטאר...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="92"/>
        <source>This is your visible name</source>
        <translation>זהו השם הנראה שלך</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>Changing your avatar will create a post in your timeline with it.
If you delete that post your avatar will be deleted too.</source>
        <translation>החלפת האווטאר שלך תיצור פוסט בציר זמן שלך עמו.
אם תמחק את הפוסט הזה האווטאר שלך יימחק גם כן.</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="126"/>
        <source>&amp;Save Profile</source>
        <translation>&amp;שמור דיוקן</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="133"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="166"/>
        <source>Webfinger ID</source>
        <translation>מזהה Webfinger</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="167"/>
        <source>E-mail</source>
        <translation>דוא״ל</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="169"/>
        <source>Avatar</source>
        <translation>אווטאר</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="171"/>
        <source>Full &amp;Name</source>
        <translation>&amp;שם מלא</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="172"/>
        <source>&amp;Hometown</source>
        <translation>&amp;עיר מגורים</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="173"/>
        <source>&amp;Bio</source>
        <translation>&amp;ביו</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="211"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>לא מוגדר</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="284"/>
        <source>Select avatar image</source>
        <translation>שנה תמונת אווטאר</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="286"/>
        <source>Image files</source>
        <translation>קבצי תמונה</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="289"/>
        <source>All files</source>
        <translation>כל הקבצים</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="312"/>
        <source>Invalid image</source>
        <translation>תמונה לא כשרה</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="313"/>
        <source>The selected image is not valid.</source>
        <translation>התמונה הנבחרת אינה כשרה.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>תצורת Proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>אל תשתמש בציר</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>שם משתמש ציר</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>הערה: סיסמה אינה מאוחסנת באופן מאובטח. אם תרצה, באפשרותך להותיר את השדה ריק, ואתה תישאל לסיסמה בעת הפעלה.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="75"/>
        <source>&amp;Save</source>
        <translation>&amp;שמור</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="82"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="98"/>
        <source>Proxy &amp;Type</source>
        <translation>&amp;טיפוס ציר</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="100"/>
        <source>&amp;Hostname</source>
        <translation>שם מא&amp;רח</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="102"/>
        <source>&amp;Port</source>
        <translation>&amp;פורט</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="104"/>
        <source>Use &amp;Authentication</source>
        <translation>&amp;אימות משתמש</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="106"/>
        <source>&amp;User</source>
        <translation>&amp;משתמש</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="108"/>
        <source>Pass&amp;word</source>
        <translation>&amp;סיסמה</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>קביעת כותרת גורמת לערוץ ״בינתיים״ להיות יותר אינפורמטיבי</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="75"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="80"/>
        <source>Add a brief title for the post here (recommended)</source>
        <translation>הוסף כותרת מקוצרת לפוסט כאן (רצוי)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="108"/>
        <source>Remove</source>
        <translation>הסר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="111"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>בטל את התצריף, וחזור למברק רגיל</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="175"/>
        <source>Drafts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="185"/>
        <source>To...</source>
        <translation>לכבוד...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="188"/>
        <source>Select who will see this post</source>
        <translation>בחר מי יראה את פוסט זה</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="196"/>
        <source>Cc...</source>
        <translation>העתק...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="199"/>
        <source>Select who will get a copy of this post</source>
        <translation>בחר מי יקבל העתק של פוסט זה</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="229"/>
        <source>Picture</source>
        <translation>תצלום</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="236"/>
        <source>Audio</source>
        <translation>אודיו</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="243"/>
        <source>Video</source>
        <translation>וידאו</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="250"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>אחר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="266"/>
        <source>Ad&amp;d...</source>
        <translation>הוס&amp;ף...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="269"/>
        <source>Upload media, like pictures or videos</source>
        <translation>העלה מדיה, כגון תצלומים או סרטונים</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="294"/>
        <location filename="../src/publisher.cpp" line="929"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>פרסם</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="297"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>הקש Control+Enter כדי לפרסם בעזרת המקלדת</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="303"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="306"/>
        <source>Cancel the post</source>
        <translation>בטל את הפוסט</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="420"/>
        <source>Note started from another application.</source>
        <translation>מברק נפתח מתוך יישום אחר.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="424"/>
        <source>Ignoring new note request from another application.</source>
        <translation>מתעלם מבקשת מברק אחרת מתוך יישום אחר.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="951"/>
        <source>Attachment upload was cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1038"/>
        <source>Select Picture...</source>
        <translation>בחר תצלום...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1040"/>
        <source>Find the picture in your folders</source>
        <translation>מצא את התצלום בתוך התיקיות שלך</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1033"/>
        <source>Picture not set</source>
        <translation>תצלום לא נבחר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1063"/>
        <source>Select Audio File...</source>
        <translation>בחר קובץ אודיו...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1065"/>
        <source>Find the audio file in your folders</source>
        <translation>מצא את הקובץ אודיו בתוך התיקיות שלך</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1058"/>
        <source>Audio file not set</source>
        <translation>קובץ אודיו לא נבחר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1088"/>
        <source>Select Video...</source>
        <translation>בחר וידאו...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1090"/>
        <source>Find the video in your folders</source>
        <translation>מצא את הוידאו בתוך התיקיות שלך</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1083"/>
        <source>Video not set</source>
        <translation>וידאו לא נבחר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1112"/>
        <source>Select File...</source>
        <translation>בחר קובץ...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1114"/>
        <source>Find the file in your folders</source>
        <translation>מצא את הקובץ בתוך התיקיות שלך</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1107"/>
        <source>File not set</source>
        <translation>קובץ לא נבחר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1183"/>
        <location filename="../src/publisher.cpp" line="1227"/>
        <source>Error: Already composing</source>
        <translation>שגיאה: כבר מלחין</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1184"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>אין באפשרותך לערוך פוסט בזמן זה, מכיוון שתגובה אחרת כבר מצויה בשלבי כתיבה.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1202"/>
        <source>Update</source>
        <translation>עדכן</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1213"/>
        <source>Editing post.</source>
        <translation type="unfinished">עריכת פוסט.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="829"/>
        <source>It is owned by %1.</source>
        <comment>%1 = a username</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1228"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>אין באפשרותך ליצור הודעה עבור %1 בזמן זה, מכיוון שפרסום מצוי כעת בשלבי כתיבה.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1355"/>
        <source>Draft loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1360"/>
        <source>ERROR: Already composing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1361"/>
        <source>You can&apos;t load a draft at this time, because a post is already being composed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1380"/>
        <source>Draft saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1417"/>
        <source>Posting failed.

Try again.</source>
        <translation>פרסום נכשל.

נסה שוב.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1561"/>
        <source>Warning: You have no followers yet</source>
        <translation>אזהרה: אין לך עוקבים עדיין</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1562"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>אתה מנסה לפרסם לעוקבים שלך, אבל אין לך שום עוקבים עדיין.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1566"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>אם תפרסם בדרך זו, לאף אחד לא תהא האפשרות לראות את הודעתך.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1569"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>האם ברצונך להפוך את הפרסום לפומבי במקום לעוקבים-בלבד?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1572"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;כן, הפוך זאת לפומבי</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1573"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;לא, תפרסם לעוקבים שלי בלבד</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1574"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;בטל, חזור לפוסט</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1594"/>
        <source>Posting...</source>
        <translation>כעת מפרסם...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1627"/>
        <source>Updating...</source>
        <translation>כעת מעדכן...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1638"/>
        <source>Post is empty.</source>
        <translation>פוסט הינו ריק.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1642"/>
        <source>File not selected.</source>
        <translation>קובץ לא נבחר.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="590"/>
        <source>Select one image</source>
        <translation>בחירת תמונה יחידה</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="591"/>
        <source>Image files</source>
        <translation>קבצי תמונה</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="594"/>
        <source>Invalid image</source>
        <translation>תמונה לא כשרה</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="595"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>פורמט תמונה לא ניתן לאיתור.
הסיומת עשויה להיות שגויה, כמו תמונת GIF אשר שמה שונה אל image.jpg או דומה.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="601"/>
        <source>Select one audio file</source>
        <translation>בחירת קובץ אודיו יחיד</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="602"/>
        <source>Audio files</source>
        <translation>קבצי אודיו</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="605"/>
        <source>Invalid audio file</source>
        <translation>קובץ אודיו לא כשר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="606"/>
        <source>The audio format cannot be detected.</source>
        <translation>פורמט אודיו לא ניתן לאיתור.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="610"/>
        <source>Select one video file</source>
        <translation>בחירת קובץ וידאו יחיד</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="611"/>
        <source>Video files</source>
        <translation>קבצי וידאו</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="614"/>
        <source>Invalid video file</source>
        <translation>קובץ וידאו לא כשר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="615"/>
        <source>The video format cannot be detected.</source>
        <translation>פורמט וידאו לא ניתן לאיתור.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="619"/>
        <source>Select one file</source>
        <translation>בחירת קובץ יחיד</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="622"/>
        <source>Invalid file</source>
        <translation>קובץ לא כשר</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="623"/>
        <source>The file type cannot be detected.</source>
        <translation>טיפוס קובץ לא ניתן לאיתור.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="635"/>
        <source>All files</source>
        <translation>כל הקבצים</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="652"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>מאחר ואנחנו מעלים תמונה, באפשרותך להקטין את מידותיה מעט או לשמור אותה בפורמט דחוס, כמו JPG.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="657"/>
        <source>File is too big</source>
        <translation>קובץ הינו גדול מדי</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="658"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>‏Dianara בעת זו מגבילה העלאות לסך של 10 MiB לכל פוסט, כדי למנוע בעיות אחסון או רשת אפשריות בתוך השרתים.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="663"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>זוהי אמת מידה זמנית, מאחר והשרתים לא מסוגלים לקבוע גבולות בעצמם עדיין.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="668"/>
        <source>Sorry for the inconvenience.</source>
        <translation>סליחה על אי הנוחות.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="837"/>
        <source>File not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="840"/>
        <source>Error</source>
        <translation type="unfinished">שגיאה</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="822"/>
        <source>The selected file cannot be accessed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="832"/>
        <source>You might not have the necessary permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="697"/>
        <source>Resolution</source>
        <comment>Image resolution (size)</comment>
        <translation>רזולוציה</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="720"/>
        <source>Type</source>
        <translation>טיפוס</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="722"/>
        <source>Size</source>
        <translation>גודל</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1670"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 קי״ב מתוך %2 קי״ב הועלו</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="252"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>מורשה להשתמש בחשבון %1. משיג מידע ראשוני.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="257"/>
        <source>There is no authorized account.</source>
        <translation>אין חשבון מורשה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="549"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation type="unfinished">כעת משיג רשימה של &apos;עוקב&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="562"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>כעת משיג רשימה של &apos;עוקבים&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="633"/>
        <source>Getting list of person lists...</source>
        <translation>כעת משיג רשימה של רשימות אישים...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="669"/>
        <source>Creating person list...</source>
        <translation>כעת יוצר רשימת אישים...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="692"/>
        <source>Deleting person list...</source>
        <translation>כעת מוחק רשימת אישים...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="707"/>
        <source>Getting a person list...</source>
        <translation>כעת משיג רשימת אישים...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="751"/>
        <source>Adding person to list...</source>
        <translation>כעת מוסיף אדם לתוך רשימה...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="792"/>
        <source>Removing person from list...</source>
        <translation>כעת מסיר אדם מתוך רשימה...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="840"/>
        <source>Creating group...</source>
        <translation>כעת יוצר קבוצה...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="870"/>
        <source>Joining group...</source>
        <translation>כעת מצטרף לקבוצה...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="895"/>
        <source>Leaving group...</source>
        <translation>כעת עוזב קבוצה...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="910"/>
        <source>Getting likes...</source>
        <translation>כעת משיג סימונים...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="937"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation>התגובות לפוסט זה לא יכולות להיטען בשל מידע חסר מן השרת.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="942"/>
        <source>Getting comments...</source>
        <translation>כעת משיג תגובות...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="998"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>כעת משיג &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1045"/>
        <source>Timeline</source>
        <translation>ציר זמן</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1049"/>
        <source>Messages</source>
        <translation>הודעות</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1207"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>כעת מעלה %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1505"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>שגיאת HTTP</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1517"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>פקיעת זמן דרך-שער</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1527"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>שירות לא זמין</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1556"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation>לא מיושם</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1566"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>שגיאת שרת פנימית</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1586"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>אבוד</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1596"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>לא נמצא</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1606"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>אסור</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1616"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>לא מורשה</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1627"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>בקשה רעה</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1646"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>הועבר זמנית</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1656"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>הועבר לצמיתות</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1667"/>
        <source>Error connecting to %1</source>
        <translation>שגיאה בהתחברות אל %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1677"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>קוד שגיאת HTTP לא מטופל %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1790"/>
        <source>Profile received.</source>
        <translation>פרופיל נתקבל.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1792"/>
        <source>Followers</source>
        <translation>עוקבים</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1797"/>
        <source>Following</source>
        <translation>עוקב</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1813"/>
        <source>Profile updated.</source>
        <translation>פרופיל עודכן.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2068"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>נתקבל &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2619"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation>כעת טוען תמונות חיצוניות מתוך %1 בלי התחשבות בשגיאות SSL, כפי שהוגדר...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1053"/>
        <source>Activity</source>
        <translation>פעילות</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="362"/>
        <source>Updating profile...</source>
        <translation>כעת מעדכן פרופיל...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="581"/>
        <source>Getting site users for %1...</source>
        <comment>%1 is a server name</comment>
        <translation>כעת משיג משתמשי אתר של %1...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1057"/>
        <source>Favorites</source>
        <translation>מועדפים</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1062"/>
        <source>Meanwhile</source>
        <translation>בינתיים</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1066"/>
        <source>Mentions</source>
        <translation>איזכורים</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1070"/>
        <source>Actions</source>
        <translation>פעולות</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1075"/>
        <source>User timeline</source>
        <translation>ציר זמן משתמש</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1465"/>
        <source>Error loading timeline!</source>
        <translation>שגיאה בטעינת ציר זמן!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1476"/>
        <source>Error loading minor feed!</source>
        <translation>שגיאה בטעינת ערוץ שולי!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1488"/>
        <source>Unable to verify the address!</source>
        <translation>לא מסוגל לאמת את הכתובת!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1546"/>
        <source>Bad Gateway</source>
        <comment>HTTP 502 error string</comment>
        <translation>דרך-שער רע</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1787"/>
        <source>Server version: %1</source>
        <translation>גרסת שרת: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1822"/>
        <source>E-mail updated: %1</source>
        <translation>דוא״ל עודכן: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1850"/>
        <source>%1 published successfully. Updating post content...</source>
        <comment>%1 is the type of object: note, image...</comment>
        <translation>%1 התפרסם בהצלחה. כעת מעדכן תוכן פוסט...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1866"/>
        <source>Untitled post %1 published successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>פוסט (בלי כותרת) %1 פורסם בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1870"/>
        <source>Post %1 published successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>פוסט %1 פורסם בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1887"/>
        <source>Avatar published successfully.</source>
        <translation>אווטאר פורסם בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1919"/>
        <source>Untitled post %1 updated successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>פוסט (בלי כותרת) %1 עודכן בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1923"/>
        <source>Post %1 updated successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>פוסט %1 עודכן בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1933"/>
        <source>Comment %1 updated successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>תגובה %1 עודכנה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1947"/>
        <source>Message liked or unliked successfully.</source>
        <translation type="unfinished">הודעה סומנה או שסימונה בוטל בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1961"/>
        <source>Likes received.</source>
        <translation>סימונים נתקבלו.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1981"/>
        <source>Comment %1 posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>תגובה %1 פורסמה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2006"/>
        <source>1 comment received.</source>
        <translation>תגובה 1 נתקבלה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2010"/>
        <source>%1 comments received.</source>
        <translation>נתקבלו %1 תגובות.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2037"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>פוסט מאת %1 שותף בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2081"/>
        <source>Adding items...</source>
        <translation>כעת מוסיף פריטים...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2164"/>
        <source>Message deleted successfully.</source>
        <translation>הודעה נמחקה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2192"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>עוקב אחר %1 ‏(%2) בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2202"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>הפסיק לעקוב אחר %1 ‏(%2) בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2252"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>רשימת &apos;עוקב&apos; נתקבלה לגמרי.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2259"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>רשימה חלקית של &apos;עוקב&apos; נתקבלה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2282"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>רשימת &apos;עוקבים&apos; נתקבלה לגמרי.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2288"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>רשימה חלקית של &apos;עוקבים&apos; נתקבלה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2325"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation type="unfinished">רשימת של &apos;lists&apos; נתקבלה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2345"/>
        <source>List of %1 users received.</source>
        <comment>%1 is a server name</comment>
        <translation>רשימה של משתמשי %1 נתקבלה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2369"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>רשימת אישים &apos;%1&apos; נוצרה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2380"/>
        <source>Person list deleted successfully.</source>
        <translation>רשימת אישים נמחקה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2400"/>
        <source>Person list received.</source>
        <translation>רשימת אישים נתקבלה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2435"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 ‏(%2) התווסף לתוך רשימה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 ‏(%2) הוסר מתוך רשימה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2466"/>
        <source>Group %1 created successfully.</source>
        <translation>קבוצה %1 נוצרה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2477"/>
        <source>Group %1 joined successfully.</source>
        <translation>קבוצה %1 צורפה בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2490"/>
        <source>Left the %1 group successfully.</source>
        <translation>עזב את הקבוצה %1 בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2554"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>קובץ הועלה בהצלחה. כעת מפרסם הודעה...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2563"/>
        <source>Avatar uploaded.</source>
        <translation>אווטאר הועלה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2607"/>
        <source>SSL errors in connection to %1!</source>
        <translation>שגיאות SSL בחיבור אל %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2648"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>היישום אינו רשום עם השרת שלך עדיין. כעת רושם...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2685"/>
        <source>Getting OAuth token...</source>
        <translation>כעת משיג אסימון OAuth...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2706"/>
        <source>OAuth support error</source>
        <translation>שגיאת תמיכת OAuth</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2707"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>נראה כי להתקנת QOAuth שלך, ספרייה אשר מנוצלת על ידי Dianara, אין תמיכת HMAC-SHA1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2711"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>עליך כנראה להתקין תוסף OpenSSL לצורך QCA: %1, %2 או דומה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2765"/>
        <location filename="../src/pumpcontroller.cpp" line="2821"/>
        <source>Authorization error</source>
        <translation>שגיאת אימות</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2766"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>אירעה שגיאת OAuth במהלך ניסיון להשיג אסימון הרשאה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2769"/>
        <source>QOAuth error %1</source>
        <translation>שגיאת QOAuth %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2801"/>
        <source>Application authorized successfully.</source>
        <translation>יישום אושר בהצלחה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2817"/>
        <source>OAuth error while authorizing application.</source>
        <translation>שגיאת OAuth במהלך אישור יישום.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2844"/>
        <source>Waiting for proxy password...</source>
        <translation>כעת ממתין לסיסמת ציר...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2873"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>עדיין ממתין לפרופיל. כעת מנסה שוב...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2985"/>
        <source>%1 attempts</source>
        <translation>%1 ניסיונות</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2989"/>
        <source>1 attempt</source>
        <translation>ניסיון 1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2992"/>
        <source>Some initial data was not received. Restarting initialization...</source>
        <translation>מידע ראשוני מסוים לא נתקבל. מתחיל מחדש פתיחה...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2999"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>מידע ראשוני מסוים לא נתקבל עובר ניסיונות מספר. משהו עלול להיות משובש בשרת שלך. אפשרי כי עוד תוכל להשתמש בשירות באופן רגיל.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3009"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>כל המידע הראשוני נתקבל. פתיחה הושלמה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3022"/>
        <source>Ready.</source>
        <translation>מוכן.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3410"/>
        <source>Can&apos;t follow %1 at this time.</source>
        <comment>%1 is a user ID</comment>
        <translation>לא מסוגל לעקוב אחר %1 ברגע זה.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3414"/>
        <source>Trying to follow %1.</source>
        <comment>%1 is a user ID</comment>
        <translation>כעת מנסה לעקוב אחר %1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3434"/>
        <source>Checking address %1 before following...</source>
        <translation>כעת בודק כתובת %1 בטרם עקיבה...</translation>
    </message>
</context>
<context>
    <name>SiteUsersList</name>
    <message>
        <location filename="../src/siteuserslist.cpp" line="30"/>
        <source>You can get a list of the newest users registered on your server by clicking the button below.</source>
        <translation>באפשרותך להשיג רשימה של המשתמשים הרשומים החדשים ביותר על השרת שלך על ידי הקשה על הלחצן מטה.</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="47"/>
        <source>More resources to find users:</source>
        <translation>עוד משאבים כדי למצוא משתמשים:</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="51"/>
        <source>Wiki page &apos;Users by language&apos;</source>
        <translation>עמוד Wiki &apos;משתמשים לפי שפה&apos;</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="56"/>
        <source>PPump user search service at inventati.org</source>
        <translation>שירות חיפוש משתמשים PPump אצל inventati.org</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="61"/>
        <source>List of Followers for the Pump.io Community account</source>
        <translation>רשימת עוקבים שח\ל חשבון קהילת Pump.io</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="41"/>
        <source>Get list of users from your server</source>
        <translation>השג רשימת משתמשים מתוך השרת שלי</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="87"/>
        <source>Close list</source>
        <translation>סגור רשימה</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="143"/>
        <source>Loading...</source>
        <translation>כעת טוען...</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="154"/>
        <source>%1 users in %2</source>
        <comment>%1 = user count, %2 = server name</comment>
        <translation>%1 משתמשים בתוך %2</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="76"/>
        <source>Welcome to Dianara</source>
        <translation>ברוך בואך אל Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="78"/>
        <source>Dianara is a &lt;b&gt;Pump.io&lt;/b&gt; client.</source>
        <translation>&amp;rlm;Dianara היא לקוח &lt;b&gt;Pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="81"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>אם אין לך חשבון Pump עדיין, באפשרותך להשיג חשבון בכתובת הבאה, למשל:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="88"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>הקש &lt;b&gt;F1&lt;/b&gt; אם ברצונך לפתוח חלון עזרה.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="91"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>ראשית, הגדר את החשבון שלך מתוך התפריט &lt;b&gt;הגדרות - חשבון&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="94"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>לאחר שהתהליך הסתיים, הפרופיל וצירי הזמן שלך אמורים להתעדכן אוטומטית.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="98"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>קח רגע אחד כדי להסתכל סביב התפריטים וחלון התצורה.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="102"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>באפשרותך גם להגדיר את נתוני ותצלום הפרופיל שלך מתוך התפריט &lt;b&gt;הגדרות - ערוך פרופיל&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="106"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>ישנן תיבות צצות בכל מקום, כך שאם אתה עובר על לחצן או שדה טקסט בעזרת העכבר שלך, אתה עשוי לראות מידע נוסף.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="113"/>
        <source>Dianara&apos;s blog</source>
        <translation>בלוג Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="115"/>
        <source>Pump.io User Guide</source>
        <translation>מדריך משתמש Pump.io</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="126"/>
        <source>Direct Messages Timeline</source>
        <translation>ציר זמן הודעות ישירות</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="127"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>כאן, אתה תמצא פוסטים אשר מכוונים הישר אליך.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="137"/>
        <source>Activity Timeline</source>
        <translation>ציר זמן פעילות</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="138"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>אתה תראה את הפוסטים שלך כאן.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="147"/>
        <source>Favorites Timeline</source>
        <translation>ציר זמן מועדפים</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="148"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>פוסטים ותגובות אשר אהבת.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="231"/>
        <source>Newest</source>
        <translation>הכי חדש</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="240"/>
        <source>Newer</source>
        <translation>חדש יותר</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="263"/>
        <source>Older</source>
        <translation>ישן יותר</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="335"/>
        <source>Requesting...</source>
        <translation>כעת מבקש...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="506"/>
        <source>Loading...</source>
        <translation>כעת טוען...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="615"/>
        <source>Page %1 of %2.</source>
        <translation>עמוד %1 מתוך %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="619"/>
        <source>Showing %1 posts per page.</source>
        <translation>מציג %1 פוסטים בכל עבוד.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="622"/>
        <source>%1 posts in total.</source>
        <translation>%1 פוסטים בסך הכל.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="626"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>הקש כאן או לחץ Control+G כדי לקפוץ לעמוד מסוים</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="740"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>&apos;%1&apos; לא ניתן לעדכון מכיוון שתגובה מצויה בשלבי כתיבה.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="848"/>
        <source>%1 more posts pending for next update.</source>
        <translation>%1 פוסטים נוספים ממתינים לעדכון הבא.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="854"/>
        <source>Click here to receive them now.</source>
        <translation>לחץ כאן כדי לקבלם עכשיו.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="1139"/>
        <source>There are no posts</source>
        <translation>אין פוסטים</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>חותמת זמן לא כשרה!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>ממש עכשיו</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>בעתיד</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>לפני דקה</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>לפני %1 דקות</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>לפני שעה</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>לפני %1 שעות</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>אתמול</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>לפני %1 ימים</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>לפני חודש</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>לפני %1 חודשים</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>לפני שנה</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>לפני %1 שנים</translation>
    </message>
</context>
<context>
    <name>UserPosts</name>
    <message>
        <location filename="../src/userposts.cpp" line="39"/>
        <source>Posts by %1</source>
        <translation>פוסטים מאת %1</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="64"/>
        <source>Loading...</source>
        <translation>כעת טוען...</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="69"/>
        <source>&amp;Close</source>
        <translation>&amp;סגור</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="153"/>
        <source>Received &apos;%1&apos;.</source>
        <translation>נתקבל &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="160"/>
        <source>%1 posts</source>
        <translatorcomment>פרסומים</translatorcomment>
        <translation>%1 פוסטים</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="169"/>
        <source>Error loading the timeline</source>
        <translation>שגיאה בטעינת ציר זמן</translation>
    </message>
</context>
</TS>
