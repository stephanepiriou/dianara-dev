/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COMPOSER_H
#define COMPOSER_H

#include <QTextEdit>
#include <QFocusEvent>
#include <QLabel>
#include <QPushButton>
#include <QToolButton>
#include <QMenu>
#include <QInputDialog>
#include <QMessageBox>
#include <QApplication>
#include <QClipboard>
#include <QMimeData> // Needed since Qt5
#include <QCompleter>
#include <QAbstractItemView> // For QCompleter's popup
#include <QTableView>
#include <QHeaderView>
#include <QTextDocument>
#include <QTextDocumentFragment>
#include <QTextCursor>

#include <QDebug>

#ifdef HAVE_SONNET_SPELLCHECKER
#include <Sonnet/SpellCheckDecorator>
#endif

#include "globalobject.h"
#include "mischelpers.h"

#ifdef HAVE_KCHARSELECT
#include "characterpicker.h"
#endif


class Composer : public QTextEdit
{
    Q_OBJECT

public:
    Composer(GlobalObject *globalObject, bool forPublisher,
             QWidget *parent = nullptr);
    ~Composer();

    void erase();
    void insertLink(QString url, QString title="");
    void requestCompletion(QString partialNick);

    void hideInfoMessage();
    QPushButton *getToolsButton();
#ifdef HAVE_KCHARSELECT
    QToolButton *getCharPickerButton();
#endif

    void setPlainPasteEnabled(bool state);


signals:
    void focusReceived();
    void editingFinished();
    void editingCancelled();
    void cancelSavingDraftRequested();
    void focusTitleRequested();

    void nickInserted(QString id, QString name, QString url, QString listType);

    void fileDropped(QString fileUrl);

    void errorHappened(QString errorMessage);


public slots:
    void makeNormal();
    void makeBold();
    void makeItalic();
    void makeUnderline();
    void makeStrikethrough();
    void makeHeader();
    void makeList();
    void makeTable();
    void makePreformatted();
    void makeQuote();
    void requestSpellConfigDialog();
    void makeLink();
    void insertImage();
    void insertLine();

    void insertSymbol(QAction *action);

#ifdef HAVE_KCHARSELECT
    void showCharacterPicker();
#endif

    void pasteAsPlaintext();

    void insertCompletedNick(QModelIndex nickData);

    void cancelPost();


protected:
    virtual void focusInEvent(QFocusEvent *event);
    virtual void dropEvent(QDropEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *event);
    virtual void insertFromMimeData(const QMimeData *source);


private:
    QPushButton *m_toolsButton;
    QMenu *m_toolsMenu;
    QMenu *m_symbolsMenu;

#ifdef HAVE_KCHARSELECT
    QToolButton *m_charPickerButton;
    CharacterPicker *m_characterPicker;
#endif

    QAction *m_pastePlaintextAction;

    QMenu *m_customContextMenu;

    QCompleter *m_nickCompleter;
    QTableView *m_popupTableView;

#ifdef HAVE_SONNET_SPELLCHECKER
    Sonnet::SpellCheckDecorator *m_spellDecorator;
#endif

    QString m_clickToPostString;
    bool m_forPublisher;


    GlobalObject *m_globalObject;
};

#endif // COMPOSER_H
