/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "hclabel.h"


HClabel::HClabel(QString initialText,
                 QWidget *parent) : QLabel(parent)
{
    this->setWordWrap(true);
    this->setAutoFillBackground(true);
    this->setTextFormat(Qt::RichText);

    this->setBaseText(initialText);

    this->setHighlighted(false); // Default
    this->setExpanded(false);


    m_toggleTimer = new QTimer(this);
    m_toggleTimer->setSingleShot(true);
    connect(m_toggleTimer, &QTimer::timeout,
            this, &HClabel::toggleContent);

    qDebug() << "HClabel created";
}

HClabel::~HClabel()
{
    qDebug() << "HClabel destroyed";
}


void HClabel::setHighlighted(bool highlighted)
{
    if (highlighted)
    {
        // Constant highlighting
        this->setStyleSheet("QLabel "
                            "{ color: palette(highlighted-text);               "
                            "  background-color: qlineargradient(spread:pad,   "
                            "                     x1:0, y1:0, x2:0, y2:1,      "
                            "                     stop:0   rgba(0, 0, 0, 0),   "
                            "                     stop:0.3 palette(highlight), "
                            "                     stop:0.7 palette(highlight), "
                            "                     stop:1   rgba(0, 0, 0, 0));  "
                            "  border-radius: 4px                              "
                            "}"
                            "QLabel:hover "
                            "{ color: palette(highlighted-text);    "
                            "  background-color: palette(highlight);"
                            "  border-radius: 4px                   "
                            "}");
    }
    else
    {
        // Highlight only on mouse hover
        this->setStyleSheet("QLabel "
                            "{ background-color: transparent }"
                            "QLabel:hover "
                            "{ color: palette(highlighted-text);    "
                            "  background-color: palette(highlight);"
                            "  border-radius: 4px                   "
                            "}");
    }
}


void HClabel::setExpanded(bool isExpanded)
{
    m_expanded = isExpanded;
}


void HClabel::setBaseText(QString text)
{
    m_baseText = text;

    if (m_extendedText.isEmpty() || !m_expanded)
    {
        this->setText(m_baseText);
    }
    else
    {
        this->setText(m_baseText
                      + "<hr>"
                      + m_extendedText
                      + "<hr>");
    }
}


void HClabel::setExtendedText(QString text)
{
    m_baseText = this->text(); // Save it
    m_extendedText = text.trimmed();
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////// SLOTS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void HClabel::toggleContent()
{
    if (m_expanded)
    {
        this->setText(m_baseText);
    }
    else
    {
        if (!m_extendedText.isEmpty())
        {
            this->setText(m_baseText
                          + "<hr>"
                          + m_extendedText
                          + "<hr>");
        }
    }

    this->setExpanded(!m_expanded);
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////// PROTECTED ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void HClabel::mousePressEvent(QMouseEvent *event)
{
    m_toggleTimer->start(200);

    // Don't emit the signal for now... we'll see if we need it
    //emit clicked();

    event->ignore(); // Let the click pass through to the parent
}
