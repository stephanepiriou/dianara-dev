/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QColorDialog>

#include <QDebug>


class ColorPicker : public QWidget
{
    Q_OBJECT

public:
    explicit ColorPicker(QString description,
                         QString initialColorString,
                         QWidget *parent = 0);
    ~ColorPicker();

    void setButtonColor(QColor color);
    QString getCurrentColor();


signals:


public slots:
    void changeColor();


private:
    QHBoxLayout *m_layout;

    QLabel *m_descriptionLabel;
    QCheckBox *m_checkBox;
    QPixmap m_buttonPixmap;
    QPushButton *m_button;

    QColor m_currentColor;
};

#endif // COLORPICKER_H
