/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef POST_H
#define POST_H

#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTextBrowser>
#include <QLabel>
#include <QIcon>
#include <QPixmap>
#include <QPushButton>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QFile>
#include <QVariantList>
#include <QMessageBox>
#include <QTimer>
#include <QSettings>

#include <QDebug>


#include "pumpcontroller.h"
#include "globalobject.h"
#include "timestamp.h"
#include "mischelpers.h"
#include "commenterblock.h"
#include "imageviewer.h"
#include "asactivity.h"
#include "avatarbutton.h"
#include "downloadwidget.h"
#include "hclabel.h"
#include "filtermatcheswidget.h"


class Post : public QFrame
{
    Q_OBJECT

public:
    Post(ASActivity *activity,
         bool highlightedByFilter,
         bool isStandalone,
         PumpController *pumpController,
         GlobalObject *globalObject,
         QWidget *parent);
    ~Post();

    void updateDataFromActivity(ASActivity *activity);
    void updateDataFromObject(ASObject *object);

    void updateCommentFromObject(ASObject *object);
    void setCommentDeletedFromObject(ASObject *object);

    void setPostContents();
    void onResizeOrShow();
    void setPostHeight();
    void getPendingImages();

    QString likesUrl();
    void setLikes(QVariantList likesList, int likesCount=-1);
    void appendLike(QString actorId, QString actorName, QString actorUrl);
    void removeLike(QString actorId);
    void refreshLikesInfo(int likesListSize, int likesCount);
    void setLikesLabel(int likesCount);


    QString commentsUrl();
    void setComments(QVariantList commentsList);
    void appendComment(ASObject *comment);
    void setCommentsLabel(int commentsCount);
    void updateBestCommentsUrl();


    QString sharesUrl();
    void setShares(QVariantList sharesList, int sharesCount=-1);
    void setSharesLabel(int resharesCount);


    void setPostUnreadStatus();
    void setPostAsNew();
    void setPostAsRead(bool informTimeline=true);
    void setPostDeleted(QString postDeletedTime);

    int getHighlightType();
    QString getActivityId();
    QString getObjectId();

    void setFuzzyTimestamps();
    void syncAvatarFollowState();

    bool isBeingCommented();
    bool isNew();

    enum PostHightlightType
    {
        NoHighlight = -1,
        MessageForUserHighlight,
        OwnMessageHighlight,
        FilterRulesHighlight
    };


signals:
    void postRead(bool wasHighlighted);
    void commentingOnPost(QWidget *commenterWidget);


public slots:
    void likePost(bool like);
    void fixLikeButton(bool isLiked);
    void getAllLikes();

    void commentOnPost();
    void sendComment(QString commentText);
    void updateComment(QString commentId, QString commentText);

    void requestCommenterComments();
    void getAllComments();
    void setAllComments(QVariantList commentsList,
                        QString originatingPostUrl);
    bool canGetAllComments();

    void sharePost();
    void unsharePost();

    void editPost();

    void deletePost();

    void joinGroup();

    void openClickedUrl(QUrl url);
    void showHighlightedUrl(QString url);


    void openPostInBrowser();
    void copyPostUrlToClipboard();
    void openParentPost();
    void normalizeTextFormat();

    void triggerResize();
    void delayedResize();

    void redrawImages(QString imageUrl);
    void onImageFailed(QString imageUrl);


protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void leaveEvent(QEvent *event);
    virtual void closeEvent(QCloseEvent *event);
    virtual void showEvent(QShowEvent *event);


private:
    QHBoxLayout *m_mainLayout;
    QVBoxLayout *m_leftColumnLayout;
    QVBoxLayout *m_rightColumnLayout;
    QVBoxLayout *m_outerLayout;

    QFrame *m_leftColumnFrame;
    QFrame *m_rightColumnFrame;

    QString m_activityId;
    QString m_postId;
    QString m_postType;
    QString m_postUrl;
    QString m_postAuthorId;
    QString m_postAuthorName;

    QString m_postShareInfoString;
    QString m_postSharedToCcString;
    QString m_postShareTime;

    bool m_postIsOwn;
    bool m_postIsUnread;
    bool m_postIsDeleted;
    int m_highlightType;
    QString m_unreadPostColor;

    AvatarButton *m_postAuthorAvatarButton;
    QAction *m_openInBrowserAction;
    QAction *m_copyPostUrlAction;
    QAction *m_normalizeTextAction;
    QAction *m_closeAction;

    QLabel *m_authorLabel;
    HClabel *m_timestampLabel;
    QLabel *m_generatorLabel;
    HClabel *m_locationLabel;
    QLabel *m_toLabel;
    QLabel *m_ccLabel;
    QLabel *m_shareInfoLabel;
    QLabel *m_shareHintLabel;

    HClabel *m_likesCountLabel;
    QLabel *m_commentsCountLabel;
    HClabel *m_sharesCountLabel;

    QPushButton *m_openParentButton;

    QLabel *m_titleLabel;
    QLabel *m_summaryLabel;
    QTextBrowser *m_bodyTextBrowser;

    QHBoxLayout *m_buttonsLayout;
    QPushButton *m_likeButton;
    QPushButton *m_commentButton;
    QPushButton *m_shareButton;
    QPushButton *m_editButton;
    QPushButton *m_deleteButton;

    QPushButton *m_joinLeaveButton;
    QLabel *m_groupInfoLabel;

    DownloadWidget *m_downloadWidget;
    bool m_haveDownloadWidget;

    FilterMatchesWidget *m_filterMatchesWidget;

    QString m_postTitle;
    QString m_postFullImageUrl;
    QString m_postImageUrl;
    QSize m_postImageSize;
    bool m_postImageIsAnimated;
    bool m_postImageFailed;
    QString m_postAudioUrl;
    QString m_postVideoUrl;
    QString m_postFileUrl;
    QString m_postFileMimeType;
    QString m_postAttachmentPureUrl;
    QString m_postOriginalText;
    QVariantMap m_postParentMap;

    QString m_timeCreatedAtString;
    QString m_timeUpdatedAtString;
    QString m_generatorString;

    int m_postWidth;

    QString m_likesUrl;
    int m_likesCount;
    QVariantMap m_likesMap;
    QString m_commentsUrl;
    QString m_sharesUrl;


    QStringList m_pendingImagesList;

    bool m_standalone;

    QString m_seeFullImageString;
    QString m_downloadAttachmentString;

    QTimer *m_resizeTimer;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
    CommenterBlock *m_commenterBlock;
};

#endif // POST_H
