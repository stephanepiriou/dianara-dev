/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PROFILEEDITOR_H
#define PROFILEEDITOR_H

#include <QWidget>
#include <QFormLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QMessageBox>
#include <QAction>
#include <QCloseEvent>

#include <QDebug>


#include "pumpcontroller.h"
#include "mischelpers.h"
#include "emailchanger.h"


class ProfileEditor : public QWidget
{
    Q_OBJECT

public:
    explicit ProfileEditor(PumpController *pumpController,
                           QWidget *parent = 0);
    ~ProfileEditor();

    void setProfileData(QString avatarUrl, QString fullName,
                        QString hometown, QString bio,
                        QString eMail);

    void setAvatar(QString filename);

    void toggleWidgetsEnabled(bool state);

signals:


public slots:
    void redrawAvatar(QString avatarUrl, QString avatarFilename);
    void findAvatarFile();
    void saveProfile();
    void sendProfileData(QString newImageUrl = QString());
    void enableSaveButton();


protected:
    virtual void closeEvent(QCloseEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QFormLayout *m_topLayout;
    QHBoxLayout *m_emailLayout;
    QHBoxLayout *m_avatarLayout;

    QLabel *m_webfingerLabel;

    QLabel *m_emailLabel;
    QPushButton *m_changeEmailButton;
    EmailChanger *m_emailChanger;

    QLabel *m_avatarLabel;
    QPushButton *m_changeAvatarButton;
    bool m_avatarHasChanged;

    QLineEdit *m_fullNameLineEdit;
    QLineEdit *m_hometownLineEdit;
    QTextEdit *m_bioTextEdit;

    QLabel *m_newAvatarInfoLabel;

    QPushButton *m_saveButton;
    QPushButton *m_cancelButton;
    QDialogButtonBox *m_buttonBox;

    QAction *m_cancelAction;

    QString m_currentAvatarUrl;
    QString m_oldAvatarFilename;
    QString m_newAvatarFilename;
    QString m_newAvatarContentType;

    PumpController *m_pumpController;
};

#endif // PROFILEEDITOR_H
