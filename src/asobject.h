/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ASOBJECT_H
#define ASOBJECT_H

#include <QObject>
#include <QVariantMap>

#include <QDebug>

#include "asperson.h"
#include "timestamp.h"


class ASObject : public QObject
{
    Q_OBJECT

public:
    explicit ASObject(QVariantMap objectMap,
                      QObject *parent = 0);
    ~ASObject();

    void updateAuthorFromPerson(ASPerson *person);

    ASPerson *author();

    QString getId();
    QString getType();
    static QString getTranslatedType(QString typeString);
    QString getUrl();
    QString getCreatedAt();
    QString getUpdatedAt();
    QString getLocationName();
    QString getLocationFormatted();
    QString getLocationCountry();
    QString getLocationTooltip();

    QString getDeletedTime();
    QString getDeletedOnString();
    static QString makeDeletedOnString(QString deletionTime);
    bool isLiked();

    QString getTitle();
    QString getSummary();
    QString getContent();

    QString getImageUrl();
    QString getSmallImageUrl();
    int getImageWidth();
    int getImageHeight();
    QString getAudioUrl();
    QString getVideoUrl();
    QString getFileUrl();
    QString getMimeType();
    QString getAttachmentPureUrl();

    int getMemberCount();
    QString getMemberUrl();

    int getLikesCount();
    int getCommentsCount();
    int getSharesCount();
    bool hasProxiedUrls();

    QVariantList getLastLikesList();
    QVariantList getLastCommentsList();
    QVariantList getLastSharesList();

    QString getLikesUrl();
    QString getCommentsUrl();
    QString getSharesUrl();

    QVariantMap getOriginalObject();
    QVariantMap getInReplyTo();
    QString getInReplyToId();

    // This one to deprecate
    static QString personStringFromList(QVariantList variantList,
                                        int count=-1);

    static QVariantMap simplePersonMapFromList(QVariantList variantList);
    static void addOnePersonToSimpleMap(QString personId, QString personName,
                                        QString personUrl, QVariantMap *personMap);
    static QString personStringFromSimpleMap(QVariantMap personMap,
                                             int totalCount);

    static bool canDisplayObject(QString objectType);


signals:

public slots:


private:
    ASPerson *m_author;

    QString m_id;
    QString m_type;
    QString m_url;
    QString m_createdAt;
    QString m_updatedAt;
    QString m_locationName;
    QString m_locationFormatted;
    QString m_locationCountry;

    QString m_deleted;
    bool m_liked;


    QString m_title;
    QString m_summary;
    QString m_content;

    QString m_imageUrl;
    QString m_smallImageUrl;
    int m_imageWidth;
    int m_imageHeight;
    QString m_audioUrl;
    QString m_videoUrl;
    QString m_fileUrl;
    QString m_mimeType;
    QString m_attachmentPureUrl; // To have filename with extension when using a proxyURL

    // Properties for group objects
    int m_memberCount;
    QString m_memberUrl;

    int m_likesCount;
    int m_commentsCount;
    int m_sharesCount;

    QVariantList m_lastLikesList;
    QVariantList m_lastCommentsList;
    QVariantList m_lastSharesList;

    QString m_likesUrl;
    QString m_commentsUrl;
    QString m_sharesUrl;
    bool m_proxiedUrls;

    QVariantMap m_originalObjectMap;
    QVariantMap m_inReplyToMap;
};

#endif // ASOBJECT_H
