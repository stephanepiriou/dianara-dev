/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PAGESELECTOR_H
#define PAGESELECTOR_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QSlider>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QAction>
#include <QHideEvent>

#include <QDebug>


class PageSelector : public QWidget
{
    Q_OBJECT

public:
    explicit PageSelector(QWidget *parent = 0);
    ~PageSelector();

    void showForPage(int currentPage, int totalPageCount);


signals:
    void pageJumpRequested(int pageNumber);


public slots:
    void setToFirstPage();
    void setToLastPage();
    void decreasePage();
    void increasePage();
    void goToPage();

    void onPageNumberEntered();
    void onPageNumberChanged(int selectedPage);



private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_topLayout;
    QHBoxLayout *m_middleLayout;
    QDialogButtonBox *m_bottomButtonBox;

    QLabel *m_messageLabel;
    QSpinBox *m_pageNumberSpinbox;
    QLabel *m_rangeLabel;

    QPushButton *m_firstButton;
    QPushButton *m_lastButton;

    QPushButton *m_newerButton;
    QSlider *m_pageNumberSlider;
    QPushButton *m_olderButton;

    QPushButton *m_goButton;
    QPushButton *m_closeButton;

    QAction *m_closeAction;

    int m_initialPage;
};

#endif // PAGESELECTOR_H
