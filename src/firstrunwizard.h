/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef FIRSTRUNWIZARD_H
#define FIRSTRUNWIZARD_H

#include <QWidget>
#include <QIcon>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QSettings>
#include <QCloseEvent>

#include <QDebug>

#include "accountdialog.h"
#include "profileeditor.h"
#include "configdialog.h"
#include "helpwidget.h"
#include "globalobject.h"


class FirstRunWizard : public QWidget
{
    Q_OBJECT

public:
    explicit FirstRunWizard(AccountDialog *accountDialog,
                            ProfileEditor *profileEditor,
                            ConfigDialog *configDialog,
                            HelpWidget *helpWidget,
                            GlobalObject *globalObject,
                            QWidget *parent = 0);
    ~FirstRunWizard();

signals:


public slots:


protected:
    virtual void closeEvent(QCloseEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_bottomLayout;

    QLabel *m_explanationLabel;
    QPushButton *m_configAccountButton;

    QLabel *m_editProfileLabel;
    QPushButton *m_editProfileButton;

    QLabel *m_publicPostsLabel;
    QCheckBox *m_publicPostsCheckbox;

    QPushButton *m_helpButton;

    QCheckBox *m_showAgainCheckbox;
    QPushButton *m_closeButton;

    // Widgets from MainWindow
    AccountDialog *m_accountDialog;
    ProfileEditor *m_profileEditor;
    ConfigDialog *m_configDialog;
    HelpWidget *m_helpWidget;
};

#endif // FIRSTRUNWIZARD_H
