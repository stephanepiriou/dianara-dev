Summary:	pump.io social network client
Name:		dianara
Version:	0.9
Release:	%mkrel 1
License:	GPLv2+
Group:		Networking/News
URL:		http://jancoding.wordpress.com/dianara/
Source0:	http://qt-apps.org/CONTENT/content-files/148103-dianara-v%{version}.tar.gz
BuildRequires:	libqt4-devel
BuildRequires:	qjson-devel
BuildRequires:	libqoauth-devel
BuildRequires:	imagemagick
Requires:	qt4-common
Requires:	libqjson
Requires:	libqoauth
Requires:	qca2-plugin-openssl

%description
Dianara is a pump.io client, a desktop application for GNU/linux
that allows users to manage their Pump.io social networking
accounts without the need to use a web browser.

%prep
%setup -qn %{name}-v%{version}

%build
qmake
%make

%install
%define apps 	%{_datadir}/applications/
%define pixmaps %{_datadir}/pixmaps/
%define locale	%{_datadir}/%{name}/locale/

rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}/
cp -p %{name} %{buildroot}%{_bindir}/

mkdir -p %{buildroot}%{apps}
cp -p %{name}.desktop %{buildroot}%{apps}

mkdir -p %{buildroot}%{pixmaps}
cp -p icon/64x64/%{name}.png %{buildroot}%{pixmaps}%{name}.png

mkdir -p %{buildroot}%{locale}
cp -p translations/*.qm %{buildroot}%{locale}

%files
%doc CHANGELOG LICENSE README INSTALL BUGS TODO
%{_bindir}/%{name}
%{apps}%{name}.desktop
%{pixmaps}%{name}.png
%{locale}*.qm

