##  Dianara - A Pump.io client
##  Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2 of the License, or
##  (at your option) any later version.
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the
##  Free Software Foundation, Inc.,
##  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
##
## -------------------------------------------------
## Project created by QtCreator
## -------------------------------------------------

message("Generating Makefile for Dianara... $$escape_expand(\\n)\
Using $$_FILE_$$escape_expand(\\n)")


QT       *= core gui widgets network
message("Building with Qt v$$QT_VERSION")


lessThan(QT_MAJOR_VERSION, 5) {
    warning(" >>> You're trying to build with Qt 4")
    warning(" >>> This version of Dianara requires Qt 5")
    warning(" >>> You might need to use qmake-qt5 instead $$escape_expand(\\n)")

    error("Aborting!")
}



TARGET = dianara

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050600


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/configdialog.cpp \
    src/notifications.cpp \
    src/post.cpp \
    src/timeline.cpp \
    src/publisher.cpp \
    src/composer.cpp \
    src/timestamp.cpp \
    src/contactcard.cpp \
    src/mischelpers.cpp \
    src/pumpcontroller.cpp \
    src/imageviewer.cpp \
    src/minorfeed.cpp \
    src/profileeditor.cpp \
    src/accountdialog.cpp \
    src/audienceselector.cpp \
    src/commenterblock.cpp \
    src/comment.cpp \
    src/minorfeeditem.cpp \
    src/listsmanager.cpp \
    src/asobject.cpp \
    src/asactivity.cpp \
    src/filtereditor.cpp \
    src/peoplewidget.cpp \
    src/logviewer.cpp \
    src/asperson.cpp \
    src/avatarbutton.cpp \
    src/colorpicker.cpp \
    src/filterchecker.cpp \
    src/contactmanager.cpp \
    src/contactlist.cpp \
    src/downloadwidget.cpp \
    src/proxydialog.cpp \
    src/helpwidget.cpp \
    src/fontpicker.cpp \
    src/groupsmanager.cpp \
    src/globalobject.cpp \
    src/pageselector.cpp \
    src/hclabel.cpp \
    src/userposts.cpp \
    src/emailchanger.cpp \
    src/siteuserslist.cpp \
    src/firstrunwizard.cpp \
    src/bannernotification.cpp \
    src/filtermatcheswidget.cpp \
    src/ivgraphicsview.cpp \
    src/draftsmanager.cpp \
    src/datafile.cpp

HEADERS  += src/mainwindow.h \
    src/configdialog.h \
    src/notifications.h \
    src/post.h \
    src/timeline.h \
    src/publisher.h \
    src/composer.h \
    src/timestamp.h \
    src/contactcard.h \
    src/mischelpers.h \
    src/pumpcontroller.h \
    src/imageviewer.h \
    src/minorfeed.h \
    src/profileeditor.h \
    src/accountdialog.h \
    src/audienceselector.h \
    src/commenterblock.h \
    src/comment.h \
    src/minorfeeditem.h \
    src/listsmanager.h \
    src/asobject.h \
    src/asactivity.h \
    src/filtereditor.h \
    src/peoplewidget.h \
    src/logviewer.h \
    src/asperson.h \
    src/avatarbutton.h \
    src/colorpicker.h \
    src/filterchecker.h \
    src/contactmanager.h \
    src/contactlist.h \
    src/downloadwidget.h \
    src/proxydialog.h \
    src/helpwidget.h \
    src/fontpicker.h \
    src/groupsmanager.h \
    src/globalobject.h \
    src/pageselector.h \
    src/hclabel.h \
    src/userposts.h \
    src/emailchanger.h \
    src/siteuserslist.h \
    src/firstrunwizard.h \
    src/bannernotification.h \
    src/filtermatcheswidget.h \
    src/ivgraphicsview.h \
    src/draftsmanager.h \
    src/datafile.h



# If D-Bus available, include D-Bus interface and enable Dbus-based notifications
qtHaveModule(dbus) {
    message("Building with D-Bus support (QtDBus module OK!) $$escape_expand(\\n)")
    QT += dbus

    SOURCES += src/dbusinterface.cpp
    HEADERS += src/dbusinterface.h
} else {
    warning(">>> QtDBus module not available! $$escape_expand(\\n)")
}


# SOURCE_DATE_EPOCH is read from environment, to enable reproducible builds in Debian
source_date_epoch = $$(SOURCE_DATE_EPOCH)
!isEmpty(source_date_epoch) {
    message("Creating a reproducible build (avoiding hardcoded timestamps) \
because SOURCE_DATE_EPOCH is defined: $$(SOURCE_DATE_EPOCH)")
    DEFINES += REPRODUCIBLEBUILD
}



OTHER_FILES += \
    CHANGELOG \
    README \
    org.nongnu.dianara.desktop \
    INSTALL \
    TODO \
    BUGS \
    TRANSLATING \
    manual/dianara.1 \
    translations/translation-status


TRANSLATIONS += translations/dianara_es.ts \
    translations/dianara_ca.ts \
    translations/dianara_gl.ts \
    translations/dianara_eu.ts \
    translations/dianara_fr.ts \
    translations/dianara_it.ts \
    translations/dianara_de.ts \
    translations/dianara_pt.ts \
    translations/dianara_ru.ts \
    translations/dianara_pl.ts \
    translations/dianara_he.ts \
    translations/dianara_EMPTY.ts

RESOURCES += dianara.qrc


load(oauth) {
    message("QtOAuth module found OK")
    CONFIG += oauth
} else {
    warning("QtOAuth module NOT found!")
    warning("Compilation will fail.")
    warning("If you have QtOAuth installed, your installation might be missing \
a .prf feature file.")
    warning("See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=825976")
}


# Enable optional KF5-based features, if available,
# unless CONFIG+=NO_KF5 is passed to qmake
!NO_KF5 {
    # Enable Sonnet spellcheck support, if necessary modules are installed
    qtHaveModule(SonnetCore) {
        message("Spellchecking support: SonnetCore module found OK")
        QT += SonnetCore

        qtHaveModule(SonnetUi) {
            message("Spellchecking support: SonnetUi module found OK")
            QT += SonnetUi
            DEFINES += HAVE_SONNET_SPELLCHECKER HAVE_KF5
            message("Spellchecking support: ENABLED")
        } else {
            message("Optional spellchecking support: DISABLED (SonnetUi module not found)")
        }
    } else {
        message("Optional spellchecking support: DISABLED (SonnetCore module not found)")
    }

    # Enable KWidgetsAddons, if the necessary module is installed, for KCharSelect
    qtHaveModule(KWidgetsAddons) {
        message("Character picker support: KWidgetsAddons module found OK")
        QT += KWidgetsAddons
        SOURCES += src/characterpicker.cpp
        HEADERS += src/characterpicker.h
        DEFINES += HAVE_KCHARSELECT HAVE_KF5
        message("Character picker (KCharSelect) support: ENABLED")
    } else {
        message("Optional character picker (KCharSelect) support: DISABLED")
        message("(KWidgetsAddons module not found)")
    }
}
else {
    message("* Disabling all KDE Frameworks support.")
    message("* Spellcheck support and character picker will not be available.")
}


# Console support under mswin
win32 {
    message("Enabling MSwin console support")
    CONFIG += console
}


## This is here so the makefile has a 'make install' target
target.path = /usr/bin/

desktop_file.files = org.nongnu.dianara.desktop
desktop_file.path = /usr/share/applications/

man_file.files = manual/dianara.1
man_file.path = /usr/share/man/man1/

appdata_file.files = appdata/org.nongnu.dianara.appdata.xml
appdata_file.path = /usr/share/metainfo/

icon32_png.files = icon/32x32/dianara.png
icon32_png.path = /usr/share/icons/hicolor/32x32/apps/

icon64_png.files = icon/64x64/dianara.png
icon64_png.path = /usr/share/icons/hicolor/64x64/apps/

INSTALLS += target \
            desktop_file \
            man_file \
            appdata_file \
            icon32_png \
            icon64_png



message("$$escape_expand(\\n\\n\\n)\
Makefile done!$$escape_expand(\\n\\n)\
If you're building the binary, you can run 'make' now. $$escape_expand(\\n)")
