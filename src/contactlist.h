/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QLineEdit>
#include <QPushButton>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "contactcard.h"


class ContactList : public QWidget
{
    Q_OBJECT

public:
    explicit ContactList(PumpController *pumpController,
                         GlobalObject *globalObject,
                         QString listType,
                         QWidget *parent = 0);
    ~ContactList();

    void clearListContents();
    void setListContents(QVariantList contactList);

    QString getContactsStringForExport();


signals:
    void contactCountChanged(int difference);


public slots:
    void filterList(QString filterText);

    void addSingleContact(ASPerson *contact);
    void removeSingleContact(ASPerson *contact);

    void focusFilterField();


private:
    QVBoxLayout *m_mainLayout;
    QVBoxLayout *m_contactsLayout;
    QHBoxLayout *m_filterLayout;

    QWidget *m_contactsWidget;
    QScrollArea *m_contactsScrollArea;

    QLabel *m_filterIcon;
    QLineEdit *m_filterLineEdit;
    QLabel *m_matchesCountLabel;
    QPushButton *m_clearFilterButton;
    QAction *m_focusFilterAction;


    QList<ContactCard *> m_contactsInList;
    bool m_isFollowing;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // CONTACTLIST_H
