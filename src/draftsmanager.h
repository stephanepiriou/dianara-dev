/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef DRAFTSMANAGER_H
#define DRAFTSMANAGER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QMenu>
#include <QAction>
#include <QListWidget>
#include <QLabel>
#include <QTextBrowser>
#include <QPushButton>
#include <QTime>
#include <QDate>
#include <QCloseEvent>
#include <QHideEvent>

#include <QDebug>

#include "globalobject.h"
#include "datafile.h"
#include "mischelpers.h"


class DraftsManager : public QWidget
{
    Q_OBJECT
public:
    explicit DraftsManager(GlobalObject *globalObject,
                           QWidget *parent = nullptr);
    ~DraftsManager();

    void loadDraftsFromFile();
    void saveDraftsToFile();

    void saveDraft(QString title, QString body,
                   QString type, QString attachment,
                   QVariantMap audience, int position);

    QString generateDraftId(QString title, QString body);
    void updateDraftId(QString id);

    QMenu *getDraftMenu();


signals:
    void draftSelected(QString id,
                       QString title, QString body,
                       QString type, QString attachment,
                       QVariantMap audience, int position);
    void saveDraftRequested();
    void windowShown();


public slots:
    void onDraftSelectedFromMenu(QAction *selectedAction);
    void onDraftSelectedFromList(int row);
    void deleteSelectedDraft();


protected:
    virtual void showEvent(QShowEvent *event);
    virtual void hideEvent(QHideEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_buttonsLayout;

    QListWidget *m_listWidget;
    QLabel *m_previewLabel;
    QPushButton *m_deleteButton;
    QPushButton *m_closeButton;

    QAction *m_closeAction;

    QMenu *m_draftsMenu;

    QMenu *m_loadMenu;
    QAction *m_saveAction;
    QAction *m_showManagerAction;


    QString m_currentDraftId;
    GlobalObject *m_globalObject;
};

#endif // DRAFTSMANAGER_H
