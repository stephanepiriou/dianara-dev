/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include <QObject>
#include <QVariantMap>
#include <QString>
#include <QIcon>

#ifdef QT_DBUS_LIB
#include <QtDBus>
#endif


#include <QDebug>

class FDNotifications : public QObject
{
    Q_OBJECT

public:
    FDNotifications(QObject *parent);
    ~FDNotifications();

    bool areNotificationsAvailable();
    void setNotificationOptions(int style, int duration,
                                bool notifyNewTL,
                                bool notifyHLTL,
                                bool notifyNewMW,
                                bool notifyHLMW,
                                bool notifyErr);
    void setCurrentUserId(QString newId);

    bool getNotifyNewTimeline();
    bool getNotifyHLTimeline();
    bool getNotifyNewMeanwhile();
    bool getNotifyHLMeanwhile();
    bool getNotifyErrors();


    enum notificationTypes
    {
        SystemNotifications,
        FallbackNotifications,
        NoNotifications
    };


signals:
    void showFallbackNotification(QString title, QString message,
                                  int duration);


public slots:
    void showMessage(QString message);


private:
#ifdef QT_DBUS_LIB
    QDBusConnection *m_busConnection;
#endif

    bool m_notificationsAvailable;

    int m_notificationType;
    int m_notificationDuration;

    bool m_notifyNewTimeline;
    bool m_notifyHLTimeline;
    bool m_notifyNewMeanwhile;
    bool m_notifyHLMeanwhile;
    bool m_notifyErrors;

    QString m_currentUserId;
};

#endif // NOTIFICATIONS_H
