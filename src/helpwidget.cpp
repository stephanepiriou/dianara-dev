/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "helpwidget.h"


HelpWidget::HelpWidget(QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Basic Help") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("system-help"));
    this->setWindowFlags(Qt::Window);
    this->setMinimumSize(320, 240);

    QSettings settings;
    this->resize(settings.value("HelpWidget/helpWindowSize",
                                QSize(640, 700)).toSize());

    ///////////////////////////////////////////////////////////////// Help text
    QString helpText;

    // Table of contents
    const QString sectionStarting    = tr("Getting started");
    const QString sectionSettings    = tr("Settings");
    const QString sectionTimelines   = tr("Timelines");
    const QString sectionPosting     = tr("Posting");
    const QString sectionContacts    = tr("Managing contacts");
    const QString sectionKeyboard    = tr("Keyboard controls");
    const QString sectionCommandLine = tr("Command line options");

    helpText.append("<h1>" + tr("Contents") + "</h1>"
                    "<ul>"
                    "<li><a href=#starting>"    + sectionStarting
                    + "</a></li>"
                    "<li><a href=#settings>"    + sectionSettings
                    + "</a></li>"
                    "<li><a href=#timelines>"   + sectionTimelines
                    + "</a></li>"
                    "<li><a href=#posting>"     + sectionPosting
                    + "</a></li>"
                    "<li><a href=#contacts>"    + sectionContacts
                    + "</a></li>"
                    "<li><a href=#keyboard>"    + sectionKeyboard
                    + "</a></li>"
                    "<li><a href=#commandline>" + sectionCommandLine
                    + "</a></li>"
                    "</ul>"
                    "<hr>");


    // Actual contents
    helpText.append("<a id=starting>"
                    "<h1><img src=:/images/button-next.png/> "
                    + sectionStarting
                    + "</h1></a>");

    helpText.append(tr("The first time you start Dianara, you should see "
                       "the Account Configuration dialog. There, enter "
                       "your Pump.io address as name@server and press "
                       "the Get Verifier Code button.")
                    + "<br><br>");
    helpText.append(tr("Then, your usual web browser should load the authorization "
                       "page in your Pump.io server. There, you'll have to copy "
                       "the full VERIFIER code, and paste it into Dianara's second field. "
                       "Then press Authorize Application, and once it's confirmed, press "
                       "Save Details.")
                    + "<br><br>");
    helpText.append(tr("At this point, your profile, contact lists and timelines "
                       "will be loaded.")
                    + "<br><br>");
    helpText.append(tr("You should take a look at the Program Configuration window, "
                       "under the Settings - Configure Dianara menu. There are "
                       "several interesting options there.")
                    + "<br><br>");
    helpText.append(tr("Keep in mind that there are a lot of places in Dianara "
                       "where you can get more information by hovering over some "
                       "text or button with your mouse, and waiting for the "
                       "tooltip to appear.")
                    + "<br><br>");
    helpText.append(tr("If you're new to Pump.io, take a look at this guide:")
                    + " <a href=\"https://pumpio.readthedocs.io/en/latest/userguide.html\">"
                    + tr("Pump.io User Guide")
                    + "</a>");


    helpText.append("<br><hr>");


    helpText.append("<a id=settings>"
                    "<h1><img src=:/images/button-configure.png/> "
                    + sectionSettings
                    + "</h1></a>");

    helpText.append(tr("You can configure several things to your liking in the "
                       "settings, like the time interval between timeline "
                       "updates, how many posts per page you want, highlight "
                       "colors, notifications or how the system tray icon looks.")
                    + "<br><br>");
    helpText.append(tr("Here, you can also activate the option to always publish "
                       "your posts as Public by default. You can always change "
                       "that at the moment of posting."));


    helpText.append("<br><hr>");


    helpText.append("<a id=timelines>"
                    "<h1><img src=:/images/feed-inbox.png/> "
                    + sectionTimelines
                    + "</h1></a>");

    helpText.append(tr("There are seven timelines:")
                    + "<ul>"
                    + "<li>"
                    + tr("The main timeline, where you'll see all the stuff "
                         "posted or shared by the people you follow.") + "</li>"
                    + "<li>"
                    + tr("Messages timeline, where you'll see messages sent "
                         "to you specifically. These messages might have been "
                         "sent to other people too.") + "</li>"
                    + "<li>"
                    + tr("Activity timeline, where you'll see your own posts, "
                         "or posts shared by you.") + "</li>"
                    + "<li>"
                    + tr("Favorites timeline, where you'll see the posts and "
                         "comments you've liked. This can be used as a "
                         "bookmark system.") + "</li>"
                    + "</ul>"
                    + "<br>");
    helpText.append(tr("The fifth timeline is the minor timeline, also known "
                       "as the Meanwhile. This is visible on the left side, "
                       "though it can be hidden. Here you'll see minor activities "
                       "done by everyone you follow, such as comment actions, "
                       "liking posts or following people.",
                       "LEFT SIDE should change to RIGHT SIDE on RTL languages")
                    + "<br>"
                    + tr("The sixth and seventh timelines are also minor "
                         "timelines, similar to the Meanwhile, but containing "
                         "only activities directly addressed to you (Mentions) "
                         "and activities done by you (Actions).")
                    + "<br><br>"
                    + tr("These activities might have a '+' button in them. "
                         "Press it to open the post they're referencing. "
                         "Also, as in many other places, you can hover with "
                         "your mouse to see relevant information in the tooltip.")
                    + "<br><br>"
                    + tr("New messages appear highlighted in a different color. "
                         "You can mark them as read just by clicking on any "
                         "empty parts of the message."));


    helpText.append("<br><hr>");


    helpText.append("<a id=posting>"
                    "<h1><img src=:/images/button-post.png/> "
                    + sectionPosting
                    + "</h1></a>");

    helpText.append(tr("You can post notes by clicking in the text field at "
                       "the top of the window or by pressing Control+N. "
                       "Setting a title for your post is optional, but "
                       "highly recommended, as it will help to "
                       "better identify references to your post in "
                       "the minor feed, e-mail notifications, etc.")
                    + "<br><br>");
    helpText.append(tr("It is possible to attach images, audio, video, and "
                       "general files, like PDF documents, to your post.")
                    + "<br>");
    helpText.append("<center>"
                    "<img src=:/images/attached-audio.png/> "
                    "<img src=:/images/attached-image.png/> "
                    "<img src=:/images/attached-video.png/>"
                    "</center>"
                    "<br><br>");
    helpText.append(tr("You can use the Format button to add formatting to "
                       "your text, like bold or italics. Some of these options "
                       "require text to be selected before they are used.")
                    + "<br><br>");
    helpText.append(tr("You can select who will see your post by using the "
                       "To and Cc buttons.")
                    + " "
                    + tr("If you add a specific person to the 'To' list, they "
                         "will receive your message in their direct messages tab.")
                    + "<br>");
    helpText.append(tr("You can also type '@' and the first characters of the "
                       "name of a contact to bring up a popup menu with "
                       "matching choices.")
                    + " "
                    + tr("Choose one with the arrow keys and press Enter to "
                         "complete the name. This will add that person to the "
                         "recipients list.")
                    + "<br><br>");
    helpText.append(tr("You can create private messages by adding specific "
                       "people to these lists, and unselecting the Followers "
                       "or the Public options."));


    helpText.append("<br><hr>");


    helpText.append("<a id=contacts>"
                    "<h1><img src=:/images/button-users.png/> "
                    + sectionContacts
                    + "</h1></a>");

    helpText.append(tr("You can see the lists of people you follow, and who "
                       "follow you from the Contacts tab.")
                    + " "
                    + tr("There, you can also manage person lists, used mainly "
                         "to send posts to specific groups of people.")
                    + " "
                    + tr("There is a text field at the top, where you can "
                         "directly enter addresses of new contacts to "
                         "follow them.")
                    + "<br><br>"
                    + tr("Under the 'Neighbors' tab you'll see some resources "
                         "to find people, and have the option to browse the "
                         "latest registered users from your server directly.")
                    + "<br><br><br"
                    + tr("You can click on any avatars in the posts, the "
                         "comments, and the Meanwhile column, and you will get "
                         "a menu with several options, one of which is following "
                         "or unfollowing that person.")
                    + " "
                    + tr("You can also send a direct message (initially "
                         "private) to that contact from this menu.")
                    + "<br><br>");
    helpText.append(tr("You can find a list with some Pump.io users "
                       "and other information here:")
                    + "<br>"
                      "<a href=\"https://github.com/pump-io/pump.io/wiki/Users-by-language\">"
                    + tr("Users by language")
                    + "</a> - <a href=\"https://pumpit.info/pumpio/followers\">"
                    + tr("Followers of Pump.io Community account")
                    + "</a>");


    helpText.append("<br><hr>");


    helpText.append("<a id=keyboard>"
                    "<h1><img src=:/images/button-edit.png/> " // FIXME TMP
                    + sectionKeyboard
                    + "</h1></a>");

    helpText.append(tr("The most common actions found on the menus have "
                       "keyboard shortcuts written next to them, like F5 "
                       "or Control+N.")
                    + "<br><br>"
                    + tr("Besides that, you can use:")
                    + "<ul>"
                    + "<li>"
                    + tr("Control+Up/Down/PgUp/PgDown/Home/End to move "
                         "around the timeline.") + "</li>"
                    + "<li>"
                    + tr("Control+Left/Right to jump one page in the "
                         "timeline.") + "</li>"
                    + "<li>"
                    + tr("Control+G to go to any page in the timeline "
                         "directly.") + "</li>"
                    + "<li>"
                    + tr("Control+1/2/3 to switch between the minor "
                         "feeds.") + "</li>"
                    + "<li>"
                    + tr("Control+Enter to post, when you're done composing "
                         "a note or a comment. If the note is empty, you can "
                         "cancel it by pressing ESC.") + "</li>"
                    + "<li>"
                    + tr("While composing a note, press Enter to jump from the "
                         "title to the message body. Also, pressing the Up "
                         "arrow while you're at the start of the message, jumps "
                         "back to the title.") + "</li>"
                    + "<li>"
                    + tr("Control+Enter to finish creating a list of recipients "
                         "for a post, in the 'To' or 'Cc' lists.") + "</li>"
                    + "</ul>");


    helpText.append("<br><hr>");


    helpText.append("<a id=commandline>"
                    "<h1><img src=:/images/button-configure.png/> "
                    + sectionCommandLine
                    + "</h1></a>");

    helpText.append(tr("You can use the --config parameter to run the program "
                       "with a different configuration. This can be useful to "
                       "use two or more different accounts. You can even run two "
                       "instances of Dianara at the same time.")
                    + "<br><br>");
    helpText.append(tr("Use the --debug parameter to have extra information "
                       "in your terminal window, about what the program is doing.")
                    + "<br><br>");
    helpText.append(tr("If your server does not support HTTPS, you can use the "
                       "--nohttps parameter.")
                    + "<br><br><br>");

    helpText.append(tr("Dianara offers a D-Bus interface that allows some "
                       "control from other applications.")
                    + " "
                    + tr("The interface is at %1, and you can "
                         "access it with tools such as %2 or %3. It "
                         "offers methods like %4 and %5.")
                      .arg("<b>org.nongnu.dianara</b>")
                      .arg("<b>qdbus</b>").arg("<b>dbus-send</b>")
                      .arg("<b>'toggle'</b>").arg("<b>'post'</b>")
                    + "<br>"
                    + tr("If you use an alternate configuration, with "
                         "something like '--config otherconf', then the "
                         "interface will be at org.nongnu.dianara_otherconf."));


    helpText.append("<br><br>");

    ///////////////////////////////////////////////////////////////// Help text


    m_helpTextBrowser = new QTextBrowser(this);
    m_helpTextBrowser->setReadOnly(true);
    m_helpTextBrowser->setOpenExternalLinks(true);
    m_helpTextBrowser->setText(helpText);

    m_closeButton = new QPushButton(QIcon::fromTheme("window-close",
                                                     QIcon(":/images/button-close.png")),
                                    tr("&Close"),
                                    this);
    connect(m_closeButton, &QAbstractButton::clicked,
            this, &QWidget::hide);

    QList<QKeySequence> closeShortcuts;
    closeShortcuts << QKeySequence(Qt::Key_Escape);
    closeShortcuts << QKeySequence(Qt::Key_F1);
    m_closeAction = new QAction(this);
    m_closeAction->setShortcuts(closeShortcuts);
    connect(m_closeAction, &QAction::triggered,
            this, &QWidget::hide);
    this->addAction(m_closeAction);

    m_layout = new QVBoxLayout();
    m_layout->setContentsMargins(2, 2, 2, 2);
    m_layout->addWidget(m_helpTextBrowser);
    m_layout->addWidget(m_closeButton, 0, Qt::AlignRight);
    this->setLayout(m_layout);

    qDebug() << "HelpWidget created";
}

HelpWidget::~HelpWidget()
{
    qDebug() << "HelpWidget destroyed";
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////// PROTECTED ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void HelpWidget::closeEvent(QCloseEvent *event)
{
    this->hide();
    event->ignore();
}

void HelpWidget::hideEvent(QHideEvent *event)
{
    QSettings settings;
    if (settings.isWritable())
    {
        settings.setValue("HelpWidget/helpWindowSize", this->size());
    }

    event->accept();
}
