/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef LISTSMANAGER_H
#define LISTSMANAGER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QTreeWidget>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QMessageBox>

#include <QInputDialog> // TMP!!

#include <QDebug>

#include "pumpcontroller.h"
#include "peoplewidget.h"


class ListsManager : public QWidget
{
    Q_OBJECT

public:
    explicit ListsManager(PumpController *pumpController,
                          QWidget *parent = 0);
    ~ListsManager();

    void setListsList(QVariantList listsList);

    QTreeWidgetItem *createPersonItem(QString id,
                                      QString name,
                                      QString avatarFile);

signals:


public slots:
    void setPersonsInList(QVariantList personList, QString listUrl);
    void createList();
    void deleteList();
    void enableDisableCreateButton(QString listName);

    void enableDisableDeleteButtons();

    void showAddPersonDialog();
    void addPerson(QIcon icon, QString contactString,
                   QString contactName, QString contactId, QString contactUrl);
    void removePerson();

    void addPersonItemToList(QString personId, QString personName,
                             QString avatarUrl);
    void removePersonItemFromList(QString personId);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_buttonsLayout;

    QTreeWidget *m_listsTreeWidget;
    QPushButton *m_deleteListButton;

    QPushButton *m_addPersonButton;
    QPushButton *m_removePersonButton;

    PeopleWidget *m_peopleWidget;

    QGroupBox *m_newListGroupbox;
    QHBoxLayout *m_groupboxMainLayout;
    QVBoxLayout *m_groupboxLeftLayout;

    QLineEdit *m_newListNameLineEdit;
    QTextEdit *m_newListDescTextEdit;
    QPushButton *m_createListButton;


    QStringList m_personListsUrlList;

    PumpController *m_pumpController;
};


#endif // LISTSMANAGER_H
