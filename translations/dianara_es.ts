<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="89"/>
        <location filename="../src/asactivity.cpp" line="135"/>
        <source>Public</source>
        <translation>Público</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="422"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 de %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="275"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="280"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>Artículo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="285"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="290"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="295"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="300"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="305"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="310"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="315"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>Colección</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="320"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>Otro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="376"/>
        <source>No detailed location</source>
        <translation>No hay ubicación detallada</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="402"/>
        <source>Deleted on %1</source>
        <translation>Eliminado el %1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="578"/>
        <location filename="../src/asobject.cpp" line="648"/>
        <source>and one other</source>
        <translation>y uno más</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="582"/>
        <location filename="../src/asobject.cpp" line="652"/>
        <source>and %1 others</source>
        <translation>y %1 más</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="173"/>
        <source>Hometown</source>
        <translation>Ciudad</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="85"/>
        <source>Your Pump.io address:</source>
        <translation>Tu dirección pump.io:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="95"/>
        <source>Get &amp;Verifier Code</source>
        <translation>Obtener código de &amp;verificación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="128"/>
        <source>Verifier code:</source>
        <translation>Código de verificación:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="131"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation>Introduce o pega aquí el codigo de verificación proporcionado por tu servidor Pump</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="168"/>
        <source>&amp;Save Details</source>
        <translation>&amp;Guardar datos</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="401"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>Si el navegador web no se abre automáticamente, copia esta dirección manualmente</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>Configuración de la cuenta</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="50"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>Primero, introduce tu identificador Webfinger, tu dirección pump.io.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="53"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>Tu dirección es como usuario@servidorpump.org, y puedes encontrarla en tu perfil, en la interfaz web.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="58"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>Si tu perfil está en https://pump.ejemplo/tunombre, entonces tu dirección es tunombre@pump.ejemplo</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="62"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>Si aún no tienes una cuenta, puedes registrar una en %1. Este enlace te llevará a un servidor público aleatorio.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="70"/>
        <source>If you need help: %1</source>
        <translation>Si necesitas ayuda: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="73"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="88"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation>Tu dirección, como usuario@servidorpump.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="98"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>Cuando pulses este botón, se abrirá un navegador web, solicitando autorización para Dianara</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="109"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>Una vez que hayas autorizado a Dianara desde la interfaz web de tu servidor Pump, recibirás un código llamado VERIFIER.
Cópialo y pégalo en el campo de abajo.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="141"/>
        <source>&amp;Authorize Application</source>
        <translation>&amp;Autorizar la aplicación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="176"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="188"/>
        <source>Your account is properly configured.</source>
        <translation>Tu cuenta está configurada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="192"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>Pulsa Desbloquear si quieres configurar una cuenta diferente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="200"/>
        <source>&amp;Unlock</source>
        <translation>&amp;Desbloquear</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="327"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>Ahora se iniciará un navegador web, donde podrás obtener el código de verificación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="336"/>
        <source>Your Pump address is invalid</source>
        <translation>Tu dirección Pump no es válida</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="355"/>
        <source>Verifier code is empty</source>
        <translation>El código de verificación está vacío</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="382"/>
        <source>Dianara is authorized to access your data</source>
        <translation>Dianara tiene autorización para acceder a tu cuenta</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="410"/>
        <source>Unable to open web browser!</source>
        <translation>¡No se ha podido abrir el navegador web!</translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="33"/>
        <source>&apos;To&apos; List</source>
        <translation>Lista &apos;Para&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="37"/>
        <source>&apos;Cc&apos; List</source>
        <translation>Lista &apos;Cc&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="58"/>
        <source>&amp;Add to Selected</source>
        <translation>&amp;Añadir a seleccionados</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="73"/>
        <source>All Contacts</source>
        <translation>Todos los contactos</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="79"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <comment>ON THE LEFT should change to ON THE RIGHT in RTL languages</comment>
        <translation>Selecciona gente de la lista de la izquierda.
Puedes arrastrarlos con el ratón, hacer clic o doble clic en ellos, o seleccionarlos y usar el botón de abajo.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="96"/>
        <source>Clear &amp;List</source>
        <translation>Borrar &amp;lista</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="120"/>
        <source>&amp;Done</source>
        <translation>&amp;Hecho</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="127"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="168"/>
        <location filename="../src/audienceselector.cpp" line="386"/>
        <source>Public</source>
        <translation>Público</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="173"/>
        <location filename="../src/audienceselector.cpp" line="392"/>
        <source>Followers</source>
        <translation>Seguidores</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="178"/>
        <source>Lists</source>
        <translation>Listas</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="188"/>
        <source>People...</source>
        <translation>Personas...</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="108"/>
        <source>Selected People</source>
        <translation>Gente seleccionada</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="143"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>Abrir el perfil de %1 en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="139"/>
        <source>Open your profile in web browser</source>
        <translation>Abrir tu perfil en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="165"/>
        <source>Send message to %1</source>
        <translation>Enviar mensaje a %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="174"/>
        <source>Browse messages</source>
        <translation>Ver mensajes</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="230"/>
        <source>Stop following</source>
        <translation>Dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="241"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="287"/>
        <source>Stop following?</source>
        <translation>¿Dejar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="288"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>¿Estás seguro de que quieres dejar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="291"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="292"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>BannerNotification</name>
    <message>
        <location filename="../src/bannernotification.cpp" line="38"/>
        <source>Timelines were not automatically updated to avoid interruptions.</source>
        <translation>No se han actualizado las líneas temporales automáticamente para evitar interrupciones.</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="47"/>
        <source>This happens when it is time to autoupdate the timelines, but you are not at the top of the first page, to avoid interruptions while you read</source>
        <translation>Esto pasa cuando llega el momento de autoactualizar las líneas temporales, pero no estás al principio de la primera página, para evitar interrupciones mientras lees</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="55"/>
        <source>Update now</source>
        <translation>Actualizar ahora</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="65"/>
        <source>Hide this message</source>
        <translation>Ocultar este mensaje</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="35"/>
        <source>Change...</source>
        <translation>Cambiar...</translation>
    </message>
    <message>
        <location filename="../src/colorpicker.cpp" line="114"/>
        <source>Choose a color</source>
        <translation>Elige un color</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="251"/>
        <source>Posted on %1</source>
        <translation>Publicado el %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="256"/>
        <source>Modified on %1</source>
        <translation>Modificado el %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="114"/>
        <source>Like or unlike this comment</source>
        <translation>Decir que te gusta o que ya no te gusta este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="121"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>Citar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="129"/>
        <source>Reply quoting this comment</source>
        <translation>Responder citando este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="136"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="142"/>
        <source>Modify this comment</source>
        <translation>Modificar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="148"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="154"/>
        <source>Erase this comment</source>
        <translation>Borrar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="287"/>
        <source>Unlike</source>
        <translation>Ya no me gusta</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="291"/>
        <source>Like</source>
        <translation>Me gusta</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="311"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>A %1 les gusta este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="305"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>A %1 le gusta este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="547"/>
        <source>WARNING: Delete comment?</source>
        <translation>ADVERTENCIA: ¿Eliminar comentario?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="548"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>¿Estás seguro de que quieres eliminar este comentario?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminarlo</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="127"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>Puedes pulsar Control+Enter para enviar el comentario con el teclado</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="42"/>
        <source>Reload comments</source>
        <translation>Actualizar comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="123"/>
        <location filename="../src/commenterblock.cpp" line="510"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="134"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="137"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>Pulsa ESC para cancelar el comentario si no hay texto</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="363"/>
        <source>Check for comments</source>
        <translation>Comprobar comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="367"/>
        <source>Show all %1 comments</source>
        <translation>Mostrar los %1 comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="393"/>
        <source>Comments are not available</source>
        <translation>Los comentarios no están disponibles</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="558"/>
        <source>Error: Already composing</source>
        <translation>Error: Ya se está redactando</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="559"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>No puedes editar un comentario en este momento, porque ya se está redactando otro comentario.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="574"/>
        <source>Editing comment</source>
        <translation>Editando comentario</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="583"/>
        <source>Loading comments...</source>
        <translation>Cargando comentarios...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="650"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>Ha fallado la publicación del comentario.

Prueba de nuevo.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="667"/>
        <source>An error occurred</source>
        <translation>Ha ocurrido un error</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="691"/>
        <source>Sending comment...</source>
        <translation>Enviando comentario...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="696"/>
        <source>Updating comment...</source>
        <translation>Actualizando comentario...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="705"/>
        <source>Comment is empty.</source>
        <translation>El comentario está vacío.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="65"/>
        <source>Italic</source>
        <translation>Cursiva</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="107"/>
        <source>Make a link</source>
        <translation>Hacer un enlace</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="33"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>Haz clic aquí o pulsa Control+N para publicar una nota...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="175"/>
        <source>Type a message here to post it</source>
        <translation>Escribe un mensaje aquí para publicarlo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="56"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="37"/>
        <source>Symbols</source>
        <translation>Símbolos</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="55"/>
        <source>Formatting</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="70"/>
        <source>Underline</source>
        <translation>Subrayado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="75"/>
        <source>Strikethrough</source>
        <translation>Tachado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="82"/>
        <source>Header</source>
        <translation>Cabecera</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="87"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="91"/>
        <source>Table</source>
        <translation>Tabla</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="95"/>
        <source>Preformatted block</source>
        <translation>Bloque preformateado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="99"/>
        <source>Quote block</source>
        <translation>Bloque de cita</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="112"/>
        <source>Insert an image from a web site</source>
        <translation>Insertar una imagen desde un sitio web</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="117"/>
        <source>Insert line</source>
        <translation>Insertar línea</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="342"/>
        <source>You can attach only one file.</source>
        <translation>Solo puedes adjuntar un archivo.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="350"/>
        <source>You cannot drop folders here, only a single file.</source>
        <translation>No puedes soltar carpetas aquí, solo un único archivo.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="500"/>
        <source>Insert as image?</source>
        <translation>¿Insertar como imagen?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="501"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>Parece que el enlace que estás pegando apunta a una imagen.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="504"/>
        <source>Insert as visible image</source>
        <translation>Insertar como imagen visible</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="505"/>
        <source>Insert as link</source>
        <translation>Insertar como enlace</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="734"/>
        <source>Table Size</source>
        <translation>Tamaño de la tabla</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="738"/>
        <source>How many rows (height)?</source>
        <translation>¿Cuántas filas (altura)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="749"/>
        <source>How many columns (width)?</source>
        <translation>¿Cuántas columnas (ancho)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="833"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>Escribe o pega una dirección web aquí.
También puedes seleccionar texto antes, para convertirlo en un enlace.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="878"/>
        <source>Invalid link</source>
        <translation>Enlace no válido</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="879"/>
        <source>The text you entered does not look like a link.</source>
        <translation>El texto que has introducido no parece un enlace.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="882"/>
        <source>It should start with one of these types:</source>
        <comment>It = the link, from previous sentence</comment>
        <translation>Debería comenzar con uno de estos tipos:</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="896"/>
        <source>&amp;Use it anyway</source>
        <translation>&amp;Usarlo igualmente</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="897"/>
        <source>&amp;Enter it again</source>
        <translation>&amp;Introducirlo de nuevo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="898"/>
        <source>&amp;Cancel link</source>
        <translation>&amp;Cancelar el enlace</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="932"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>Escribe o pega la dirección de la imagen aquí.
El enlace ha de apuntar al archivo de imagen directamente.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1052"/>
        <source>Yes, but saving a &amp;draft</source>
        <translation>Sí, pero guardando un &amp;borrador</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="134"/>
        <source>Text Formatting Options</source>
        <translation>Opciones de formato de texto</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="139"/>
        <source>Paste Text Without Formatting</source>
        <translation>Pegar texto sin formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="931"/>
        <source>Insert an image from a URL</source>
        <translation>Insertar una imagen desde una URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="949"/>
        <source>Error: Invalid URL</source>
        <translation>Error: URL no válida</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="950"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation>La dirección que has introducido (%1) no es válida.
Las direcciones de imágenes han de empezar por http:// o https://</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1057"/>
        <source>Cancel message?</source>
        <translation>¿Cancelar el mensaje?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1058"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation>¿Seguro que quieres cancelar este mensaje?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1060"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;Sí, cancelarlo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1062"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="832"/>
        <source>Insert a link</source>
        <translation>Insertar un enlace</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="129"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="181"/>
        <source>Type a comment here</source>
        <translation>Escribe un comentario aquí</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="848"/>
        <source>Make a link from selected text</source>
        <translation>Hacer un link con el texto seleccionado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="849"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>Escribe o pega una dirección web aquí.
El texto seleccionado (%1) será convertido en un enlace.</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="245"/>
        <source>minutes</source>
        <translation>minutos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Top</source>
        <translation>Parte superior</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="253"/>
        <source>Bottom</source>
        <translation>Parte inferior</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>Configuración del programa</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="289"/>
        <source>Timeline &amp;update interval</source>
        <translation>Intervalo de &amp;actualización de la línea temporal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="292"/>
        <source>&amp;Tabs position</source>
        <translation>Posición de las &amp;pestañas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="294"/>
        <source>&amp;Movable tabs</source>
        <translation>Pes&amp;tañas movibles</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="322"/>
        <source>Minor Feeds</source>
        <translation>Líneas temporales menores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="464"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>&amp;Mensajes por página, línea temporal principal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="407"/>
        <location filename="../src/configdialog.cpp" line="414"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="466"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>Mensajes por página, &amp;otras líneas temporales</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="566"/>
        <source>Public posts as &amp;default</source>
        <translation>Mensajes públicos de manera &amp;predefinida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="268"/>
        <source>Pro&amp;xy Settings</source>
        <translation>Ajustes de pro&amp;xy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="297"/>
        <source>Network configuration</source>
        <translation>Configuración de red</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="275"/>
        <source>Set Up F&amp;ilters</source>
        <translation>Configurar f&amp;iltros</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="299"/>
        <source>Filtering rules</source>
        <translation>Normas de filtrado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="430"/>
        <source>Highlighted activities, except mine</source>
        <translation>Actividades destacadas, excepto las mías</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="431"/>
        <source>Any highlighted activity</source>
        <translation>Cualquier actividad destacada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="432"/>
        <source>Always</source>
        <translation>Siempre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="433"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="438"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>caracteres</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="477"/>
        <source>Snippet limit</source>
        <translation>Límite de los fragmentos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="313"/>
        <source>Post Titles</source>
        <translation>Títulos de los mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="316"/>
        <source>Post Contents</source>
        <translation>Contenido de los mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="319"/>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="347"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>Estás entre los destinatarios de la actividad, como por ejemplo un comentario dirigido a ti.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="351"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation>Usado también cuando se destacan mensajes dirigidos a ti en las líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="356"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>La actividad es en respuesta a alguna cosa hecha por ti, como un comentario publicado en respuesta a una de tus notas.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="362"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation>Eres el objeto de la actividad, como por ejemplo cuando alguien te añade a una lista.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="367"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>La actividad está relacionada con uno de tus objetos, como por ejemplo cuando a alguien le gusta uno de tus mensajes.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="371"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>Usado también cuando se destacan tus propios mensajes en las líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="376"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>Elemento destacado por normas de filtrado.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="381"/>
        <source>Item is new.</source>
        <translation>El elemento es nuevo.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="475"/>
        <source>Show snippets in minor feeds</source>
        <translation>Mostrar fragmentos en las líneas temporales menores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="470"/>
        <source>Hide duplicated posts</source>
        <translation>Ocultar mensajes duplicados</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="451"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="452"/>
        <source>Before avatar</source>
        <translation>Antes del avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="453"/>
        <source>Before avatar, subtle</source>
        <translation>Antes del avatar, sutil</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="454"/>
        <source>After avatar</source>
        <translation>Después del avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="455"/>
        <source>After avatar, subtle</source>
        <translation>Después del avatar, sutil</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="479"/>
        <source>Snippet limit when highlighted</source>
        <translation>Límite de los fragmentos destacados</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="483"/>
        <source>Show activity icons</source>
        <translation>Mostrar iconos de actividad</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="527"/>
        <source>Avatar size</source>
        <translation>Tamaño de los avatares</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="531"/>
        <source>Show extended share information</source>
        <translation>Mostrar información adicional en los compartidos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="533"/>
        <source>Show extra information</source>
        <translation>Mostrar información extra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="535"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>Destacar comentarios del autor del mensaje</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="537"/>
        <source>Highlight your own comments</source>
        <translation>Destacar tus propios comentarios</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="539"/>
        <source>Ignore SSL errors in images</source>
        <translation>Ignorar errores de SSL en las imágenes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="571"/>
        <source>Show character counter</source>
        <translation>Mostrar contador de caracteres</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="596"/>
        <source>Don&apos;t inform followers when following someone</source>
        <translation>No informar a los seguidores al seguir a alguien</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="599"/>
        <source>Don&apos;t inform followers when handling lists</source>
        <translation>No informar a los seguidores al manipular listas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="619"/>
        <source>As system notifications</source>
        <translation>Como notificaciones del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="621"/>
        <source>Using own notifications</source>
        <translation>Usando notificaciones propias</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="623"/>
        <source>Don&apos;t show notifications</source>
        <translation>No mostrar notificaciones</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="637"/>
        <source>seconds</source>
        <comment>Next to a duration, in seconds</comment>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="675"/>
        <source>Notification Style</source>
        <translation>Estilo de las notificaciones</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="679"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="681"/>
        <source>Persistent Notifications</source>
        <translation>Notificaciones persistentes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="683"/>
        <source>Also highlight taskbar entry</source>
        <translation>Destacar también en la barra de tareas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="689"/>
        <source>Notify when receiving:</source>
        <translation>Notificar cuando se reciban:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="693"/>
        <source>New posts</source>
        <translation>Mensajes nuevos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="695"/>
        <source>Highlighted posts</source>
        <translation>Mensajes destacados</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="697"/>
        <source>New activities in minor feed</source>
        <translation>Nuevas actividades en la línea temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="699"/>
        <source>Highlighted activities in minor feed</source>
        <translation>Actividades destacadas en la línea temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="701"/>
        <source>Important errors</source>
        <translation>Errores importantes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="716"/>
        <source>Default</source>
        <translatorcomment>el icono</translatorcomment>
        <translation>Predefinido</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="717"/>
        <source>System iconset, if available</source>
        <translation>Icono del sistema, si está disponible</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="718"/>
        <source>Show your current avatar</source>
        <translation>Mostrar tu avatar actual</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="719"/>
        <source>Custom icon</source>
        <translation>Icono personalizado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="753"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>&amp;Tipo de icono en la bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="757"/>
        <source>Hide window on startup</source>
        <translation>Ocultar ventana al iniciar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="113"/>
        <source>General Options</source>
        <translation>Opciones generales</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="117"/>
        <source>Fonts</source>
        <translation>Tipos de letra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="121"/>
        <source>Colors</source>
        <translation>Colores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="125"/>
        <source>Timelines</source>
        <translation>Líneas temporales</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="129"/>
        <source>Posts</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="133"/>
        <source>Composer</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="137"/>
        <source>Privacy</source>
        <translation>Privacidad</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="141"/>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="145"/>
        <source>System Tray</source>
        <translation>Bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="481"/>
        <source>Minor feed avatar sizes</source>
        <translation>Tamaño de avatares en líneas temporales menores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="529"/>
        <source>Avatar size in comments</source>
        <translation>Tamaño de los avatares en comentarios</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="726"/>
        <source>S&amp;elect...</source>
        <translation>S&amp;eleccionar...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="755"/>
        <source>Custom &amp;Icon</source>
        <translation>&amp;Icono personalizado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1027"/>
        <source>Select custom icon</source>
        <translation>Selecciona icono personalizado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="175"/>
        <source>Dianara stores data in this folder:</source>
        <translation>Dianara guarda datos en esta carpeta:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="255"/>
        <source>Left side</source>
        <comment>tabs on left side/west; RTL not affected</comment>
        <translation>Lado izquierdo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="258"/>
        <source>Right side</source>
        <comment>tabs on right side/east; RTL not affected</comment>
        <translation>Lado derecho</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="468"/>
        <source>Show information for deleted posts</source>
        <translation>Mostrar información de los mensajes eliminados</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="472"/>
        <source>Jump to new posts line on update</source>
        <translation>Saltar a la línea de nuevos mensajes al actualizar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="515"/>
        <source>Only for images inserted from web sites.</source>
        <translation>Sólo para imágenes insertadas desde sitios web.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="518"/>
        <source>Use with care.</source>
        <translation>Usar con cuidado.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="541"/>
        <source>Show full size images</source>
        <translation>Mostrar imágenes a tamaño completo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="568"/>
        <source>Use attachment filename as initial post title</source>
        <translation>Usar nombre del adjunto como título inicial del mensaje</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="602"/>
        <source>Inform only the author when liking things</source>
        <translation>Informar sólo al autor de los &quot;Me gusta&quot;</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="186"/>
        <source>&amp;Save Configuration</source>
        <translation>&amp;Guardar configuración</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="193"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="814"/>
        <source>This is a system notification</source>
        <translation>Esto es una notificación del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="818"/>
        <source>System notifications are not available!</source>
        <translation>¡Las notificaciones del sistema no están disponibles!</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="820"/>
        <source>Own notifications will be used.</source>
        <translation>Se usarán las notificaciones propias.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="830"/>
        <source>This is a basic notification</source>
        <translation>Esto es una notificación básica</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1029"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1032"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1052"/>
        <source>Invalid image</source>
        <translation>Imagen no válida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1053"/>
        <source>The selected image is not valid.</source>
        <translation>La imagen seleccionada no es válida.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="71"/>
        <source>Hometown</source>
        <translation>Ciudad</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="78"/>
        <source>Joined: %1</source>
        <translation>Se unió: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="86"/>
        <source>Updated: %1</source>
        <translation>Actualizado: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="106"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>Bio de %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="120"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>Este usuario no tiene una biografía</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="125"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>No hay biografía para %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="154"/>
        <source>Open Profile in Web Browser</source>
        <translation>Abrir el perfil en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="165"/>
        <source>Send Message</source>
        <translation>Enviar mensaje</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="173"/>
        <source>Browse Messages</source>
        <translation>Ver mensajes</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="197"/>
        <source>User Options</source>
        <translation>Opciones de usuario</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="230"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="242"/>
        <source>Stop Following</source>
        <translation>Dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="306"/>
        <source>Stop following?</source>
        <translation>¿Dejar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="307"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>¿Estás seguro de que quieres dejar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="310"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="311"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="36"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>Escribe una parte de un nombre o ID para encontrar un contacto...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="67"/>
        <source>F&amp;ull List</source>
        <translation>Lista c&amp;ompleta</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="54"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>usuario@servidor.org o https://servidor.org/usuario</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="58"/>
        <source>&amp;Enter address to follow:</source>
        <translation>&amp;Introduce dirección para seguir:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="79"/>
        <source>&amp;Follow</source>
        <translation>&amp;Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="129"/>
        <source>Reload Followers</source>
        <translation>Actualizar Seguidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="134"/>
        <source>Reload Following</source>
        <translation>Actualizar Siguiendo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="140"/>
        <source>Export Followers</source>
        <translation>Exportar Seguidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="145"/>
        <source>Export Following</source>
        <translation>Exportar Siguiendo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="151"/>
        <source>Reload Lists</source>
        <translation>Actualizar listas</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="179"/>
        <source>&amp;Neighbors</source>
        <translation>V&amp;ecinos</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="274"/>
        <source>Cannot export to this file:</source>
        <translation>No se puede exportar a este archivo:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="278"/>
        <source>Please enter another file name, or choose a different folder.</source>
        <translation>Por favor, introduce otro nombre de archivo, o elige otra carpeta.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="519"/>
        <source>The server seems to be a Pump server, but the account does not exist.</source>
        <translation>Parece que el servidor es un servidor Pump, pero la cuenta no existe.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="524"/>
        <source>%1 doesn&apos;t seem to be a Pump server.</source>
        <comment>%1 is a hostname</comment>
        <translation>No parece que %1 sea un servidor Pump.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="534"/>
        <source>Following this account at this time will probably not work.</source>
        <translation>Es probable que seguir esta cuenta en este momento no funcione.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="543"/>
        <source>The %1 server seems unavailable.</source>
        <comment>%1 is a hostname</comment>
        <translation>El servidor %1 no parece disponible.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="551"/>
        <source>Unknown</source>
        <comment>Refers to server version</comment>
        <translation>Desconocida</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="273"/>
        <location filename="../src/contactmanager.cpp" line="556"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="558"/>
        <source>The user address %1 does not exist, or the %2 server is down.</source>
        <translation>La dirección de usuario %1 no existe, o el servidor %2 no está funcionando.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="566"/>
        <source>Server version</source>
        <translation>Versión del servidor</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="570"/>
        <source>Check the address, and keep in mind that usernames are case-sensitive.</source>
        <translation>Comprueba la dirección, y ten en cuenta que los nombres de usuario diferencian entre mayúsculas y minúsculas.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="573"/>
        <source>Do you want to try following this address anyway?</source>
        <translation>¿Quieres intentar seguir esta dirección igualmente?</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="575"/>
        <source>(not recommended)</source>
        <translation>(no recomendado)</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="578"/>
        <source>Yes, follow anyway</source>
        <translation>Si, seguirla igualmente</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="579"/>
        <source>No, cancel</source>
        <translation>No, cancelar</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="598"/>
        <source>About to follow %1...</source>
        <translation>A punto de seguir a %1...</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="208"/>
        <source>Follo&amp;wers</source>
        <translation>Se&amp;guidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="213"/>
        <source>Followin&amp;g</source>
        <translation>Siguien&amp;do</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="218"/>
        <source>&amp;Lists</source>
        <translation>&amp;Listas</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="233"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation>Exportar lista de &apos;siguiendo&apos; a un archivo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="234"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>Exportar lista de &apos;seguidores&apos; a un archivo</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Open</source>
        <comment>Verb, as in: Open the downloaded file</comment>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="58"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="61"/>
        <source>Save the attached file to your folders</source>
        <translation>Guardar el archivo adjunto en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="74"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="144"/>
        <source>Save File As...</source>
        <translation>Guardar archivo como...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="147"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="198"/>
        <source>File not found!</source>
        <translation>¡No se ha encontrado el archivo!</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="209"/>
        <source>Abort download?</source>
        <translation>¿Interrumpir la descarga?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="210"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>¿Quieres detener la descarga del archivo adjunto?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="213"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;Sí, detener</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="214"/>
        <source>&amp;No, continue</source>
        <translation>&amp;No, continuar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="222"/>
        <source>Download aborted</source>
        <translation>Descarga interrumpida</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="240"/>
        <source>Download completed</source>
        <translation>Descarga completada</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="244"/>
        <source>Attachment downloaded successfully to %1</source>
        <comment>%1 = filename</comment>
        <translation>Adjunto descargado correctamente como %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="250"/>
        <source>Open the downloaded attachment with your system&apos;s default program for this type of file.</source>
        <translation>Abrir el adjunto descargado con el programa predefinido de tu sistema para este tipo de archivo.</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="265"/>
        <source>Download failed</source>
        <translation>Ha fallado la descarga</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="272"/>
        <source>Downloading attachment failed: %1</source>
        <comment>%1 = filename</comment>
        <translation>Ha fallado la descarga del adjunto: %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="305"/>
        <source>Downloading %1 KiB...</source>
        <translation>Descargando %1 KiB...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="308"/>
        <source>%1 KiB downloaded</source>
        <translatorcomment>hmmm FIXME</translatorcomment>
        <translation>%1 KiB descargados</translation>
    </message>
</context>
<context>
    <name>DraftsManager</name>
    <message>
        <location filename="../src/draftsmanager.cpp" line="28"/>
        <source>Draft Manager</source>
        <translation>Gestor de borradores</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="44"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="53"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="62"/>
        <source>Manage drafts...</source>
        <translation>Gestionar borradores...</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="87"/>
        <source>&amp;Delete selected draft</source>
        <translation>&amp;Eliminar borrador seleccionado</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="94"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="311"/>
        <source>Untitled draft</source>
        <translation>Borrador sin título</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="328"/>
        <source>Delete draft?</source>
        <translation>¿Eliminar borrador?</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="329"/>
        <source>Are you sure you want to delete this draft?</source>
        <translation>¿Estás seguro de que quieres eliminar este borrador?</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminarlo</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>EmailChanger</name>
    <message>
        <location filename="../src/emailchanger.cpp" line="28"/>
        <source>Change E-mail Address</source>
        <translation>Cambiar dirección de e-mail</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="68"/>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="76"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="97"/>
        <source>E-mail Address:</source>
        <translation>Dirección de e-mail:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="98"/>
        <source>Again:</source>
        <translation>Otra vez:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="99"/>
        <source>Your Password:</source>
        <translation>Tu contraseña:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="150"/>
        <source>E-mail addresses don&apos;t match!</source>
        <translation>¡Las direcciones de e-mail no coinciden!</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="159"/>
        <source>Password is empty!</source>
        <translation>¡La contraseña está vacía!</translation>
    </message>
</context>
<context>
    <name>FDNotifications</name>
    <message>
        <location filename="../src/notifications.cpp" line="209"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>Editor de filtros</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="84"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="44"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1 si %2 contiene: %3</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="51"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation>Aquí puedes establecer algunas normas para ocultar o destacar cosas. Puedes filtrar por contenido, autor o aplicación.

Por ejemplo, puedes filtrar mensajes publicados por la aplicación Open Farm Game, o que contienen la palabra NSFW en el mensaje. También podrías destacar mensajes que contienen tu nombre.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Highlight</source>
        <translation>Destacar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Post Contents</source>
        <translation>Contenido de la publicación</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Author ID</source>
        <translation>ID del autor</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="87"/>
        <source>Activity Description</source>
        <translation>Descripción de la actividad</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="90"/>
        <source>Keywords...</source>
        <translation>Palabras clave...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="95"/>
        <source>&amp;Add Filter</source>
        <translation>&amp;Añadir filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="108"/>
        <source>Filters in use</source>
        <translation>Filtros en uso</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="114"/>
        <source>&amp;Remove Selected Filter</source>
        <translation>&amp;Quitar filtro seleccionado</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="125"/>
        <source>&amp;Save Filters</source>
        <translation>&amp;Guardar filtros</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="132"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="153"/>
        <source>if</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="157"/>
        <source>contains</source>
        <translation>contiene</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="163"/>
        <source>&amp;New Filter</source>
        <translation>&amp;Nuevo filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="170"/>
        <source>C&amp;urrent Filters</source>
        <translation>Filtros &amp;actuales</translation>
    </message>
</context>
<context>
    <name>FilterMatchesWidget</name>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="37"/>
        <source>Content</source>
        <comment>The contents of the post matched</comment>
        <translation>Contenido</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="45"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="52"/>
        <source>App</source>
        <comment>Application, short if possible</comment>
        <translation type="unfinished">Aplicación</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="60"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
</context>
<context>
    <name>FirstRunWizard</name>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="30"/>
        <source>Welcome Wizard</source>
        <translation>Asistente de bienvenida</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="41"/>
        <source>Welcome to Dianara!</source>
        <translation>¡Bienvenido a Dianara!</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="44"/>
        <source>This wizard will help you get started.</source>
        <translation>Este asistente te ayudará a empezar.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="47"/>
        <source>You can access this window again at any time from the Help menu.</source>
        <translation>Puedes volver a acceder a esta ventana en cualquier momento desde el menú de Ayuda.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="50"/>
        <source>The first step is setting up your account, by using the following button:</source>
        <translation>El primer paso es configurar tu cuenta, usando el siguiente botón:</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="61"/>
        <source>Configure your &amp;account</source>
        <translation>Configur&amp;a tu cuenta</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="69"/>
        <source>Once you have configured your account, it&apos;s recommended that you edit your profile and add an avatar and some other information, if you haven&apos;t done so already.</source>
        <translation>Una vez que hayas configurado tu cuenta, es recomendable que edites tu perfil y añadas un avatar y algo más de información, si no lo has hecho ya.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="80"/>
        <source>&amp;Edit your profile</source>
        <translation>&amp;Edita tu perfil</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="89"/>
        <source>By default, Dianara will post only to your followers, but it&apos;s recommended that you post to Public, at least sometimes.</source>
        <translation>De manera predefinida, Dianara publicará sólo para tus seguidores, pero es recomendable que publiques para Público, al menos a veces.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="97"/>
        <source>Post to &amp;Public by default</source>
        <translation>Publicar para &amp;Público de manera predefinida</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="106"/>
        <source>Open general program &amp;help window</source>
        <translation>Abrir la ventana de ay&amp;uda general del programa</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="114"/>
        <source>&amp;Show this again next time Dianara starts</source>
        <translation>&amp;Mostrar de nuevo la próxima vez que se inicie Dianara</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="120"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change...</source>
        <translation>Cambiar...</translation>
    </message>
    <message>
        <location filename="../src/fontpicker.cpp" line="97"/>
        <source>Choose a font</source>
        <translation>Elige un tipo de letra</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>Ayuda básica</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation>Empezando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="73"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation>La primera vez que inicies Dianara, deberías ver la ventana de Configuración de la cuenta. Allí, introduce tu dirección de Pump.io como nombre@servidor y pulsa el botón Obtener código de verificación.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="78"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation>A continuación, tu navegador web habitual debería cargar la página de autorización en tu servidor Pump.io. Allí, tendrás que copiar el código &apos;VERIFIER&apos; completo, y pegarlo en el segundo campo de Dianara. Entonces pulsa &apos;Autorizar la aplicación&apos;, y una vez que se confirme, pulsa &apos;Guardar datos&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>En este momento, se cargará tu perfil, las listas de contactos y las líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="87"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>Deberías echar un vistazo a la ventana &apos;Configuración del programa&apos;, en el menú Configuración - Configurar Dianara. Allí encontrarás varias opciones interesantes.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="110"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>Puedes ajustar varias cosas a tu gusto en la configuración, como el intervalo de tiempo entre actualizaciones de la línea temporal, cuantos mensajes por página quieres, colores para destacar mensajes, notificaciones o qué aspecto tendrá el icono de la bandeja del sistema.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>Líneas temporales</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="91"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>Recuerda que hay muchos lugares en Dianara donde puedes obtener más información manteniendo el ratón sobre algún texto o botón, y esperando a que aparezca la descripción emergente (tooltip).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="115"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation>Aquí, también puedes activar la opción para publicar siempre tus mensajes como públicos de forma predefinida. Siempre puedes cambiar esto en el momento de publicar.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="131"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>La línea temporal principal, donde verás todo lo que ha publicado o compartido la gente a la que sigues.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="134"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>Línea temporal de mensajes, donde verás mensajes enviados a ti específicamente. Estos mensajes pueden haber sido enviados también a otras personas.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="138"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>Línea temporal de actividad, donde verás tus propios mensajes, o mensajes que has compartido.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="141"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>Línea temporal de favoritos, donde verás los mensajes que te han gustado. Esto puede usarse como un sistema de marcadores.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="158"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>Estas actividades pueden tener un botón &quot;+&quot;. Púlsalo para abrir el mensaje al que hacen referencia. Además, como en muchos otros lugares, puedes mantener el ratón encima para ver información relevante en la descripción emergente (tooltip).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="233"/>
        <source>Under the &apos;Neighbors&apos; tab you&apos;ll see some resources to find people, and have the option to browse the latest registered users from your server directly.</source>
        <translation>Dentro de la pestaña &apos;Vecinos&apos; verás algunos recursos para encontrar gente, y tendrás la opción de ver los últimos usuarios registrados en tu servidor, directamente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="316"/>
        <source>Dianara offers a D-Bus interface that allows some control from other applications.</source>
        <translation>Dianara ofrece una interfaz D-Bus que permite un cierto control desde otras aplicaciones.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="319"/>
        <source>The interface is at %1, and you can access it with tools such as %2 or %3. It offers methods like %4 and %5.</source>
        <translation>La interfaz se encuentra en %1, y puedes acceder a ella con herramientas como %2 o %3. Ofrece métodos como %4 y %5.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>Publicando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="96"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>Si es la primera vez que usas Pump.io, echa un vistazo a esta guía:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>Los mensajes nuevos aparecen destacados en un color diferente. Puedes marcarlos como leídos haciendo clic en cualquier parte vacía del mensaje.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="176"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation>Puedes publicar notas haciendo clic en el campo de texto de la parte superior de la ventana o pulsando Control+N. Añadir un título al mensaje es opcional, pero muy recomendable, ya que ayudará a identificar mejor las referencias a tu mensaje en la línea temporal menor, notificaciones por e-mail, etc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>Es posible adjuntar imágenes, audio, video y archivos generales, como documentos PDF, a tu mensaje.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="192"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>Puedes usar el botón Formato para añadir formato al texto, como negrita o cursiva. Algunas de estas opciones requieren que se seleccione el texto antes de usarlas.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="199"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>Si añades una persona específica a la lista &apos;Para&apos;, recibirá tu mensaje en su pestaña de mensajes directos.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="210"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>Puedes hacer mensajes privados añadiendo gente específica a estas listas, y desmarcando las opciones Público y Seguidores.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>Gestionando los contactos</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="223"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>Puedes ver las listas de gente a la que sigues, y que te sigue, en la pestaña Contactos.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="226"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>Allí puedes gestionar también las listas de personas, que se utilizan principalmente para enviar mensajes a grupos de gente específicos.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="237"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>Puedes hacer clic en cualquier avatar en los mensajes, los comentarios, y la columna &quot;Mientras tanto&quot;, y verás un menú con varias opciones, una de las cuales es seguir o dejar de seguir a esta persona.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>Control por teclado</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="98"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="128"/>
        <source>There are seven timelines:</source>
        <translation>Hay siete líneas temporales:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>La sexta y séptima líneas temporales también son líneas temporales menores, parecidas al &quot;Mientras tanto&quot;, pero contienen únicamente actividades dirigidas a ti (Menciones) y actividades hechas por ti (Acciones).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="196"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>Puedes seleccionar quien verá tu mensaje usando los botones &apos;Para&apos; y &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="202"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>También puedes teclear &apos;@&apos; y los primeros caracteres del nombre de un contacto para mostrar un menú emergente con opciones que coincidan.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="242"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>También puedes enviar un mensaje directo (inicialmente privado) a este contacto desde este menú.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="245"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>Puedes encontrar una lista con algunos usuarios de Pump.io y otros datos aquí:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="249"/>
        <source>Users by language</source>
        <translation>Usuarios por idioma</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="251"/>
        <source>Followers of Pump.io Community account</source>
        <translation>Seguidores de la cuenta Pump.io Community</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="263"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>Las acciones más comunes que se encuentran en los menús tienen atajos de teclado escritos al lado, como F5 o Control+N.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="267"/>
        <source>Besides that, you can use:</source>
        <translation>Aparte de eso, puedes usar:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="270"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation>Control+Arriba/Abajo/RePag/AvPag/Inicio/Fin para moverte por la línea temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="273"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>Control+Izquierda/Derecha para pasar una página en la línea temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="276"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>Control+G para ir a cualquier página de la línea temporal directamente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="279"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>Control+1/2/3 para cambiar entre las líneas temporales menores.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="282"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>Control+Enter para publicar, cuando hayas acabado de redactar una nota o un comentario. Si la nota está vacía, la puedes cancelar pulsando ESC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Opciones de línea de comandos</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="146"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <comment>LEFT SIDE should change to RIGHT SIDE on RTL languages</comment>
        <translation>La quinta línea temporal es la línea temporal menor, también conocida como el &quot;Mientras tanto&quot;. Es visible en el lado izquierdo, aunque se puede esconder. Aquí verás actividades secundarias hechas por todo el mundo, como acciones de comentar, marcar mensajes con &quot;Me gusta&quot; o seguir a gente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="206"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>Elige una con las teclas de cursor y pulsa Enter para completar el nombre. Esto añadirá a esta persona a la lista de destinatarios.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="229"/>
        <source>There is a text field at the top, where you can directly enter addresses of new contacts to follow them.</source>
        <translation>Hay un campo de texto en la parte superior, donde puedes introducir directamente direcciones de contactos nuevos para seguirles.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="286"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>Mientras redactas una nota, pulsa Enter para pasar del título al cuerpo del mensaje. Además, pulsando la flecha arriba cuando te encuentres al principio del mensaje, vuelve al título.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="291"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>Control+Enter para acabar de crear una lista de destinatarios para un mensaje, en las listas &apos;Para&apos; o &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="304"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>Puedes usar el parámetro --config para ejecutar el programa con una configuración diferente. Esto puede ser útil para usar dos o más cuentas diferentes. Incluso puedes ejecutar dos instancias de Dianara a la vez.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="309"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Usa el parámetro --debug para tener información adicional en la ventana de la terminal, sobre lo que está haciendo el programa.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="312"/>
        <source>If your server does not support HTTPS, you can use the --nohttps parameter.</source>
        <translation>Si tu servidor no tiene soporte HTTPS, puedes utilizar el parámetro --nohttps.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="326"/>
        <source>If you use an alternate configuration, with something like &apos;--config otherconf&apos;, then the interface will be at org.nongnu.dianara_otherconf.</source>
        <translation>Si usas una configuración alternativa, con algo como &apos;--config otraconf&apos;, entonces la interfaz estará en org.nongnu.dianara_otraconf.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="343"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="33"/>
        <source>Image</source>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="31"/>
        <source>Untitled</source>
        <translation>Sin título</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="67"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Guardar como...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="80"/>
        <source>&amp;Restart</source>
        <comment>Restart animation</comment>
        <translation>&amp;Reiniciar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="90"/>
        <source>Fit</source>
        <comment>As in: fit image to window</comment>
        <translation>Ajustar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="137"/>
        <source>Rotate image to the left</source>
        <comment>RTL: This actually means LEFT, anticlockwise</comment>
        <translation>Girar la imagen a la izquierda</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="147"/>
        <source>Rotate image to the right</source>
        <comment>RTL: This actually means RIGHT, clockwise</comment>
        <translation>Girar la imagen a la derecha</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="160"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="296"/>
        <source>Save Image...</source>
        <translation>Guardar imagen...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="336"/>
        <source>Close Viewer</source>
        <translation>Cerrar visor</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="377"/>
        <source>Downloading full image...</source>
        <translation>Descargando imagen completa...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="523"/>
        <source>Error downloading image!</source>
        <translation>Error descargando la imagen!</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="525"/>
        <source>Try again later.</source>
        <translation>Vuelve a intentarlo más tarde.</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="536"/>
        <source>Save Image As...</source>
        <translation>Guardar imagen como...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="539"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="540"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="551"/>
        <source>Error saving image</source>
        <translation>Error guardando la imagen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="552"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>Ha habido un problema al guardar %1.

El nombre del archivo debería acabar con la extensión .jpg o .png.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="39"/>
        <source>Members</source>
        <translation>Miembros</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="52"/>
        <source>Add Mem&amp;ber</source>
        <translation>Añadir miem&amp;bro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="60"/>
        <source>&amp;Remove Member</source>
        <translation>&amp;Quitar miembro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="69"/>
        <source>&amp;Delete Selected List</source>
        <translation>&amp;Borrar lista seleccionada</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="77"/>
        <source>Add New &amp;List</source>
        <translation>Añadir nueva &amp;lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="96"/>
        <source>Create L&amp;ist</source>
        <translation>Crear l&amp;ista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="104"/>
        <source>&amp;Add to List</source>
        <translation>&amp;Añadir a lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="316"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation>¿Estás seguro de que quieres eliminar %1?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="451"/>
        <source>Remove person from list?</source>
        <translation>¿Quitar persona de la lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="452"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>¿Estás seguro de que quieres quitar a %1 de la lista %2?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sí</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="80"/>
        <source>Type a name for the new list...</source>
        <translation>Escribe un nombre para la nueva lista...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="89"/>
        <source>Type an optional description here</source>
        <translation>Escribe una descripción opcional aquí</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="315"/>
        <source>WARNING: Delete list?</source>
        <translation>ADVERTENCIA: ¿Eliminar lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminarla</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>Borrar el &amp;registro</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="61"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="2662"/>
        <source>&amp;Messages</source>
        <translation>&amp;Mensajes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>&amp;Contacts</source>
        <translation>&amp;Contactos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>&amp;Session</source>
        <translation>&amp;Sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1099"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;Barra de estado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2653"/>
        <source>&amp;Timeline</source>
        <translation>Línea &amp;temporal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2671"/>
        <source>&amp;Activity</source>
        <translation>&amp;Actividad</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="738"/>
        <location filename="../src/mainwindow.cpp" line="1387"/>
        <source>Initializing...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="788"/>
        <source>Your account is not configured yet.</source>
        <translation>Tu cuenta no está configurada todavía.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="163"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation>Actividades secundarias hechas por todo el mundo, tales como respuestas a mensajes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Minor activities addressed to you</source>
        <translation>Actividades secundarias dirigidas a ti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="198"/>
        <source>Minor activities done by you</source>
        <translation>Actividades secundarias hechas por ti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="429"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>La gente a la que sigues, la que te sigue, y tus listas de personas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>Auto-actualizar líneas &amp;temporales</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1039"/>
        <source>Mark All as Read</source>
        <translation>Marcar todo como leído</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;Publicar una nota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1072"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Locked Panels and Toolbars</source>
        <translation>Paneles y barras de herramientas bloqueados</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1075"/>
        <source>Side Panel</source>
        <translation>Panel lateral</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>&amp;Toolbar</source>
        <translation>&amp;Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1112"/>
        <source>Full &amp;Screen</source>
        <translation>&amp;Pantalla completa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>&amp;Log</source>
        <translation>&amp;Registro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>S&amp;ettings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Edit &amp;Profile</source>
        <translation>Editar &amp;perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>&amp;Account</source>
        <translation>&amp;Cuenta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>&amp;Configure Dianara</source>
        <translation>&amp;Configurar Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>&amp;Help</source>
        <translation>Ay&amp;uda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Basic &amp;Help</source>
        <translation>&amp;Ayuda básica</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>Visit &amp;Website</source>
        <translation>Visitar sitio &amp;web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <source>Report a &amp;Bug</source>
        <translation>Informar de un &amp;fallo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1241"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>&amp;Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1257"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>Lista de algunos &amp;usuarios de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1265"/>
        <source>Pump.io &amp;Network Status Website</source>
        <translation>Web del estado de la &amp;red Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1276"/>
        <source>About &amp;Dianara</source>
        <translation>Acerca de &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>Toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1371"/>
        <source>Open the log viewer</source>
        <translation>Abrir el visor del registro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1394"/>
        <source>Auto-updating enabled</source>
        <translation>Auto-actualizaciones activadas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1401"/>
        <source>Auto-updating disabled</source>
        <translation>Auto-actualizaciones desactivadas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1953"/>
        <source>Proxy password required</source>
        <translation>Contraseña de proxy necesaria</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1954"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>Has configurado un servidor proxy con autenticación, pero la contraseña no está definida.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1959"/>
        <source>Enter the password for your proxy server:</source>
        <translation>Introduce la contraseña para tu servidor proxy:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2048"/>
        <source>Your biography is empty</source>
        <translation>Tu biografía está vacía</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2083"/>
        <source>Click to edit your profile</source>
        <translation>Haz clic para editar tu perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2108"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>Iniciando actualización automática de líneas temporales, una vez cada %1 minutos.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2115"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>Deteniendo actualización automática de líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2376"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation>Se han recibido %1 mensajes anteriores en &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2416"/>
        <source>1 highlighted</source>
        <comment>singular, refers to a post</comment>
        <translation>1 destacado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2421"/>
        <source>%1 highlighted</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 destacados</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2411"/>
        <source>Direct messages</source>
        <translation>Mensajes directos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2412"/>
        <source>By filters</source>
        <translation>Por filtros</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2453"/>
        <source>1 more pending to receive.</source>
        <comment>singular, one post</comment>
        <translation>1 más pendiente de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2458"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation>%1 más pendientes de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2470"/>
        <location filename="../src/mainwindow.cpp" line="2886"/>
        <source>Also:</source>
        <translation>Además:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2592"/>
        <source>Last update: %1</source>
        <translation>Última actualización: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2869"/>
        <source>1 more pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation>1 más pendiente de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2874"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation>%1 más pendientes de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2385"/>
        <location filename="../src/mainwindow.cpp" line="2822"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>&apos;%1&apos; actualizada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="764"/>
        <source>%1 started.</source>
        <comment>1=program name and version</comment>
        <translation>%1 iniciado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="767"/>
        <source>Running with Qt v%1 on %2.</source>
        <comment>1=Qt version, 2=OS name</comment>
        <translation type="unfinished">Funcionando con Qt v%1 sobre %2.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2477"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 filtrado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2482"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 filtrados.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2495"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 eliminado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2500"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 eliminados.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2561"/>
        <source>No new posts.</source>
        <translation>No hay mensajes nuevos.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2834"/>
        <source>There is 1 new activity.</source>
        <translation>Hay 1 actividad nueva.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2838"/>
        <source>There are %1 new activities.</source>
        <translation>Hay %1 actividades nuevas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2851"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation>1 destacada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2856"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation>%1 destacadas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2890"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to one activity</comment>
        <translation>1 filtrada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>%1 filtered out.</source>
        <comment>plural, several activities</comment>
        <translation>%1 filtradas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2964"/>
        <source>No new activities.</source>
        <translation>No hay actividades nuevas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3040"/>
        <source>Error storing image!</source>
        <translation>¡Error almacenando la imagen!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3042"/>
        <source>%1 bytes</source>
        <translation>%1 bytes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3086"/>
        <source>Link to: %1</source>
        <translation>Enlace a: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3105"/>
        <source>Marking everything as read...</source>
        <translation>Marcando todo como leído...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3436"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>Con Dianara puedes ver tus líneas temporales, crear nuevos mensajes, subir fotos y multimedia, interactuar con los mensajes, gestionar tus contactos y seguir gente nueva.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3442"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traducción al castellano por JanKusanagi.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3453"/>
        <source>Dianara is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation>Dianara es software libre, licenciado bajo la licencia GNU GPL, y utiliza algunos iconos Oxygen con licencia LGPL.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3490"/>
        <source>&amp;Hide Window</source>
        <translation>&amp;Ocultar ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3501"/>
        <source>&amp;Show Window</source>
        <translation>&amp;Mostrar ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3549"/>
        <source>Closing due to environment shutting down...</source>
        <translatorcomment>meh...</translatorcomment>
        <translation>Cerrando debido a que el entorno se está apagando...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3572"/>
        <location filename="../src/mainwindow.cpp" line="3677"/>
        <source>Quit?</source>
        <translation>¿Salir?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3573"/>
        <source>You are composing a note or a comment.</source>
        <translation>Estás redactando una nota o un comentario.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3575"/>
        <source>Do you really want to close Dianara?</source>
        <translation>¿Realmente quieres cerrar Dianara?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3576"/>
        <location filename="../src/mainwindow.cpp" line="3685"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;Sí, cerrar el programa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3576"/>
        <location filename="../src/mainwindow.cpp" line="3685"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3599"/>
        <source>Shutting down Dianara...</source>
        <translation>Cerrando Dianara...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3678"/>
        <source>System tray icon is not available.</source>
        <translation>El icono de la bandeja del sistema no está disponible.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3680"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>Dianara no se puede ocultar en la bandeja del sistema.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3683"/>
        <source>Do you want to close the program completely?</source>
        <translation>¿Quieres cerrar el programa completamente?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2397"/>
        <source>There is 1 new post.</source>
        <translation>Hay 1 mensaje nuevo.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2401"/>
        <source>There are %1 new posts.</source>
        <translation>Hay %1 mensajes nuevos.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2391"/>
        <source>Timeline updated at %1.</source>
        <translation>Línea temporal actualizada a las %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2645"/>
        <source>Total posts: %1</source>
        <translation>Total de mensajes: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2737"/>
        <source>Your Pump.io account is not configured</source>
        <translation>Tu cuenta Pump.io no está configurada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3432"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>Dianara es un cliente de red social para pump.io.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3449"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>¡Gracias a todos los &apos;testers&apos;, traductores y empaquetadores, que ayudan a hacer Dianara mejor!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2655"/>
        <source>The main timeline</source>
        <translation>La línea temporal principal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="723"/>
        <source>Press F1 for help</source>
        <translation>Pulsa F1 para ver la ayuda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="794"/>
        <source>Click here to configure your account</source>
        <translation>Haz clic aquí para configurar tu cuenta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="934"/>
        <source>Update %1</source>
        <translation>Actualizar %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Show Welcome Wizard</source>
        <translation>Mostrar el asistente de bienvenida</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2673"/>
        <source>Your own posts</source>
        <translation>Tus propios mensajes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2682"/>
        <source>Your favorited posts</source>
        <translation>Los mensajes que te gustan</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2664"/>
        <source>Messages sent explicitly to you</source>
        <translation>Mensajes enviados explícitamente a ti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>&amp;Filters and Highlighting</source>
        <translatorcomment>hmmm</translatorcomment>
        <translation>&amp;Filtros y destacados</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1249"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>Algunos &amp;consejos sobre Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2680"/>
        <source>Favor&amp;ites</source>
        <translation>&amp;Favoritos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2814"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation>Se han recibido %1 actividades anteriores en &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2827"/>
        <source>Minor feed updated at %1.</source>
        <translation>Línea temporal menor actualizada a las %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>About Dianara</source>
        <translation>Acerca de Dianara</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="70"/>
        <source>Older Activities</source>
        <translation>Actividades anteriores</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="74"/>
        <source>Get previous minor activities</source>
        <translation>Obtener actividades secundarias anteriores</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="102"/>
        <source>There are no activities to show yet.</source>
        <translation>Aún no hay actividades para mostrar.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="452"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation>Recibir %1 más nuevas</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="69"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>Usando %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="83"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>Para: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="90"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>Cc: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="266"/>
        <source>Open referenced post</source>
        <translation>Abrir el mensaje referenciado</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="315"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="671"/>
        <source>Error: Unable to launch browser</source>
        <translation>Error: No se ha podido abrir el navegador</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="672"/>
        <source>The default system web browser could not be executed.</source>
        <translation>No se ha podido ejecutar el navegador web predefinido del sistema.</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="675"/>
        <source>You might need to install the XDG utilities.</source>
        <translation>Puede que necesites instalar las utilidades XDG.</translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>Saltar a página</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="35"/>
        <source>Page number:</source>
        <translation>Número de página:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="50"/>
        <source>&amp;First</source>
        <comment>As in: first page</comment>
        <translation>&amp;Primera</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="57"/>
        <source>&amp;Last</source>
        <comment>As in: last page</comment>
        <translation>Últim&amp;a</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="64"/>
        <source>Newer</source>
        <comment>As in: newer pages</comment>
        <translation>Más nuevas</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="85"/>
        <source>Older</source>
        <comment>As in: older pages</comment>
        <translation>Más antiguas</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="96"/>
        <source>&amp;Go</source>
        <translation>&amp;Ir</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>&amp;Buscar:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>Escribe aquí un nombre para buscarlo</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="102"/>
        <source>Add a contact to a list</source>
        <translation>Añadir un contacto a una lista</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="114"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="1801"/>
        <source>Like this post</source>
        <translation>Decir que te gusta este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1802"/>
        <source>Like</source>
        <translation>Me gusta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="259"/>
        <source>Shared on %1</source>
        <translation>Compartido el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="359"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1000"/>
        <source>In</source>
        <translation>En</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="215"/>
        <location filename="../src/post.cpp" line="436"/>
        <source>To</source>
        <translation>Para</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="263"/>
        <location filename="../src/post.cpp" line="952"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>Usando %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="522"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>Padre</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="531"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>Abrir el mensaje padre, al que éste responde</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="681"/>
        <source>Modify this post</source>
        <translation>Modificar este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="767"/>
        <source>Join Group</source>
        <translation>Unirse al grupo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="773"/>
        <source>%1 members in the group</source>
        <translation>%1 miembros en el grupo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1407"/>
        <source>1 like</source>
        <translation>Le gusta a 1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1476"/>
        <source>1 comment</source>
        <translation>1 comentario</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1576"/>
        <source>Shared %1 times</source>
        <translation>Compartido %1 veces</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="652"/>
        <source>Share</source>
        <translation>Compartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>Haz clic para descargar el archivo adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="140"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="944"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="959"/>
        <source>Modified on %1</source>
        <translation>Modificado el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="678"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1123"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>La imagen es animada. Haz clic para reproducirla.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1155"/>
        <source>Loading image...</source>
        <translation>Cargando imagen...</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1412"/>
        <source>%1 likes</source>
        <translation>Les gusta a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1481"/>
        <source>%1 comments</source>
        <translation>%1 comentarios</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="690"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="205"/>
        <source>Via %1</source>
        <translatorcomment>Meh...</translatorcomment>
        <translation>A través de %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1709"/>
        <source>Edited: %1</source>
        <translation>Editado: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="942"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>Publicado el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="632"/>
        <source>If you select some text, it will be quoted.</source>
        <translation>Si seleccionas parte del texto, será citado.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="660"/>
        <source>Unshare</source>
        <translation>Dejar de compartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="662"/>
        <source>Unshare this post</source>
        <translation>Dejar de compartir este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="313"/>
        <source>Open post in web browser</source>
        <translation>Abrir el mensaje en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="221"/>
        <location filename="../src/post.cpp" line="453"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="322"/>
        <source>Copy post link to clipboard</source>
        <translation>Copiar el enlace del mensaje al portapapeles</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="341"/>
        <source>Normalize text colors</source>
        <translation>Normalizar colores del texto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="627"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>Reply to this post.</source>
        <translation>Responder a este mensaje.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="654"/>
        <source>Share this post with your contacts</source>
        <translation>Compartir este mensaje con tus contactos</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="693"/>
        <source>Erase this post</source>
        <translation>Borrar este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1133"/>
        <source>Size</source>
        <comment>Image size (resolution)</comment>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1150"/>
        <source>Couldn&apos;t load image!</source>
        <translation>¡No se ha podido cargar la imagen!</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1173"/>
        <source>Attached Audio</source>
        <translation>Audio adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1183"/>
        <source>Attached Video</source>
        <translation>Vídeo adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1217"/>
        <source>Attached File</source>
        <translation>Archivo adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1381"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>Le gusta a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1386"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation>Les gusta a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1537"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 ha compartido esto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1542"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 han compartido esto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1571"/>
        <source>Shared once</source>
        <translation>Compartido una vez</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1795"/>
        <source>You like this</source>
        <translation>Te gusta esto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1796"/>
        <source>Unlike</source>
        <translation>Ya no me gusta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1957"/>
        <source>Are you sure you want to share your own post?</source>
        <translation>¿Estás seguro de que quieres compartir tu propio mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1962"/>
        <source>Share post?</source>
        <translation>¿Compartir mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1952"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>¿Quieres compartir el mensaje de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1965"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;Sí, compartirlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1966"/>
        <location filename="../src/post.cpp" line="1986"/>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1982"/>
        <source>Unshare post?</source>
        <translation>¿Dejar de compartir el mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1983"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>¿Quieres dejar de compartir el mensaje de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1985"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;Sí,dejar de compartirlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2027"/>
        <source>WARNING: Delete post?</source>
        <translation>ADVERTENCIA: ¿Eliminar mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2028"/>
        <source>Are you sure you want to delete this post?</source>
        <translation>¿Estás seguro de que quieres eliminar este mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminarlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>Haz clic en la imagen para verla en tamaño completo</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>Editor de perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="51"/>
        <source>This is your Pump address</source>
        <translation>Esta es tu dirección Pump</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="53"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>Esta es la dirección de e-mail asociada con tu cuenta, para cosas como notificaciones y recuperación de la contraseña</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="70"/>
        <source>Change &amp;E-mail...</source>
        <translation>Cambiar &amp;e-mail...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="82"/>
        <source>Change &amp;Avatar...</source>
        <translation>Cambiar &amp;avatar...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="92"/>
        <source>This is your visible name</source>
        <translation>Este es tu nombre visible</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>Changing your avatar will create a post in your timeline with it.
If you delete that post your avatar will be deleted too.</source>
        <translation>Cambiar tu avatar creará una publicación en tu línea temporal con él.
Si borras esta publicación, tu avatar se borrará también.</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="126"/>
        <source>&amp;Save Profile</source>
        <translation>&amp;Guardar perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="133"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="166"/>
        <source>Webfinger ID</source>
        <translation>ID Webfinger</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="167"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="169"/>
        <source>Avatar</source>
        <translation>Avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="171"/>
        <source>Full &amp;Name</source>
        <translation>&amp;Nombre completo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="172"/>
        <source>&amp;Hometown</source>
        <translation>Ciuda&amp;d</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="173"/>
        <source>&amp;Bio</source>
        <translation>&amp;Bio</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="211"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>Sin definir</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="284"/>
        <source>Select avatar image</source>
        <translation>Selecciona imagen de avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="286"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="289"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="312"/>
        <source>Invalid image</source>
        <translation>Imagen no válida</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="313"/>
        <source>The selected image is not valid.</source>
        <translation>La imagen seleccionada no es válida.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>Configuración de proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>No utilizar un proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>Tu nombre de usuario para el proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>Nota: La contraseña no se guarda de forma segura. Si lo deseas, puedes dejar el campo vacío, y se te pedirá la contraseña al iniciar.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="75"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="82"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="98"/>
        <source>Proxy &amp;Type</source>
        <translation>&amp;Tipo de proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="100"/>
        <source>&amp;Hostname</source>
        <translation>&amp;Servidor</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="102"/>
        <source>&amp;Port</source>
        <translation>&amp;Puerto</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="104"/>
        <source>Use &amp;Authentication</source>
        <translation>Usar &amp;autenticación</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="106"/>
        <source>&amp;User</source>
        <translation>&amp;Usuario</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="108"/>
        <source>Pass&amp;word</source>
        <translation>Con&amp;traseña</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="229"/>
        <source>Picture</source>
        <translation>Foto</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="236"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="243"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="266"/>
        <source>Ad&amp;d...</source>
        <translation>&amp;Añadir...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="269"/>
        <source>Upload media, like pictures or videos</source>
        <translation>Subir multimedia, como fotos o vídeos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1038"/>
        <source>Select Picture...</source>
        <translation>Seleccionar foto...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="75"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1040"/>
        <source>Find the picture in your folders</source>
        <translation>Encontrar la foto en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="188"/>
        <source>Select who will see this post</source>
        <translation>Selecciona quien verá este mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="185"/>
        <source>To...</source>
        <translation>Para...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="175"/>
        <source>Drafts</source>
        <translation>Borradores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="199"/>
        <source>Select who will get a copy of this post</source>
        <translation>Selecciona quien recibirá una copia de este mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="250"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>Otros</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="303"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="306"/>
        <source>Cancel the post</source>
        <translation>Cancelar el mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1033"/>
        <source>Picture not set</source>
        <translation>Foto no seleccionada</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1063"/>
        <source>Select Audio File...</source>
        <translation>Seleccionar archivo de audio...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1065"/>
        <source>Find the audio file in your folders</source>
        <translation>Encontrar el archivo de audio en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1058"/>
        <source>Audio file not set</source>
        <translation>Archivo de audio no seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1088"/>
        <source>Select Video...</source>
        <translation>Seleccionar video...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1090"/>
        <source>Find the video in your folders</source>
        <translation>Encontrar el vídeo en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1083"/>
        <source>Video not set</source>
        <translation>Vídeo no seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1112"/>
        <source>Select File...</source>
        <translation>Seleccionar archivo...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1114"/>
        <source>Find the file in your folders</source>
        <translation>Encontrar el archivo en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1107"/>
        <source>File not set</source>
        <translation>Archivo no seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1183"/>
        <location filename="../src/publisher.cpp" line="1227"/>
        <source>Error: Already composing</source>
        <translation>Error: Ya se está redactando</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1184"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>No puedes editar un mensaje en este momento, porque ya se está redactando un mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1202"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1228"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>No puedes crear un mensaje para %1 en este momento, porque ya se está redactando un mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1355"/>
        <source>Draft loaded.</source>
        <translation>Se ha cargado un borrador.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1360"/>
        <source>ERROR: Already composing</source>
        <translation>ERROR: Ya se está redactando</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1361"/>
        <source>You can&apos;t load a draft at this time, because a post is already being composed.</source>
        <translation>No puedes cargar un borrador en este momento, porque ya se está redactando un mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1380"/>
        <source>Draft saved.</source>
        <translation>Borrador guardado.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1417"/>
        <source>Posting failed.

Try again.</source>
        <translation>Ha fallado la publicación.

Prueba de nuevo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1561"/>
        <source>Warning: You have no followers yet</source>
        <translation>Advertencia: Aún no tienes seguidores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1562"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>Estás intentando publicar sólo para tus seguidores, pero no tienes ningún seguidor todavía.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1566"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>Si publicas de esta manera, nadie podrá ver tu mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1574"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;Cancelar, volver al mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1627"/>
        <source>Updating...</source>
        <translation>Actualizando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1638"/>
        <source>Post is empty.</source>
        <translation>El mensaje está vacío.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1642"/>
        <source>File not selected.</source>
        <translation>No se ha seleccionado un archivo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="590"/>
        <source>Select one image</source>
        <translation>Selecciona una imagen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="591"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="619"/>
        <source>Select one file</source>
        <translation>Selecciona un archivo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="622"/>
        <source>Invalid file</source>
        <translation>Archivo no válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="623"/>
        <source>The file type cannot be detected.</source>
        <translation>El tipo de archivo no se puede detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="635"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="652"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>Como lo que estás subiendo es una imagen, podrías reducirla un poco o guardarla en un formato más comprimido, como JPG.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="658"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>Actualmente, Dianara limita las subidas de archivos a 10 MiB por mensaje, para evitar posibles problemas de almacenamiento, o de red, en los servidores.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="663"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>Esto es una medida temporal, ya que los servidores no pueden establecer sus propios límites todavía.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="837"/>
        <source>File not found.</source>
        <translation>No se ha encontrado el archivo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="840"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="822"/>
        <source>The selected file cannot be accessed:</source>
        <translation>No se puede acceder al archivo seleccionado:</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="832"/>
        <source>You might not have the necessary permissions.</source>
        <translation>Es posible que no tengas los permisos necesarios.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="697"/>
        <source>Resolution</source>
        <comment>Image resolution (size)</comment>
        <translation>Resolución</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="657"/>
        <source>File is too big</source>
        <translation>El archivo es demasiado grande</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="80"/>
        <source>Add a brief title for the post here (recommended)</source>
        <translation>Añade un título breve para el mensaje aquí (recomendado)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1569"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>¿Quieres hacer el mensaje público en lugar de sólo para seguidores?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1572"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;Sí, hacerlo público</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="668"/>
        <source>Sorry for the inconvenience.</source>
        <translation>Lamentamos las molestias.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="829"/>
        <source>It is owned by %1.</source>
        <comment>%1 = a username</comment>
        <translation>Es propiedad de %1.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="720"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="722"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1670"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 KiB de %2 KiB subidos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="594"/>
        <source>Invalid image</source>
        <translation>Imagen no válida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>Añadir un título ayuda a hacer el contenido del &quot;Mientras tanto&quot; más informativo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="108"/>
        <source>Remove</source>
        <translatorcomment>Quitar? Eliminar?</translatorcomment>
        <translation>Quitar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="111"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>Cancelar el adjunto y volver a una nota normal</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="196"/>
        <source>Cc...</source>
        <translation>Cc...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="294"/>
        <location filename="../src/publisher.cpp" line="929"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>Publicar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="420"/>
        <source>Note started from another application.</source>
        <translation>Nota empezada desde otra aplicación.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="424"/>
        <source>Ignoring new note request from another application.</source>
        <translation>Ignorando petición de nueva nota desde otra aplicación.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="951"/>
        <source>Attachment upload was cancelled.</source>
        <translation type="unfinished">Se ha cancelado la subida del adjunto.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1213"/>
        <source>Editing post.</source>
        <translation>Editando mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1573"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;No, publicar sólo para mis seguidores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="595"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>No se puede detectar el formato de la imagen.
La extensión podría estar equivocada, como una imagen GIF renombrada a imagen.jpg o similar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="601"/>
        <source>Select one audio file</source>
        <translation>Selecciona un archivo de audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="602"/>
        <source>Audio files</source>
        <translation>Archivos de audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="605"/>
        <source>Invalid audio file</source>
        <translation>Archivo de audio no válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="606"/>
        <source>The audio format cannot be detected.</source>
        <translation>El formato de audio no se puede detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="610"/>
        <source>Select one video file</source>
        <translation>Selecciona un archivo de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="611"/>
        <source>Video files</source>
        <translation>Archivos de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="614"/>
        <source>Invalid video file</source>
        <translation>Archivo de vídeo no válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="615"/>
        <source>The video format cannot be detected.</source>
        <translation>El formato de vídeo no se puede detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1594"/>
        <source>Posting...</source>
        <translation>Publicando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="297"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>Pulsa Control+Enter para publicar con el teclado</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="910"/>
        <source>Getting likes...</source>
        <translatorcomment>meh...</translatorcomment>
        <translation>Recibiendo los &quot;me gusta&quot;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="942"/>
        <source>Getting comments...</source>
        <translation>Recibiendo comentarios...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1667"/>
        <source>Error connecting to %1</source>
        <translation>Error conectando a %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1677"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>Código de error HTTP no gestionado: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2192"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Siguiendo a %1 (%2) correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2202"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Se ha dejado de seguir a %1 (%2) correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2252"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>Lista de &apos;siguiendo&apos; completamente recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2259"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>Parte de la lista de &apos;siguiendo&apos; recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2282"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>Lista de &apos;seguidores&apos; completamente recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2288"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>Parte de la lista de &apos;seguidores&apos; recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2345"/>
        <source>List of %1 users received.</source>
        <comment>%1 is a server name</comment>
        <translation>Lista de usuarios de %1 recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2369"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>Lista de personas &apos;%1&apos; creada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2400"/>
        <source>Person list received.</source>
        <translation>Lista de personas recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2554"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>Archivo subido correctamente. Publicando mensaje...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="252"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>Autorizado para usar la cuenta %1. Recibiendo datos iniciales.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="257"/>
        <source>There is no authorized account.</source>
        <translation>No hay ninguna cuenta autorizada.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="362"/>
        <source>Updating profile...</source>
        <translation>Actualizando perfil...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="549"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation>Recibiendo lista de &apos;Siguiendo&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="562"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>Recibiendo lista de &apos;Seguidores&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="581"/>
        <source>Getting site users for %1...</source>
        <comment>%1 is a server name</comment>
        <translation>Recibiendo usuarios del servidor %1...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="633"/>
        <source>Getting list of person lists...</source>
        <translation>Recibiendo lista de listas de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="669"/>
        <source>Creating person list...</source>
        <translation>Creando lista de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="692"/>
        <source>Deleting person list...</source>
        <translation>Borrando lista de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="707"/>
        <source>Getting a person list...</source>
        <translation>Recibiendo una lista de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="751"/>
        <source>Adding person to list...</source>
        <translation>Añadiendo una persona a una lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="792"/>
        <source>Removing person from list...</source>
        <translation>Quitando a una persona de una lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="840"/>
        <source>Creating group...</source>
        <translation>Creando grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="870"/>
        <source>Joining group...</source>
        <translation>Uniéndose al grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="895"/>
        <source>Leaving group...</source>
        <translation>Saliendo del grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1045"/>
        <source>Timeline</source>
        <translation>Línea temporal</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1049"/>
        <source>Messages</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1075"/>
        <source>User timeline</source>
        <translation>Línea temporal de usuario</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1207"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>Subiendo %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1465"/>
        <source>Error loading timeline!</source>
        <translation>¡Error cargando la línea temporal!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1476"/>
        <source>Error loading minor feed!</source>
        <translation>¡Error cargando la línea temporal menor!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1488"/>
        <source>Unable to verify the address!</source>
        <translation>¡No se ha podido verificar la dirección!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1505"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>Error HTTP</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1517"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>Tiempo de espera de la pasarela agotado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1527"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>Servicio no disponible</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1546"/>
        <source>Bad Gateway</source>
        <comment>HTTP 502 error string</comment>
        <translation>Pasarela incorrecta</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1556"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation>No implementado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1566"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>Error interno</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1586"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>Ya no disponible</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1596"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>No encontrado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1606"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>Prohibido</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1616"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>No autorizado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1627"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>Solicitud incorrecta</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1646"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>Movido temporalmente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1656"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>Movido permanentemente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1787"/>
        <source>Server version: %1</source>
        <translation>Versión del servidor: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1790"/>
        <source>Profile received.</source>
        <translation>Perfil recibido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1792"/>
        <source>Followers</source>
        <translation>Seguidores</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1797"/>
        <source>Following</source>
        <translation>Siguiendo</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1813"/>
        <source>Profile updated.</source>
        <translation>Perfil actualizado.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1822"/>
        <source>E-mail updated: %1</source>
        <translation>E-mail actualizado: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1850"/>
        <source>%1 published successfully. Updating post content...</source>
        <comment>%1 is the type of object: note, image...</comment>
        <translation>Se ha publicado &apos;%1&apos; correctamente. Actualizando contenido del mensaje...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1866"/>
        <source>Untitled post %1 published successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Mensaje sin título %1 publicado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1870"/>
        <source>Post %1 published successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Mensaje %1 publicado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1887"/>
        <source>Avatar published successfully.</source>
        <translation>Avatar publicado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1919"/>
        <source>Untitled post %1 updated successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Mensaje sin título %1 actualizado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1923"/>
        <source>Post %1 updated successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Mensaje %1 actualizado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1933"/>
        <source>Comment %1 updated successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Comentario %1 actualizado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1981"/>
        <source>Comment %1 posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Comentario %1 publicado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2985"/>
        <source>%1 attempts</source>
        <translation>%1 intentos</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2989"/>
        <source>1 attempt</source>
        <translation>1 intento</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2992"/>
        <source>Some initial data was not received. Restarting initialization...</source>
        <translation>Algunos datos iniciales no se han recibido. Reiniciando la inicialización...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3410"/>
        <source>Can&apos;t follow %1 at this time.</source>
        <comment>%1 is a user ID</comment>
        <translation>No se puede seguir a %1 en este momento.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3414"/>
        <source>Trying to follow %1.</source>
        <comment>%1 is a user ID</comment>
        <translation>Intentando seguir a %1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3434"/>
        <source>Checking address %1 before following...</source>
        <translation>Comprobando dirección %1 antes de seguirla...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1947"/>
        <source>Message liked or unliked successfully.</source>
        <translation>Mensaje marcado o desmarcado &quot;Me gusta&quot; correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1961"/>
        <source>Likes received.</source>
        <translatorcomment>meh...</translatorcomment>
        <translation>Se han recibido los &quot;me gusta&quot;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2006"/>
        <source>1 comment received.</source>
        <translation>1 comentario recibido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2010"/>
        <source>%1 comments received.</source>
        <translation>%1 comentarios recibidos.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2037"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>Mensaje de %1 compartido correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2068"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Se ha recibido &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2081"/>
        <source>Adding items...</source>
        <translation>Añadiendo elementos...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2607"/>
        <source>SSL errors in connection to %1!</source>
        <translation>¡Errores de SSL en la conexión a %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2619"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation>Cargando imagen externa de %1 a pesar de los errores SSL, como se ha configurado...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2817"/>
        <source>OAuth error while authorizing application.</source>
        <translation>Error de OAuth mientras se autorizaba a la aplicación.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2164"/>
        <source>Message deleted successfully.</source>
        <translation>Mensaje eliminado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="937"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation>Los comentarios de este mensaje no se pueden cargar debido a que faltan datos en el servidor.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="998"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Recibiendo &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1053"/>
        <source>Activity</source>
        <translation>Actividad</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1057"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1062"/>
        <source>Meanwhile</source>
        <translation>Mientras tanto</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1066"/>
        <source>Mentions</source>
        <translation>Menciones</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1070"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2325"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation>Lista de &apos;listas&apos; recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2380"/>
        <source>Person list deleted successfully.</source>
        <translation>Lista de personas borrada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2435"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>Se ha añadido a %1 (%2) a la lista correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>Se ha quitado a %1 (%2) de la lista correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2466"/>
        <source>Group %1 created successfully.</source>
        <translation>Grupo %1 creado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2477"/>
        <source>Group %1 joined successfully.</source>
        <translation>Se ha entrado al grupo %1 correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2490"/>
        <source>Left the %1 group successfully.</source>
        <translation>Se ha salido del grupo %1 correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2563"/>
        <source>Avatar uploaded.</source>
        <translation>Avatar subido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2648"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>La aplicación no está registrada con tu servidor todavía. Registrando...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2685"/>
        <source>Getting OAuth token...</source>
        <translation>Recibiendo identificador de autorización (token) de OAuth...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2706"/>
        <source>OAuth support error</source>
        <translation>Error de soporte de OAuth</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2707"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>Tu instalación de QOAuth, una biblioteca usada por Dianara, no parece tener soporte de HMAC-SHA1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2711"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>Probablemente necesites instalar el conector de OpenSSL para QCA: %1, %2 o similar.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2765"/>
        <location filename="../src/pumpcontroller.cpp" line="2821"/>
        <source>Authorization error</source>
        <translation>Error de autorización</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2766"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>Ha habido un error de OAuth mientras se intentaba obtener un identificador de autorización (token).</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2769"/>
        <source>QOAuth error %1</source>
        <translation>Error de QOAuth %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2801"/>
        <source>Application authorized successfully.</source>
        <translation>Aplicación autorizada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2844"/>
        <source>Waiting for proxy password...</source>
        <translation>Esperando contraseña del proxy...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2873"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>Aún se está esperando el perfil. Intentándolo de nuevo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2999"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>Algunos datos iniciales no se han recibido tras varios intentos. Algo puede estar fallando en tu servidor. Es posible que puedas usar el servicio con normalidad.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3009"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>Se han recibido todos los datos iniciales. Inicialización completada.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3022"/>
        <source>Ready.</source>
        <translation>Preparado.</translation>
    </message>
</context>
<context>
    <name>SiteUsersList</name>
    <message>
        <location filename="../src/siteuserslist.cpp" line="30"/>
        <source>You can get a list of the newest users registered on your server by clicking the button below.</source>
        <translation>Puedes recibir una lista de los usuarios más nuevos registrados en tu servidor, haciendo clic en el botón de abajo.</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="47"/>
        <source>More resources to find users:</source>
        <translation>Más recursos para encontrar usuarios:</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="51"/>
        <source>Wiki page &apos;Users by language&apos;</source>
        <translation>Página wiki &apos;Usuarios por idioma&apos;</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="56"/>
        <source>PPump user search service at inventati.org</source>
        <translation>Servicio PPump de búsqueda de usuarios en inventati.org</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="61"/>
        <source>List of Followers for the Pump.io Community account</source>
        <translation>Lista de los seguidores de la cuenta Pump.io Community</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="41"/>
        <source>Get list of users from your server</source>
        <translation>Recibir lista de usuarios de tu servidor</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="87"/>
        <source>Close list</source>
        <translation>Cerrar la lista</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="143"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="154"/>
        <source>%1 users in %2</source>
        <comment>%1 = user count, %2 = server name</comment>
        <translation>%1 usuarios en %2</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="76"/>
        <source>Welcome to Dianara</source>
        <translation>Bienvenido a Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="78"/>
        <source>Dianara is a &lt;b&gt;Pump.io&lt;/b&gt; client.</source>
        <translation>Dianara es un cliente &lt;b&gt;Pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="88"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>Pulsa &lt;b&gt;F1&lt;/b&gt; si quieres abrir la ventana de ayuda.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="91"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>En primer lugar, configura tu cuenta desde el menú &lt;b&gt;Configuración - Cuenta&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="94"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>Cuando el proceso esté listo, tu perfil y líneas temporales deberían de actualizarse automáticamente.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="98"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>Tómate un momento para echar un vistazo por los menús y la ventana de Configuración.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="102"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>También puedes rellenar tu información de perfil y foto desde el menú &lt;b&gt;Configuración - Editar perfil&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="113"/>
        <source>Dianara&apos;s blog</source>
        <translation>Blog de Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="231"/>
        <source>Newest</source>
        <translation>Lo más nuevo</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="240"/>
        <source>Newer</source>
        <translation>Más nuevos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="263"/>
        <source>Older</source>
        <translation>Más antiguos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="335"/>
        <source>Requesting...</source>
        <translation>Solicitando...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="506"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="615"/>
        <source>Page %1 of %2.</source>
        <translation>Página %1 de %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="619"/>
        <source>Showing %1 posts per page.</source>
        <translation>Mostrando %1 mensajes por página.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="622"/>
        <source>%1 posts in total.</source>
        <translation>%1 mensajes en total.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="626"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>Haz clic aquí o pulsa Control+G para saltar a una página específica</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="740"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>No se puede actualizar &apos;%1&apos; porque se está editando un comentario.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="848"/>
        <source>%1 more posts pending for next update.</source>
        <translation>%1 mensajes más pendientes para la próxima actualización.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="854"/>
        <source>Click here to receive them now.</source>
        <translation>Haz clic aquí para recibirlos ahora.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="1139"/>
        <source>There are no posts</source>
        <translation>No hay mensajes</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="81"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>Si aún no tienes una cuenta Pump, puedes conseguir una en la siguiente dirección, por ejemplo:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="106"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>Hay descripciones emergentes (tooltips) por todas partes, por lo que si mantienes el ratón sobre un botón o un campo de texto, es probable que veas alguna información adicional.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="115"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="126"/>
        <source>Direct Messages Timeline</source>
        <translation>Línea temporal de mensajes directos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="127"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>Aquí verás los mensajes dirigidos específicamente a ti.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="137"/>
        <source>Activity Timeline</source>
        <translation>Línea temporal de actividad</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="138"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>Aquí verás tus propios mensajes.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="147"/>
        <source>Favorites Timeline</source>
        <translation>Línea temporal de favoritos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="148"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>Mensajes y comentarios que te han gustado.</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>¡Hora/fecha no válida!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>Hace un minuto</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>Hace %1 minutos</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>Hace una hora</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>Hace %1 horas</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>Ahora mismo</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>En el futuro</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>Ayer</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>Hace %1 días</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>Hace un mes</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>Hace %1 meses</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>Hace un año</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>Hace %1 años</translation>
    </message>
</context>
<context>
    <name>UserPosts</name>
    <message>
        <location filename="../src/userposts.cpp" line="39"/>
        <source>Posts by %1</source>
        <translation>Mensajes de %1</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="64"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="69"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="153"/>
        <source>Received &apos;%1&apos;.</source>
        <translation>Se ha recibido &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="160"/>
        <source>%1 posts</source>
        <translation>%1 mensajes</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="169"/>
        <source>Error loading the timeline</source>
        <translation>Error cargando la línea temporal</translation>
    </message>
</context>
</TS>
