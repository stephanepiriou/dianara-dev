/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "firstrunwizard.h"

FirstRunWizard::FirstRunWizard(AccountDialog *accountDialog,
                               ProfileEditor *profileEditor,
                               ConfigDialog *configDialog,
                               HelpWidget *helpWidget,
                               GlobalObject *globalObject,
                               QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Welcome Wizard") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("tools-wizard"));
    this->setWindowFlags(Qt::Window);
    this->setMinimumSize(420, 460);
    this->resize(520, 620);

    m_accountDialog = accountDialog;
    m_profileEditor = profileEditor;
    m_configDialog = configDialog;
    m_helpWidget = helpWidget;

    m_explanationLabel = new QLabel("<big><b>" + tr("Welcome to Dianara!")
                                    + "</b></big>"
                                    + "<br><br>"
                                    + tr("This wizard will help you "
                                         "get started.")
                                    + " "
                                    + tr("You can access this window again "
                                         "at any time from the Help menu.")
                                    + "<br><br>"
                                    + tr("The first step is setting up "
                                         "your account, by using the "
                                         "following button:"),
                                    this);
    m_explanationLabel->setWordWrap(true);
    m_explanationLabel->setAlignment(Qt::AlignTop);


    // Config account
    m_configAccountButton = new QPushButton(QIcon::fromTheme("dialog-password",
                                                             QIcon(":/images/button-password.png")),
                                            tr("Configure your &account"),
                                            this);
    connect(m_configAccountButton, &QAbstractButton::clicked,
            m_accountDialog, &QWidget::show);



    // Edit profile
    m_editProfileLabel = new QLabel(tr("Once you have configured your "
                                       "account, it's recommended that you "
                                       "edit your profile and add an avatar "
                                       "and some other information, if you "
                                       "haven't done so already."),
                                    this);
    m_editProfileLabel->setWordWrap(true);
    m_editProfileLabel->setAlignment(Qt::AlignTop);

    m_editProfileButton = new QPushButton(QIcon::fromTheme("user-properties",
                                                           QIcon(":/images/no-avatar.png")),
                                          tr("&Edit your profile"),
                                          this);
    //m_editProfileButton->setDisabled(true); // TMP FIXME
    connect(m_editProfileButton, &QAbstractButton::clicked,
            m_profileEditor, &QWidget::show);



    // Public posting
    m_publicPostsLabel = new QLabel(tr("By default, Dianara will post only "
                                       "to your followers, but it's "
                                       "recommended that you post to "
                                       "Public, at least sometimes."),
                                    this);
    m_publicPostsLabel->setWordWrap(true);
    m_publicPostsLabel->setAlignment(Qt::AlignTop);

    m_publicPostsCheckbox = new QCheckBox(tr("Post to &Public by default"),
                                          this);
    m_publicPostsCheckbox->setChecked(globalObject->getPublicPostsByDefault());



    // Access to program help
    m_helpButton = new QPushButton(QIcon::fromTheme("system-help",
                                                    QIcon(":/images/menu-find.png")),
                                   tr("Open general program &help window"),
                                   this);
    connect(m_helpButton, &QAbstractButton::clicked,
            m_helpWidget, &QWidget::show);


    // Bottom

    m_showAgainCheckbox = new QCheckBox(tr("&Show this again next time "
                                           "Dianara starts"),
                                        this);

    m_closeButton = new QPushButton(QIcon::fromTheme("window-close",
                                                     QIcon(":/images/button-close.png")),
                                    tr("&Close"),
                                    this);
    connect(m_closeButton, &QAbstractButton::clicked,
            this, &QWidget::close);


    // Layout
    m_bottomLayout = new QHBoxLayout();
    m_bottomLayout->addWidget(m_showAgainCheckbox);
    m_bottomLayout->addWidget(m_closeButton, 0, Qt::AlignRight);

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addWidget(m_explanationLabel);
    m_mainLayout->addSpacing(12);
    m_mainLayout->addWidget(m_configAccountButton, 0, Qt::AlignCenter);
    m_mainLayout->addStretch(1);
    m_mainLayout->addWidget(m_editProfileLabel);
    m_mainLayout->addSpacing(12);
    m_mainLayout->addWidget(m_editProfileButton,   0, Qt::AlignCenter);
    m_mainLayout->addStretch(1);
    m_mainLayout->addWidget(m_publicPostsLabel);
    m_mainLayout->addSpacing(12);
    m_mainLayout->addWidget(m_publicPostsCheckbox, 0, Qt::AlignCenter);
    m_mainLayout->addStretch(1);
    m_mainLayout->addSpacing(16);
    m_mainLayout->addWidget(m_helpButton,          0, Qt::AlignCenter);
    m_mainLayout->addStretch(1);
    m_mainLayout->addSpacing(16);
    m_mainLayout->addLayout(m_bottomLayout);
    this->setLayout(m_mainLayout);


    QSettings settings;
    m_showAgainCheckbox->setChecked(settings.value("FirstRunWizard/showWizard",
                                                   true).toBool());

    qDebug() << "FirstRunWizard created";
}

FirstRunWizard::~FirstRunWizard()
{
    qDebug() << "FirstRunWizard destroyed";
}


/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/



/*****************************************************************************/
/********************************* PROTECTED *********************************/
/*****************************************************************************/


void FirstRunWizard::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.setValue("FirstRunWizard/showWizard",
                      m_showAgainCheckbox->isChecked());
    settings.sync();

    // Sync public posting option
    m_configDialog->setPublicPosts(m_publicPostsCheckbox->isChecked());


    this->hide(); // close() would kill the program if mainWindow was hidden
    this->deleteLater();
    event->ignore();
}

