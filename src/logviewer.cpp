/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "logviewer.h"

LogViewer::LogViewer(QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Log") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("text-x-log",
                                         QIcon(":/images/log.png")));
    this->setWindowFlags(Qt::Window);
    this->setMinimumSize(320, 400);

    QSettings settings;
    this->resize(settings.value("LogViewer/logWindowSize",
                                QSize(760, 560)).toSize());


    QList<QKeySequence> closeShortcuts;
    closeShortcuts << QKeySequence(Qt::Key_Escape);
    closeShortcuts << QKeySequence(Qt::Key_F12);

    m_closeAction = new QAction(this);
    m_closeAction->setShortcuts(closeShortcuts);
    connect(m_closeAction, &QAction::triggered,
            this, &QWidget::hide);
    this->addAction(m_closeAction);


    m_logTextBrowser = new QTextBrowser(this);
    m_logTextBrowser->setReadOnly(true);
    m_logTextBrowser->setOpenExternalLinks(true);


    m_clearButton = new QPushButton(QIcon::fromTheme("edit-clear-list",
                                                     QIcon(":/images/button-delete.png")),
                                    tr("Clear &Log"),
                                    this);
    connect(m_clearButton, &QAbstractButton::clicked,
            m_logTextBrowser, &QTextEdit::clear);

    m_closeButton = new QPushButton(QIcon::fromTheme("window-close",
                                                     QIcon(":/images/button-close.png")),
                                    tr("&Close"),
                                    this);
    connect(m_closeButton, &QAbstractButton::clicked,
            this, &QWidget::hide);


    // Layout
    m_buttonsLayout = new QHBoxLayout();
    m_buttonsLayout->addWidget(m_clearButton);
    m_buttonsLayout->addStretch();
    m_buttonsLayout->addWidget(m_closeButton);

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addWidget(m_logTextBrowser);
    m_mainLayout->addLayout(m_buttonsLayout);
    this->setLayout(m_mainLayout);

    qDebug() << "LogViewer created";
}


LogViewer::~LogViewer()
{
    qDebug() << "LogViewer destroyed";
}



void LogViewer::closeEvent(QCloseEvent *event)
{
    this->hide();
    event->ignore();
}

void LogViewer::hideEvent(QHideEvent *event)
{
    QSettings settings;
    if (settings.isWritable())
    {
        settings.setValue("LogViewer/logWindowSize", this->size());
    }

    event->accept();
}


/*
 * Scroll log to bottom when showing
 *
 */
void LogViewer::showEvent(QShowEvent *event)
{
    this->m_logTextBrowser->moveCursor(QTextCursor::End);
    event->accept();
}


/****************************************************************************/
/********************************* SLOTS ************************************/
/****************************************************************************/



void LogViewer::addToLog(QString message, QString url)
{
    QString logLine = "<b>["
                      + QDateTime::currentDateTime().toString(Qt::DefaultLocaleShortDate)
                      + "]</b> ";
    logLine.append(message);

    if (!url.isEmpty())
    {
        logLine.append(": <a href=\"" + url + "\">"
                       + MiscHelpers::elidedText(url, 40)
                       + "</a>");
    }

    m_logTextBrowser->append(logLine);
}


void LogViewer::toggleVisibility()
{
    if (this->isVisible())
    {
        this->hide();
    }
    else
    {
        this->show();
    }
}
