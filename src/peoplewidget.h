/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PEOPLEWIDGET_H
#define PEOPLEWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListView>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <QDebug>


#include "pumpcontroller.h"


class PeopleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PeopleWidget(QString buttonText,
                          int type,
                          PumpController *pumpController,
                          QWidget *parent = 0);
    ~PeopleWidget();

    void resetWidget();
    QStandardItem *createContactItem(ASPerson *contact);


    enum WidgetType { EmbeddedWidget, StandaloneWidget };

signals:
    void contactSelected(QIcon contactIcon, QString contactString,
                         QString contactName, QString contactId,
                         QString contactUrl);
    void addButtonPressed(QIcon contactIcon, QString contactString,
                          QString contactName, QString contactId,
                          QString contactUrl);


public slots:
    void filterList(QString searchTerms);
    void updateAllContactsList(QString listType,
                               QVariantList contactsVariantList,
                               int totalReceivedCount);
    void addContact(ASPerson *contact);
    void removeContact(ASPerson *contact);

    void returnContact();
    void returnClickedContact(QModelIndex modelIndex);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_buttonsLayout;

    QLabel *m_searchLabel;
    QLineEdit *m_searchLineEdit;

    QListView *m_allContactsListView;
    QStandardItemModel *m_itemModel;
    QSortFilterProxyModel *m_filterModel;

    QPushButton *m_addToButton;
    QPushButton *m_cancelButton;


    PumpController *m_pumpController;
};

#endif // PEOPLEWIDGET_H
