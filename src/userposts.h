/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef USERPOSTS_H
#define USERPOSTS_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QIcon>
#include <QPushButton>
#include <QSettings>

#include <QDebug>

#include "timeline.h"
#include "filterchecker.h"
#include "pumpcontroller.h"
#include "globalobject.h"


class UserPosts : public QWidget
{
    Q_OBJECT

public:
    UserPosts(QString userId, QString userName,
              QIcon userAvatar, QString userOutbox,
              PumpController *pumpController,
              GlobalObject *globalObject,
              FilterChecker *filterChecker,
              QWidget *parent);
    ~UserPosts();


signals:


public slots:
    void fillTimeLine(QVariantList postList, QString previousLink,
                     QString nextLink, int totalItems, QString url);
    void notifyTimelineUpdate();
    void onTimelineFailed();
    void scrollTimelineTo(QAbstractSlider::SliderAction sliderAction);


protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void closeEvent(QCloseEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_bottomLayout;

    QScrollArea *m_scrollArea;
    TimeLine *m_timeline;

    QLabel *m_infoLabel;
    QPushButton *m_closeButton;

    QString m_timelineTitle;
    QString m_timelineUrl;
    QString m_userInfoString;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // USERPOSTS_H
