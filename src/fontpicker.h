/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef FONTPICKER_H
#define FONTPICKER_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFontDialog>

#include <QDebug>


class FontPicker : public QWidget
{
    Q_OBJECT

public:
    explicit FontPicker(QString description,
                        QString initialFontString,
                        QWidget *parent = 0);
    ~FontPicker();

    void updateFontSample();

    QString getFontInfo();


signals:


public slots:
    void selectFont();


private:
    QHBoxLayout *m_layout;

    QLabel *m_descriptionLabel;
    QLineEdit *m_sampleLineEdit;

    QPushButton *m_button;


    QFont m_currentFont;
};

#endif // FONTPICKER_H
