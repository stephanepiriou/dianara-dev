/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "filtermatcheswidget.h"


FilterMatchesWidget::FilterMatchesWidget(QVariantMap filterMatchesMap,
                                         QWidget *parent) : QFrame(parent)
{
    this->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);

    QString filterMatchInfoString;
    QString matchingStuff;


    matchingStuff = tagList(filterMatchesMap.value("matchingContent").toStringList());
    if (!matchingStuff.isEmpty())
    {
        // FIXME: Maybe replace "category" strings with symbols
        filterMatchInfoString.append(tr("Content",
                                        "The contents of the post matched") + ": "
                                     + matchingStuff + " ");
    }

    matchingStuff = tagList(filterMatchesMap.value("matchingAuthor").toStringList());
    if (!matchingStuff.isEmpty())
    {
        filterMatchInfoString.append(" " + tr("Author") + ": "
                                     + matchingStuff + " ");
    }

    matchingStuff = tagList(filterMatchesMap.value("matchingGenerator").toStringList());
    if (!matchingStuff.isEmpty())
    {
        filterMatchInfoString.append(" " + tr("App",
                                              "Application, short if possible") + ": "
                                     + matchingStuff + " ");
    }

    matchingStuff = tagList(filterMatchesMap.value("matchingDescription").toStringList());
    if (!matchingStuff.isEmpty())
    {
        filterMatchInfoString.append(" " + tr("Description") + ": "
                                     + matchingStuff);
    }


    m_contentLabel = new QLabel("<style>span "
                                "{ font-weight: bold;                              "
                                "  color: palette(highlighted-text);               "
                                "  background-color: qlineargradient(spread:pad,   "
                                "                     x1:0, y1:0, x2:1, y2:0,      "
                                "                     stop:0   rgba(0, 0, 0, 0),   "
                                "                     stop:0.1 palette(highlight), "
                                "                     stop:0.9 palette(highlight), "
                                "                     stop:1   rgba(0, 0, 0, 0));  "
                                "}"
                                "</style>"
                                + filterMatchInfoString,
                                this);
    m_contentLabel->setWordWrap(true);
    m_contentLabel->setAlignment(Qt::AlignCenter);



    m_layout = new QHBoxLayout();
    m_layout->addWidget(m_contentLabel);
    this->setLayout(m_layout);

    qDebug() << "FilterMatchesWidget created";
}


FilterMatchesWidget::~FilterMatchesWidget()
{
    qDebug() << "FilterMatchesWidget destroyed";
}


QString FilterMatchesWidget::tagList(QStringList tagsList)
{
    QString tagListString;

    foreach (const QString tag, tagsList)
    {
        // Non-breakable spaces needed in several places
        tagListString.append("<span>&nbsp;&nbsp;"
                             + tag
                             + "&nbsp;&nbsp;</span>"
                               "&nbsp; "); // Space needed after, for wordwrap
    }

    return tagListString;
}

