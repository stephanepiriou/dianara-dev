/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QWidget>
#include <QLabel>
#include <QIcon>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QMovie>
#include <QScrollArea>
#include <QGraphicsPixmapItem>
#include <QGraphicsProxyWidget>
#include <QTimer>

#include <QDebug>

#include "mischelpers.h"
#include "ivgraphicsview.h"


class ImageViewer : public QWidget
{
    Q_OBJECT

public:
    explicit ImageViewer(QString url, QSize imageSize,
                         QString title, QString suggestedFilename,
                         bool isAnimated,
                         QWidget *parent = 0);
    ~ImageViewer();
    void createContextMenu();
    void loadImage(QString url, QSize expectedSize);
    void drawImage();
    void toggleZoomButtons();
    void updateButtons();


signals:


public slots:
    void reloadImage(QString url);
    void onImageFailed(QString url);

    void saveImage();
    void restartAnimation();

    void zoomToFit();
    void zoomToFull();
    void setFullMode();
    void zoomIn();
    void zoomOut();
    void checkAutoFit();

    void rotateLeft();
    void rotateRight();

    void onFitFullToggled();


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void showEvent(QShowEvent *event);
    virtual void hideEvent(QHideEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *event);


private:
    QVBoxLayout *m_mainLayout;

    QLabel *m_loadingLabel;
    QGraphicsPixmapItem *m_graphicsPixmapItem;
    QGraphicsScene *m_graphicsScene;
    IvGraphicsView *m_ivGraphicsView;

    QHBoxLayout *m_buttonsLayout;
    QPushButton *m_saveButton;
    QPushButton *m_restartButton;

    QHBoxLayout *m_fitFullLayout;
    QPushButton *m_fitButton;
    QLabel *m_zoomLabel;
    QPushButton *m_fullButton;

    QPushButton *m_zoomInButton;
    QPushButton *m_zoomOutButton;

    QPushButton *m_rotateLeftButton;
    QPushButton *m_rotateRightButton;

    QLabel *m_infoLabel;
    QPushButton *m_closeButton;


    QMenu *m_contextMenu;
    QAction *m_saveAction;
    QAction *m_toggleFitFullAction;
    QAction *m_rotateLeftAction;
    QAction *m_rotateRightAction;
    QAction *m_closeAction;

    QTimer *m_autoFitTimer;


    QString m_imageUrl;

    QPixmap m_originalPixmap;
    QString m_originalFileUri;

    bool m_imageIsAnimated;
    QLabel *m_movieLabel;
    QMovie *m_movie;
    QGraphicsProxyWidget *m_labelProxyWidget;

    QString m_suggestedFilename;

    bool m_fitToWindow;
    double m_zoomLevel;
    double m_rotationAngle;
};

#endif // IMAGEVIEWER_H
