<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="89"/>
        <location filename="../src/asactivity.cpp" line="135"/>
        <source>Public</source>
        <translation>Öffentlich</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="422"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 von %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="275"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translatorcomment>passende Übersetzung von dict.leo.org</translatorcomment>
        <translation>Mitteilung</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="280"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>Artikel</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="285"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="290"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="295"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="300"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="305"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="310"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="315"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>Sammlung</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="320"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>Sonstiges</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="376"/>
        <source>No detailed location</source>
        <translation>kein genauer Ort</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="402"/>
        <source>Deleted on %1</source>
        <translation>Gelöscht am %1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="578"/>
        <location filename="../src/asobject.cpp" line="648"/>
        <source>and one other</source>
        <translatorcomment>Kommt darauf an ob es nur um weitere &apos;Kommentare&apos; oder auch um &apos;Notizen&apos; o.ä. geht.</translatorcomment>
        <translation>und ein(e) Weitere(r)</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="582"/>
        <location filename="../src/asobject.cpp" line="652"/>
        <source>and %1 others</source>
        <translation>und %1 weitere</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="173"/>
        <source>Hometown</source>
        <translation>Heimatort</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="85"/>
        <source>Your Pump.io address:</source>
        <translation>Ihre Pump.io Adresse:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="95"/>
        <source>Get &amp;Verifier Code</source>
        <translation>&amp;Verifizierungskode erhalten</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="128"/>
        <source>Verifier code:</source>
        <translation>Verifizierungskode:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="131"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translatorcomment>(Jan) This should be updated with the new &quot;or paste&quot; part --&gt; Done!</translatorcomment>
        <translation>Hier den von Ihrem Pump-Server bereitgestellten Verifizierungskode eingeben oder einfügen</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="168"/>
        <source>&amp;Save Details</source>
        <translation>Daten &amp;speichern</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="401"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>Falls der Browser sich nicht automatisch öffnet, kopieren Sie die folgende Adresse von Hand</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>Konto konfigurieren</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="50"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>Geben Sie zuerst Ihre Webfinger-ID (Ihre Pump.io Adresse) ein.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="53"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>Ihre Adresse sieht aus wie &apos;Benutzer@pumpeserver.org&apos;. Sie können diese über die Web-Oberfläche in Ihrem Profil finden.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="58"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>Für ein Profil auf &apos;https://pump.beispiel/benutzer&apos;, ist Ihre zugehörige Adresse &apos;benutzer@pump.beispiel&apos;</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="62"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>Falls Sie noch kein Konto haben, können Sie sich unter %1 anmelden. Dieser Link zeigt auf einen zufälligen öffentlichen Server.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="70"/>
        <source>If you need help: %1</source>
        <translation>Falls Sie Hilfe benötigen: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="73"/>
        <source>Pump.io User Guide</source>
        <translation>Pump.io Leitfaden</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="88"/>
        <source>Your address, like username@pumpserver.org</source>
        <translatorcomment>(Jan) took this one from another string that said &quot;as username@...&quot;; it might need a different translation here</translatorcomment>
        <translation>Ihre Adresse, als benutzer@pumpserver.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="98"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>Nachdem Sie diese Schaltfläche gedrückt haben, wird sich ein Webbrowser öffnen, um die Autorisierung für Dianara anzufordern</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="109"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>Sobald Sie Dianara von der Oberfläche Ihres Pump-Servers aus autorisiert haben, werden Sie einen Code namens &apos;VERIFIER&apos; erhalten. Kopieren Sie diesen und fügen Sie ihn in das Feld unterhalb ein.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="141"/>
        <source>&amp;Authorize Application</source>
        <translation>Anwendung &amp;autorisieren</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="176"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="188"/>
        <source>Your account is properly configured.</source>
        <translation>Ihr Konto ist korrekt konfiguriert.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="192"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>Klicken Sie &apos;Entsperren&apos;, falls Sie ein anderes Konto konfigurieren möchten.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="200"/>
        <source>&amp;Unlock</source>
        <translatorcomment>Is this shortcut (&amp;E) available?</translatorcomment>
        <translation>&amp;Entsperren</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="327"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>Ein Browser wird jetzt gestartet, um den Verifizierungskode zu erhalten</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="336"/>
        <source>Your Pump address is invalid</source>
        <translation>Ihre Pump-Adresse ist ungültig</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="355"/>
        <source>Verifier code is empty</source>
        <translation>Verifizierungskode ist leer</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="382"/>
        <source>Dianara is authorized to access your data</source>
        <translation>Dianara hat autorisierten Zugang zu Ihre Daten</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="410"/>
        <source>Unable to open web browser!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="33"/>
        <source>&apos;To&apos; List</source>
        <translation>Empfänger Liste</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="37"/>
        <source>&apos;Cc&apos; List</source>
        <translation>CC-Empfänger Liste</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="58"/>
        <source>&amp;Add to Selected</source>
        <translatorcomment>unsicher über Position von &apos;&amp;&apos;</translatorcomment>
        <translation>Zur Auswahl &amp;hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="73"/>
        <source>All Contacts</source>
        <translation>Alle Kontakte</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="79"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <comment>ON THE LEFT should change to ON THE RIGHT in RTL languages</comment>
        <translation>Wählen Sie Kontakte aus der Liste links aus.
Sie können diese mit der Maus ziehen, einfach oder doppelt anklicken oder sie auswählen und die Schaltfläche unten verwenden.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="96"/>
        <source>Clear &amp;List</source>
        <translation>&amp;Liste leeren</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="120"/>
        <source>&amp;Done</source>
        <translation>&amp;Fertig</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="127"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="168"/>
        <location filename="../src/audienceselector.cpp" line="386"/>
        <source>Public</source>
        <translation type="unfinished">Öffentlich</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="173"/>
        <location filename="../src/audienceselector.cpp" line="392"/>
        <source>Followers</source>
        <translation type="unfinished">Anhängerschaft</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="178"/>
        <source>Lists</source>
        <translation type="unfinished">Listen</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="188"/>
        <source>People...</source>
        <translation type="unfinished">Leute...</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="108"/>
        <source>Selected People</source>
        <translation>Ausgewählte Kontakte</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="143"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>Profil von %1 im Browser öffnen</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="139"/>
        <source>Open your profile in web browser</source>
        <translation>Ihres Profil im Browser öffnen</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="165"/>
        <source>Send message to %1</source>
        <translation>Sende eine Nachricht an %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="174"/>
        <source>Browse messages</source>
        <translation>Nachrichten durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="230"/>
        <source>Stop following</source>
        <translation>Nicht mehr folgen</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="241"/>
        <source>Follow</source>
        <translation>Folgen</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="287"/>
        <source>Stop following?</source>
        <translation>Nicht mehr folgen?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="288"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Sind Sie sicher, dass Sie %1 nicht mehr folgen wollen?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="291"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Ja, nicht mehr folgen</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="292"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
</context>
<context>
    <name>BannerNotification</name>
    <message>
        <location filename="../src/bannernotification.cpp" line="38"/>
        <source>Timelines were not automatically updated to avoid interruptions.</source>
        <translation>Die Zeitleisten wurden nicht automatisch aktualisiert, um Unterbrechungen zu verhindern.</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="47"/>
        <source>This happens when it is time to autoupdate the timelines, but you are not at the top of the first page, to avoid interruptions while you read</source>
        <translatorcomment>translation uses the equivalent of &quot;and&quot; instead of &quot;but&quot; for style reasons.
Maybe &quot;Zeitleistenaktualisierung&quot; is too long in this notification?</translatorcomment>
        <translation>Dies geschieht, wenn eine automatische Zeitleistenaktualisierung ansteht und Sie sich gerade nicht oben auf der ersten Seite befinden, um Unterbrechungen beim Lesen zu vermeiden</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="55"/>
        <source>Update now</source>
        <translation>Jetzt aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="65"/>
        <source>Hide this message</source>
        <translatorcomment>&quot;Hinweis&quot; is correct if this is a tooltip-message</translatorcomment>
        <translation>Diesen Hinweis verbergen</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="35"/>
        <source>Change...</source>
        <translation>Wechseln...</translation>
    </message>
    <message>
        <location filename="../src/colorpicker.cpp" line="114"/>
        <source>Choose a color</source>
        <translation>Wählen Sie eine Farbe</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="251"/>
        <source>Posted on %1</source>
        <translation>Am %1 veröffentlicht</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="256"/>
        <source>Modified on %1</source>
        <translation>Am %1 verändert</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="114"/>
        <source>Like or unlike this comment</source>
        <translation>Diesen Kommentar favorisieren / defavorisieren</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="121"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>Zitieren</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="129"/>
        <source>Reply quoting this comment</source>
        <translation>Antwort mit Zitat dieses Kommentars</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="136"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="142"/>
        <source>Modify this comment</source>
        <translation>Diesen Kommentar bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="148"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="154"/>
        <source>Erase this comment</source>
        <translation>Diesen Kommentar löschen</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="287"/>
        <source>Unlike</source>
        <translation>Defavorisieren</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="291"/>
        <source>Like</source>
        <translation>Favorisieren</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="311"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>%1 gefällt dieser Kommentar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="305"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>%1 gefällt dieser Kommentar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="547"/>
        <source>WARNING: Delete comment?</source>
        <translation>WARNUNG: Kommentar löschen?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="548"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>Sind Sie sicher, dass Sie diesen Kommentar löschen wollen?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Ja, löschen</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="127"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>Sie können Strg+Eingabe auf der Tastatur drücken, um den Kommentar abzusenden</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="42"/>
        <source>Reload comments</source>
        <translation>Kommentare neu laden</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="123"/>
        <location filename="../src/commenterblock.cpp" line="510"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>Kommentieren</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="134"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="137"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>Drücken Sie ESC um den Kommentar abzubrechen, wenn das Textfeld leer ist</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="363"/>
        <source>Check for comments</source>
        <translation>Auf Kommentare überprüfen</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="367"/>
        <source>Show all %1 comments</source>
        <translation>Zeige alle %1 Kommentare</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="393"/>
        <source>Comments are not available</source>
        <translation>Kommentare sind nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="558"/>
        <source>Error: Already composing</source>
        <translation>Fehler: Bereits am Kommentieren</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="559"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>Sie können jetzt keinen Kommentar bearbeiten, weil bereits ein anderer Kommentar erstellt wird.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="574"/>
        <source>Editing comment</source>
        <translation>Kommentar bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="583"/>
        <source>Loading comments...</source>
        <translatorcomment>(Jan: rough approximation, copied from the &quot;Getting comments&quot; string)</translatorcomment>
        <translation type="unfinished">Rufe Kommentare ab...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="650"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>Veröffentlichung des Kommentars schlug fehl.

Versuchen Sie es noch einmal.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="667"/>
        <source>An error occurred</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="691"/>
        <source>Sending comment...</source>
        <translation>Kommentar wird gesendet...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="696"/>
        <source>Updating comment...</source>
        <translation>Kommentar wird aktualisiert...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="705"/>
        <source>Comment is empty.</source>
        <translation>Kommentar ist leer.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="175"/>
        <source>Type a message here to post it</source>
        <translatorcomment>&quot;Type your post here&quot;</translatorcomment>
        <translation>Schreiben Sie hier Ihren Beitrag</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="33"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>Klicken Sie hier oder drücken Sie Strg+N um eine Mitteilung zu schreiben...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="37"/>
        <source>Symbols</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="55"/>
        <source>Formatting</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="56"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Bold</source>
        <translation>Fett</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="65"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="70"/>
        <source>Underline</source>
        <translation>Unterstrichen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="75"/>
        <source>Strikethrough</source>
        <translation>Durchgestrichen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="82"/>
        <source>Header</source>
        <translation>Vorspann</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="87"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="91"/>
        <source>Table</source>
        <translation>Tabelle</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="95"/>
        <source>Preformatted block</source>
        <translation>Vorformattierter Bereich</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="99"/>
        <source>Quote block</source>
        <translation>Zitat</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="107"/>
        <source>Make a link</source>
        <translatorcomment>Insert link</translatorcomment>
        <translation>Link hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="112"/>
        <source>Insert an image from a web site</source>
        <translation>Bild von einer Webseite hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="117"/>
        <source>Insert line</source>
        <translation>Linie hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="129"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;Format</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="134"/>
        <source>Text Formatting Options</source>
        <translation>Textformateinstellungen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="139"/>
        <source>Paste Text Without Formatting</source>
        <translation>Text ohne Formatierung einfügen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="181"/>
        <source>Type a comment here</source>
        <translation>Geben Sie hier einen Kommentar ein</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="342"/>
        <source>You can attach only one file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="350"/>
        <source>You cannot drop folders here, only a single file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="500"/>
        <source>Insert as image?</source>
        <translation>Als Bild einfügen?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="501"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>Der Link den Sie gerade einfügen scheint auf eine Bilddatei zu zeigen.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="504"/>
        <source>Insert as visible image</source>
        <translation>Als sichtbares Bild einfügen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="505"/>
        <source>Insert as link</source>
        <translation>Als Link einfügen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="734"/>
        <source>Table Size</source>
        <translation>Tabellengröße</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="738"/>
        <source>How many rows (height)?</source>
        <translation>Wie viele Zeilen (Höhe)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="749"/>
        <source>How many columns (width)?</source>
        <translation>Wie viele Spalten (Breite)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="832"/>
        <source>Insert a link</source>
        <translation>Link hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="833"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>Tippen oder fügen Sie hier eine Webadresse ein.
Sie können auch zuerst Text auswählen, um diesen in einen Link umzuwandeln.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="848"/>
        <source>Make a link from selected text</source>
        <translation>Link aus ausgewähltem Text erstellen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="849"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>Tippen oder fügen Sie hier eine Webadresse ein.
Der ausgewählte Text (%1) wird in einen Link umgewandelt.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="878"/>
        <source>Invalid link</source>
        <translation>Ungültiger Link</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="879"/>
        <source>The text you entered does not look like a link.</source>
        <translation>Der eingegebene Text sieht nicht wie ein Link aus.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="882"/>
        <source>It should start with one of these types:</source>
        <comment>It = the link, from previous sentence</comment>
        <translatorcomment>assuming &quot;type&quot; means &apos;character&apos; or &apos;letter&apos;</translatorcomment>
        <translation>Er sollte mit einem dieser Lettern beginnen:</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="896"/>
        <source>&amp;Use it anyway</source>
        <translation>&amp;Trotzdem verwenden</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="897"/>
        <source>&amp;Enter it again</source>
        <translation>Nochmals &amp;eingeben</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="898"/>
        <source>&amp;Cancel link</source>
        <translation>Link &amp;verwerfen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="931"/>
        <source>Insert an image from a URL</source>
        <translation>Fügen Sie ein Bild aus einer URL ein</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="932"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>Tippen oder fügen Sie hier die Adresse für ein Bild ein.
Dieser Link muss direkt auf die Bilddatei verweisen.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="949"/>
        <source>Error: Invalid URL</source>
        <translation>Fehler: Ungültige URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="950"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translatorcomment>&quot;Addresses should begin with...&quot;</translatorcomment>
        <translation>Die Adresse, dass Sie hinzugefügt haben (%1) ist nicht gültig.
Adressen sollen mit http:// oder https:// anfangen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1052"/>
        <source>Yes, but saving a &amp;draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1057"/>
        <source>Cancel message?</source>
        <translatorcomment>&quot;Discard message?&quot;</translatorcomment>
        <translation>Beitrag verwerfen?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1058"/>
        <source>Are you sure you want to cancel this message?</source>
        <translatorcomment>&quot;(...) discard this message?&quot;</translatorcomment>
        <translation>Sind Sie sicher, dass Sie diesen Beitrag verwerfen wollen?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1060"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;Ja, abbrechen</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1062"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="245"/>
        <source>minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Top</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="253"/>
        <source>Bottom</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>Programmeinstellungen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="289"/>
        <source>Timeline &amp;update interval</source>
        <translation>Akt&amp;ualisierungsintervall der Zeitleiste</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="464"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>Beiträge &amp;pro Seite, Haupt-Zeitleiste</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="407"/>
        <location filename="../src/configdialog.cpp" line="414"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>Beiträge</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="466"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>Beiträge pr&amp;o Seite, andere Zeitleisten</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="292"/>
        <source>&amp;Tabs position</source>
        <translation>Stelle für &amp;Tableiste</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="294"/>
        <source>&amp;Movable tabs</source>
        <translation>Be&amp;wegliche Tableiste</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="566"/>
        <source>Public posts as &amp;default</source>
        <translation>Öffentliche Beiträge als Stan&amp;dard</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="268"/>
        <source>Pro&amp;xy Settings</source>
        <translation>Pro&amp;xy-Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="297"/>
        <source>Network configuration</source>
        <translation>Netzwerkkonfiguration</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="275"/>
        <source>Set Up F&amp;ilters</source>
        <translation>F&amp;ilter einrichten</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="299"/>
        <source>Filtering rules</source>
        <translation>Filterregeln</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="322"/>
        <source>Minor Feeds</source>
        <translatorcomment>(Jan) need to update it to plural --&gt; Done!</translatorcomment>
        <translation>Nebenfeeds</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="430"/>
        <source>Highlighted activities, except mine</source>
        <translation>Hervorgehobene Aktivitäten, außer meine</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="431"/>
        <source>Any highlighted activity</source>
        <translation>Alle hervorgehobenen Aktivitäten</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="432"/>
        <source>Always</source>
        <translation>Immer</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="433"/>
        <source>Never</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="438"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>Zeichen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="477"/>
        <source>Snippet limit</source>
        <translation>Maximale Schnipsel-Länge</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="313"/>
        <source>Post Titles</source>
        <translatorcomment>&quot;Titles of Posts&quot;</translatorcomment>
        <translation>Beitragstitel</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="316"/>
        <source>Post Contents</source>
        <translatorcomment>&quot;Contents of Posts&quot;</translatorcomment>
        <translation>Beitragsinhalte</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="319"/>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="347"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>Sie sind unter den Empfängern der Aktivität, z.B. eines Kommentars, der an Sie gerichtet ist.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="351"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translatorcomment>&quot;Is also used to highlight posts addressed to you in the timelines.&quot;</translatorcomment>
        <translation>Wird auch verwendet, um an Sie gerichtete Beiträge in den Zeitleisten hervorzuheben.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="356"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>Die Aktivität ist eine Reaktion auf eine Ihrer Aktionen, z.B. ein Kommentar, der als Antwort auf eine Ihrer Mitteilungen veröffentlicht wurde.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="362"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translatorcomment>&quot;(...), e.g. if you&apos;ve been added to a list.&quot;</translatorcomment>
        <translation>Sie sind das Ziel dieser Aktivität, z.B. , wenn Sie zu einer Liste hinzugefügt wurden.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="367"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>Die Aktivität bezieht sich auf eines Ihrer Objekte, z.B., wenn einer Ihrer Beiträge favorisiert wurde.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="371"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>Wird auch verwendet, um Ihre eigenen Beiträge in den Zeitleisten hervorzuheben.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="376"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>Element wurde wegen einer Filterregel hervorgehoben.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="381"/>
        <source>Item is new.</source>
        <translation>Element ist neu.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="475"/>
        <source>Show snippets in minor feeds</source>
        <translatorcomment>(Jan) Needs to be updated to plural, &quot;feeds&quot;</translatorcomment>
        <translation>Schnipsel aus Nebenfeeds anzeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="468"/>
        <source>Show information for deleted posts</source>
        <translation>Informationen über gelöschte Beiträge anzeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="451"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="452"/>
        <source>Before avatar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="453"/>
        <source>Before avatar, subtle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="454"/>
        <source>After avatar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="455"/>
        <source>After avatar, subtle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="470"/>
        <source>Hide duplicated posts</source>
        <translation>Doppelte Beiträge verbergen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="472"/>
        <source>Jump to new posts line on update</source>
        <translatorcomment>Suggestion: Could &apos;line&apos; be omitted from the original string?</translatorcomment>
        <translation type="unfinished">Beim Aktualisieren zur Zeile des neuen Beitrags springen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="479"/>
        <source>Snippet limit when highlighted</source>
        <translation>Maximale Schnipsel-Länge, wenn hervorgehoben</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="481"/>
        <source>Minor feed avatar sizes</source>
        <translation>Avatargröße in Nebenfeeds</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="483"/>
        <source>Show activity icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="527"/>
        <source>Avatar size</source>
        <translatorcomment>(Jan) I tried my luck xD</translatorcomment>
        <translation>Avatargröße</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="529"/>
        <source>Avatar size in comments</source>
        <translation>Avatargröße in Kommentaren</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="531"/>
        <source>Show extended share information</source>
        <translation>Ausführliche &apos;Teilen&apos;-Informationen anzeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="533"/>
        <source>Show extra information</source>
        <translation>Zusätzliche Informationen anzeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="535"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>Kommentare des Beitragsautors hervorheben</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="537"/>
        <source>Highlight your own comments</source>
        <translation>Eigene Kommentare hervorheben</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="539"/>
        <source>Ignore SSL errors in images</source>
        <translatorcomment>**Suggestion**: &quot;from images&quot; instead? **Rationale**: The errors aren&apos;t in the images themselves, but come *from* loading them. (At least that&apos;s how I understand it.)</translatorcomment>
        <translation type="unfinished">SSL-Fehler in Bilddateien ignorieren</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="541"/>
        <source>Show full size images</source>
        <translation>Bilder in voller Größe anzeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="571"/>
        <source>Show character counter</source>
        <translatorcomment>&quot;character count&quot;</translatorcomment>
        <translation>Zeichenanzahl anzeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="596"/>
        <source>Don&apos;t inform followers when following someone</source>
        <translation>&apos;Anhängerschaft&apos; nicht informieren, wenn ich jemandem folge</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="599"/>
        <source>Don&apos;t inform followers when handling lists</source>
        <translation>&apos;Anhängerschaft&apos; nicht informieren, wenn ich Listen bearbeite</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="619"/>
        <source>As system notifications</source>
        <translation>Als Systembenachrichtigungen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="621"/>
        <source>Using own notifications</source>
        <translatorcomment>&quot;Use own notification system&quot;</translatorcomment>
        <translation>Eigenes Benachrichtigungssytem benutzen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="623"/>
        <source>Don&apos;t show notifications</source>
        <translation>Keine Benachrichtigungen zeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="637"/>
        <source>seconds</source>
        <comment>Next to a duration, in seconds</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="675"/>
        <source>Notification Style</source>
        <translation>Stil der Benachrichtigungen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="679"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="681"/>
        <source>Persistent Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="683"/>
        <source>Also highlight taskbar entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="689"/>
        <source>Notify when receiving:</source>
        <translation>Benachrichtigen beim Empfang von:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="693"/>
        <source>New posts</source>
        <translatorcomment>translated as completion of the sentence &quot;Notify when receiving: New Posts&quot;</translatorcomment>
        <translation>neuen Beiträgen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="695"/>
        <source>Highlighted posts</source>
        <translatorcomment>translated as completion of the sentence &quot;Notify when receiving: Highlighted posts&quot;</translatorcomment>
        <translation>hervorgehobenen Beiträgen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="697"/>
        <source>New activities in minor feed</source>
        <translatorcomment>translated as completion of the sentence &quot;Notify when receiving: New activities in minor feed&quot;</translatorcomment>
        <translation>neuen Aktivitäten im Nebenfeed</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="699"/>
        <source>Highlighted activities in minor feed</source>
        <translation>hervorgehobenen Aktivitäten im Nebenfeed</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="701"/>
        <source>Important errors</source>
        <translation>Wichtige Fehler</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="716"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="717"/>
        <source>System iconset, if available</source>
        <translation>Systemeigenes Iconset, falls verfügbar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="718"/>
        <source>Show your current avatar</source>
        <translation>Ihren derzeitigen Avatar zeigen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="719"/>
        <source>Custom icon</source>
        <translation>Benutzerdefiniertes Symbol</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="753"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>Symbol&amp;typ fürs System-Benachrichtigungsfeld</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="726"/>
        <source>S&amp;elect...</source>
        <translation>Aus&amp;wählen...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="755"/>
        <source>Custom &amp;Icon</source>
        <translation>Benutzerdef&amp;iniertes Symbol</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="757"/>
        <source>Hide window on startup</source>
        <translation>Fenster beim Starten verstecken</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="113"/>
        <source>General Options</source>
        <translation>Allgemeine Optionen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="117"/>
        <source>Fonts</source>
        <translation>Schriftarten</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="121"/>
        <source>Colors</source>
        <translation>Farben</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="125"/>
        <source>Timelines</source>
        <translation>Zeitleisten</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="129"/>
        <source>Posts</source>
        <translation>Beiträge</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="133"/>
        <source>Composer</source>
        <translatorcomment>&quot;post-editor&quot;</translatorcomment>
        <translation type="unfinished">Beitragseditor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="137"/>
        <source>Privacy</source>
        <translation>Privatsphäre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="141"/>
        <source>Notifications</source>
        <translation>Benachrichtigungen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="145"/>
        <source>System Tray</source>
        <translation>System-Benachrichtigungsfeld</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="814"/>
        <source>This is a system notification</source>
        <translation>Dies ist eine Systembenachrichtigung</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="818"/>
        <source>System notifications are not available!</source>
        <translation>Systembenachrichtigungen sind nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="820"/>
        <source>Own notifications will be used.</source>
        <translation>Eigene Benachrichtigungen werden verwendet.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="830"/>
        <source>This is a basic notification</source>
        <translation>Dies ist eine einfache Benachrichtigung</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1027"/>
        <source>Select custom icon</source>
        <translation>Benutzerdefiniertes Symbol auswählen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="175"/>
        <source>Dianara stores data in this folder:</source>
        <translation>Dianara speichert Daten in diesen Ordner:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="255"/>
        <source>Left side</source>
        <comment>tabs on left side/west; RTL not affected</comment>
        <translation>Linke Seite</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="258"/>
        <source>Right side</source>
        <comment>tabs on right side/east; RTL not affected</comment>
        <translation>Rechte Seite</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="515"/>
        <source>Only for images inserted from web sites.</source>
        <translation>Nur für Bilder von Webseiten.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="518"/>
        <source>Use with care.</source>
        <translation>Bitte vorsichtig einsetzen.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="568"/>
        <source>Use attachment filename as initial post title</source>
        <translation>Dateinamen des Anhangs als Beitragstitel verwenden</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="602"/>
        <source>Inform only the author when liking things</source>
        <translation>Nur den Autor benachrichtigen, wenn ich Dinge favorisiere</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="186"/>
        <source>&amp;Save Configuration</source>
        <translation>Konfiguration &amp;speichern</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="193"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1029"/>
        <source>Image files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1032"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1052"/>
        <source>Invalid image</source>
        <translation>Ungültige Bilddatei</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1053"/>
        <source>The selected image is not valid.</source>
        <translation>Die ausgewählte Bilddatei ist nicht gültig.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="71"/>
        <source>Hometown</source>
        <translation>Heimatort</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="78"/>
        <source>Joined: %1</source>
        <translation>Beigetreten: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="86"/>
        <source>Updated: %1</source>
        <translation>Aktualisiert: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="106"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>Biographie von %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="120"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>Dieser Benutzer hat keine Biographie</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="125"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>Keine Biographie für %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="154"/>
        <source>Open Profile in Web Browser</source>
        <translation>Profil im Browser öffnen</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="165"/>
        <source>Send Message</source>
        <translation>Nachricht senden</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="173"/>
        <source>Browse Messages</source>
        <translatorcomment>assuming &quot;messages&quot; and NOT &quot;posts&quot;</translatorcomment>
        <translation>Nachrichten durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="197"/>
        <source>User Options</source>
        <translation>Benutzereinstellungen</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="230"/>
        <source>Follow</source>
        <translation>Folgen</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="242"/>
        <source>Stop Following</source>
        <translation>Nicht mehr folgen</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="306"/>
        <source>Stop following?</source>
        <translation>Nicht mehr folgen?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="307"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Sind Sie sich sicher, dass Sie %1 nicht mehr folgen wollen?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="310"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Ja, nicht mehr folgen</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="311"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="36"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>Tippen Sie ein Stück eines Namens oder einer ID ein, um einen Kontakt zu finden...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="67"/>
        <source>F&amp;ull List</source>
        <translation>&amp;Komplette Liste</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="54"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>benutzer@server.org oder https://server.org/benutzer</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="58"/>
        <source>&amp;Enter address to follow:</source>
        <translatorcomment>Literally, &quot;enter user address&quot;, the follow button already gives enough context to understand what is this about</translatorcomment>
        <translation>&amp;Benutzeraddresse eingeben:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="79"/>
        <source>&amp;Follow</source>
        <translation>&amp;Folgen</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="129"/>
        <source>Reload Followers</source>
        <translation>&apos;Anhängerschaft&apos; aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="134"/>
        <source>Reload Following</source>
        <translation>&apos;Verfolgte&apos; aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="140"/>
        <source>Export Followers</source>
        <translation>&apos;Anhängerschaft&apos; exportieren</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="145"/>
        <source>Export Following</source>
        <translation>&apos;Verfolgte&apos; exportieren</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="151"/>
        <source>Reload Lists</source>
        <translation>Listen auffrischen</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="179"/>
        <source>&amp;Neighbors</source>
        <translation>&amp;Nachbarn</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="208"/>
        <source>Follo&amp;wers</source>
        <translation>&amp;Anhängerschaft</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="213"/>
        <source>Followin&amp;g</source>
        <translation>Verfol&amp;gte</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="218"/>
        <source>&amp;Lists</source>
        <translation>&amp;Listen</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="233"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation>Exportiere Liste der &apos;Verfolgten&apos; in eine Datei</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="234"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>Exportiere Liste der &apos;Anhängerschaft&apos; in eine Datei</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="274"/>
        <source>Cannot export to this file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="278"/>
        <source>Please enter another file name, or choose a different folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="519"/>
        <source>The server seems to be a Pump server, but the account does not exist.</source>
        <translation>Der Server scheint ein Pump-Server zu sein, aber das Konto existiert nicht.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="524"/>
        <source>%1 doesn&apos;t seem to be a Pump server.</source>
        <comment>%1 is a hostname</comment>
        <translation>%1 scheint kein Pump-Server zu sein.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="534"/>
        <source>Following this account at this time will probably not work.</source>
        <translation>Diesem Konto jetzt zu folgen, wird wahrscheinlich nicht funktionieren.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="543"/>
        <source>The %1 server seems unavailable.</source>
        <comment>%1 is a hostname</comment>
        <translation>Der Server %1 scheint unerreichbar.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="551"/>
        <source>Unknown</source>
        <comment>Refers to server version</comment>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="273"/>
        <location filename="../src/contactmanager.cpp" line="556"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="558"/>
        <source>The user address %1 does not exist, or the %2 server is down.</source>
        <translation>Die Benutzeradresse %1 existiert nicht oder der Server %2 läuft nicht.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="566"/>
        <source>Server version</source>
        <translation>Server-Version</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="570"/>
        <source>Check the address, and keep in mind that usernames are case-sensitive.</source>
        <translatorcomment>&quot;consider case-sensitivity&quot;</translatorcomment>
        <translation>Überprüfen Sie die Adresse und beachten Sie beim Benutzernamen die Groß-Klein-Schreibung.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="573"/>
        <source>Do you want to try following this address anyway?</source>
        <translation>Wollen Sie trotzdem versuchen dieser Adresse zu folgen?</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="575"/>
        <source>(not recommended)</source>
        <translation>(nicht empfohlen)</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="578"/>
        <source>Yes, follow anyway</source>
        <translation>Ja, trotzdem folgen</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="579"/>
        <source>No, cancel</source>
        <translation>Nein, abbrechen</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="598"/>
        <source>About to follow %1...</source>
        <translation>Folge gleich %1...</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Open</source>
        <comment>Verb, as in: Open the downloaded file</comment>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="58"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="61"/>
        <source>Save the attached file to your folders</source>
        <translation>Angehängte Datei in einen Ordner speichern</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="74"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="144"/>
        <source>Save File As...</source>
        <translation>Datei speichern unter...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="147"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="198"/>
        <source>File not found!</source>
        <translation>Datei nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="209"/>
        <source>Abort download?</source>
        <translation>Herunterladen abbrechen?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="210"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>Wollen Sie das Herunterladen der angehängten Datei anhalten?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="213"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;Ja, anhalten</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="214"/>
        <source>&amp;No, continue</source>
        <translation>&amp;Nein, fahre fort</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="222"/>
        <source>Download aborted</source>
        <translation>Herunterladen abgebrochen</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="240"/>
        <source>Download completed</source>
        <translation>Herunterladen abgeschlossen</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="244"/>
        <source>Attachment downloaded successfully to %1</source>
        <comment>%1 = filename</comment>
        <translation>Anhang erfolgreich nach %1 heruntergeladen</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="250"/>
        <source>Open the downloaded attachment with your system&apos;s default program for this type of file.</source>
        <translatorcomment>Jan: please re-check; I changed the source text slightly (in your... &gt; with your...)</translatorcomment>
        <translation type="unfinished">Heruntergeladenen Anhang im Standardprogramm ihres Systems für diesen Dateityp öffnen.</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="265"/>
        <source>Download failed</source>
        <translation>Herunterladen fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="272"/>
        <source>Downloading attachment failed: %1</source>
        <comment>%1 = filename</comment>
        <translation>Herunterladen dieses Anhangs fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="305"/>
        <source>Downloading %1 KiB...</source>
        <translation>Lade %1 KiB herunter...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="308"/>
        <source>%1 KiB downloaded</source>
        <translation>%1 KiB heruntergeladen</translation>
    </message>
</context>
<context>
    <name>DraftsManager</name>
    <message>
        <location filename="../src/draftsmanager.cpp" line="28"/>
        <source>Draft Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="44"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="53"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="62"/>
        <source>Manage drafts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="87"/>
        <source>&amp;Delete selected draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="94"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="311"/>
        <source>Untitled draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="328"/>
        <source>Delete draft?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="329"/>
        <source>Are you sure you want to delete this draft?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;Yes, delete it</source>
        <translation type="unfinished">&amp;Ja, löschen</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;No</source>
        <translation type="unfinished">&amp;Nein</translation>
    </message>
</context>
<context>
    <name>EmailChanger</name>
    <message>
        <location filename="../src/emailchanger.cpp" line="28"/>
        <source>Change E-mail Address</source>
        <translation>E-Mail-Adresse wechseln</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="68"/>
        <source>Change</source>
        <translation>Wechseln</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="76"/>
        <source>&amp;Cancel</source>
        <translation>Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="97"/>
        <source>E-mail Address:</source>
        <translation>E-Mail-Adresse:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="98"/>
        <source>Again:</source>
        <translation>Erneut:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="99"/>
        <source>Your Password:</source>
        <translation>Ihr Passwort:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="150"/>
        <source>E-mail addresses don&apos;t match!</source>
        <translation>Die E-Mail-Adressen stimmen nicht überein!</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="159"/>
        <source>Password is empty!</source>
        <translation>Das Passwort ist leer!</translation>
    </message>
</context>
<context>
    <name>FDNotifications</name>
    <message>
        <location filename="../src/notifications.cpp" line="209"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>Filter-Editor</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="44"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1, wenn %2 %3 enthält</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="51"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation>Hier können Sie einige Regeln setzen, um Elemente zu verstecken oder hervorzuheben. Sie können nach Inhalt, Autor oder Anwendung filtern.
Zum Beispiel können Sie Beiträge herausfiltern, welche von der Anwendung &apos;Open Farm Game&apos; stammen, oder welche das Wort &apos;NSFW&apos; enthalten. Sie können auch Beiträge hervorheben, die Ihren Namen enthalten.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Hide</source>
        <translation>Verstecken</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Highlight</source>
        <translation>Hervorheben</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Post Contents</source>
        <translation>Beitragsinhalt</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Author ID</source>
        <translation>Autor-ID</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="84"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="87"/>
        <source>Activity Description</source>
        <translation>Aktivitätsbeschreibung</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="90"/>
        <source>Keywords...</source>
        <translation>Kennwörter...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="95"/>
        <source>&amp;Add Filter</source>
        <translation>Filter &amp;hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="108"/>
        <source>Filters in use</source>
        <translation>Filter in Benutzung</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="114"/>
        <source>&amp;Remove Selected Filter</source>
        <translatorcomment>singular</translatorcomment>
        <translation>Ausgewählten Filter entfe&amp;rnen</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="125"/>
        <source>&amp;Save Filters</source>
        <translation>Filter &amp;speichern</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="132"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="153"/>
        <source>if</source>
        <translation>wenn</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="157"/>
        <source>contains</source>
        <translation>enthält</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="163"/>
        <source>&amp;New Filter</source>
        <translatorcomment>Lit. &quot;Create new filter&quot; in order to avoid having a bare accusative.</translatorcomment>
        <translation>&amp;Neuen Filter erstellen</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="170"/>
        <source>C&amp;urrent Filters</source>
        <translation>&amp;Aktuelle Filter</translation>
    </message>
</context>
<context>
    <name>FilterMatchesWidget</name>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="37"/>
        <source>Content</source>
        <comment>The contents of the post matched</comment>
        <translatorcomment>(Jan: rough approximation from a &quot;Contents&quot; string)</translatorcomment>
        <translation type="unfinished">Inhalt</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="45"/>
        <source>Author</source>
        <translatorcomment>(Jan: rough approximation from another string)</translatorcomment>
        <translation type="unfinished">Autor</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="52"/>
        <source>App</source>
        <comment>Application, short if possible</comment>
        <translatorcomment>(Jan: approximation from another string)</translatorcomment>
        <translation type="unfinished">Anwendung</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="60"/>
        <source>Description</source>
        <translatorcomment>(Jan: rough adaptation from another string)</translatorcomment>
        <translation type="unfinished">Beschreibung</translation>
    </message>
</context>
<context>
    <name>FirstRunWizard</name>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="30"/>
        <source>Welcome Wizard</source>
        <translatorcomment>&quot;Welcome helper&quot;</translatorcomment>
        <translation>Willkommens-Assistent</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="41"/>
        <source>Welcome to Dianara!</source>
        <translation>Willkommen bei Dianara!</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="44"/>
        <source>This wizard will help you get started.</source>
        <translation>Dieser Assistent wir Sie durch die ersten Schritte führen.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="47"/>
        <source>You can access this window again at any time from the Help menu.</source>
        <translation>Sie können dieses Fenster jederzeit über das &apos;Hilfe&apos;-Menü aufrufen.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="50"/>
        <source>The first step is setting up your account, by using the following button:</source>
        <translation>Der Erste Schritt ist Ihre Kontoeinrichtung über die folgende Schaltfläche:</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="61"/>
        <source>Configure your &amp;account</source>
        <translation>&amp;Konto konfigurieren</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="69"/>
        <source>Once you have configured your account, it&apos;s recommended that you edit your profile and add an avatar and some other information, if you haven&apos;t done so already.</source>
        <translation>Sobald Sie Ihr Konto konfiguriert haben, empfiehlt es sich Ihr Profil zu bearbeiten und einen Avatar und einige Angaben hinzuzufügen, falls nicht bereits geschehen.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="80"/>
        <source>&amp;Edit your profile</source>
        <translation>Profil b&amp;earbeiten</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="89"/>
        <source>By default, Dianara will post only to your followers, but it&apos;s recommended that you post to Public, at least sometimes.</source>
        <translation>Standardmäßig wird Dianara Beiträge nur an ihre &apos;Anhängerschaft&apos; senden, aber es empfiehlt sich auch an die Öffentlichkeit zu senden, zumindest hin und wieder.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="97"/>
        <source>Post to &amp;Public by default</source>
        <translation>Standardmäßig an &amp;Öffentlichkeit senden</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="106"/>
        <source>Open general program &amp;help window</source>
        <translation>Fenster mit allgemeiner Programm-&amp;Hilfe öffnen</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="114"/>
        <source>&amp;Show this again next time Dianara starts</source>
        <translation>Dies beim nächsten &amp;Start von Dianara erneut anzeigen</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="120"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change...</source>
        <translation>Wechseln...</translation>
    </message>
    <message>
        <location filename="../src/fontpicker.cpp" line="97"/>
        <source>Choose a font</source>
        <translation>Schriftart wählen</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>Hilfe zu Grundlagen</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation>Loslegen</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="73"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translatorcomment>consider the translations in category &apos;AccountDialog&apos;</translatorcomment>
        <translation>Wenn Sie Dianara zum ersten Mal starten, sollten Sie den Konten-Konfigurationsdialog sehen. Geben Sie dort Ihre Pump.io-Adresse in der Form &apos;Name@Server&apos; ein und drücken Sie die Schaltfläche &apos;Verifizierungkode erhalten&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="78"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translatorcomment>consider the translations in category &apos;AccountDialog&apos;</translatorcomment>
        <translation>Dann sollte Ihr üblicher Browser die Autorisierungsseite Ihres Pump.io-Servers laden. Dort werden Sie den ganzen &apos;VERIFIER&apos;-Code kopieren und diesen dann in das zweite Feld bei Dianara einfügen müssen. Drücken Sie daraufhin &apos;Anwendung autorisieren&apos; und - sobald dies bestätigt ist - &apos;Daten speichern&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>Zu diesem Zeitpunkt werden Ihr Profil, Ihre Kontaklisten und Ihre Zeitleisten geladen werden.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="87"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>Sie sollten einen Blick in das Fenster &apos;Programmeinstellungen&apos; werfen, erreichbar über das Menü &apos;Einstellungen&apos; - &apos;Dianara einrichten&apos;. Dort finden Sie einige interessante Optionen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="110"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>In den Einstellungen können Sie viele Dinge nach Ihren Wünschen anpassen, darunter das Zeitintervall zwischen Zeitleisten-Aktualisierungen, die Anzahl Beiträge, die Sie pro Seite angezeigt bekommen, die Farben für Hervorhebungen, die Benachrichtigungen oder das Aussehen des Symbols im System-Benachrichtigungsfeld.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>Zeitleisten</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="91"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>Vergessen Sie nicht, dass es in Dianara viele Stellen gibt, wo Sie zusätzliche Informationen erhalten können, indem Sie Textbereiche oder Schaltflächen mit Ihrer Maus überfahren und auf das Erscheinen eines Tooltips achten.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="96"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>Falls Sie neu bei Pump.io sind, schauen Sie sich diesen Leitfaden an:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="115"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation>Dort können Sie auch die Option, immer &apos;öffentlich&apos; zu veröffentlichen, aktivieren. Ansonsten können Sie dies immer im Augenblick des Veröffentlichens bestimmen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="131"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>Die Haupt-Zeitleiste, in der Sie all das Zeug sehen werden, das von den Leuten, denen Sie folgen veröffentlicht oder geteilt wird.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="134"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>Die Nachrichten-Zeitleiste, in der Sie Nachrichten sehen werden, die direkt an Sie gerichtet sind. Diese Nachrichten können auch an andere Leute gesendet worden sein.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="138"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>Die &apos;Aktivität&apos;-Zeitleiste, in der Sie Ihre eigenen oder von Ihnen geteilte Beiträge sehen werden.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="141"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>Die Favoriten-Zeitleiste, in der Sie die Beiträge und Kommentare sehen werden, die Sie favorisiert haben (indem Sie &quot;Gefällt mir&quot; gedrückt haben). Dieses kann als Lesezeichensystem verwendet werden.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="158"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>Diese Aktivitäten können eine &apos;+&apos;-Schaltfläche enthalten. Drücken Sie darauf, um den erwähnten Beitrag zu öffnen. Außerdem können Sie, wie an vielen anderen Stellen, durch Überfahren mit der Maus relevante Informationen sehen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>Veröffentlichen</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>Neue Beiträge erscheinen andersfarbig hervorgehoben. Sie können sie einfach als &apos;gelesen&apos; markieren, indem Sie auf irgendeine freie Stelle innerhalb des Beitrags klicken. </translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="176"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation>Sie können Mitteilungen veröffentlichen, indem Sie in das Eingabefeld im oberen Fensterbereich klicken oder Strg+N drücken. Ihrem Beitrag einen Titel zu geben ist optional, jedoch sehr empfehlenswert, da es das Wiedererkennen von Verweisen auf Ihren Beitrag im Nebenfeed, in E-Mail-Benachrichtigungen usw., vereinfacht.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>Es ist möglich Bilder, Audio- und Videodateien sowie allgemeine Dateien (wie PDF-Dokumente) an Ihren Beitrag anzuhängen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="192"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>Mittels der &apos;Format&apos; Schaltfläche können Sie Ihrem Text Formatierungen wie Fett- oder Kursivdruck verleihen. Manche dieser Optionen setzen eine Textauswahl voraus.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="199"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>Wenn Sie der &apos;An&apos;-Liste eine bestimmte Person beifügen, wird diese Ihren Beitrag über ihr Direktnachrichten-Tab empfangen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="210"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>Sie können private Nachrichten erstellen, indem Sie bestimmte Personen zu diesen Listen hinzufügen und die Optionen &apos;Anhängerschaft&apos; und &apos;öffentlich&apos; abwählen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>Kontakte verwalten</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="223"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>Sie können die Listen der Leute, denen Sie folgen, und derjenigen, die Ihnen folgen, im Kontakte-Tab anzeigen lassen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="226"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>Dort können Sie auch Personenlisten verwalten, die in erster Linie dazu verwendet werden, Beiträge an bestimmte Personenkreise zu senden.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="237"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>Sie können auf alle Avatare in Beiträgen, Kommentaren und der &apos;Inzwischen&apos;-Spalte klicken, woraufhin ein Menü mit mehreren Optionen erscheint; eine davon das Folgen bzw. Nicht-mehr-folgen der betreffenden Person.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>Tastaturbefehle</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="98"/>
        <source>Pump.io User Guide</source>
        <translation>Pump.io Leitfaden</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="128"/>
        <source>There are seven timelines:</source>
        <translation>Es gibt sieben Zeitleisten:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="146"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <comment>LEFT SIDE should change to RIGHT SIDE on RTL languages</comment>
        <translation>Die fünfte Zeitleiste ist der Nebenfeed, auch als &apos;Inzwischen&apos; bezeichnet. Dieser ist auf der linken Seite zu sehen, doch kann er auch versteckt werden. Dort werden Sie beiläufige Aktivitäten von allen, denen Sie folgen, sehen, darunter Kommentar-Aktionen, Favorisieren von Beiträgen oder Leuten zu folgen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>Die sechste und siebte Zeitleiste sind ebenfalls Nebenfeeds, ähnlich wie &apos;Inzwischen&apos;, enthalten aber nur Aktivitäten, die direkt Sie betreffen (Erwähnungen), und Aktionen durch Sie (Aktionen).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="196"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>Indem Sie die &apos;An&apos;- und die &apos;CC&apos;-Schaltfläche verwenden, bestimmen Sie wer Ihren Beitrag sehen wird.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="202"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>Sie können auch &apos;@&apos; gefolgt vom ersten Zeichen eines Kontaktnamens eingeben, um ein Pop-Up-Menü mit passenden Möglichkeiten anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="229"/>
        <source>There is a text field at the top, where you can directly enter addresses of new contacts to follow them.</source>
        <translation>Es gibt oben ein Textfeld, in das Sie direkt die Adressen von Kontakten eingeben können, um ihnen zu folgen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="242"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>Sie können diesem Kontakt aus dem selben Menü auch eine (anfangs private) Direktnachricht senden.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="245"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>Sie finden eine Liste mit einigen Pump.io-Nutzern und anderen Informationen hier:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="249"/>
        <source>Users by language</source>
        <translation>Benutzer nach Sprache</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="251"/>
        <source>Followers of Pump.io Community account</source>
        <translation>&apos;Anhängerschaft&apos; des Community-Kontos von Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="263"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>Die geläufigsten Befehle in den Menüs sind durch  Tastaturkürzel, wie &apos;F5&apos; oder &apos;Strg+N&apos;, ergänzt.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="267"/>
        <source>Besides that, you can use:</source>
        <translation>Außerdem können Sie Folgendes benutzen:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="270"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation>Strg+Hoch/Runter/BildAuf/BildAb/Pos1/Ende, um sich über die Zeitleiste zu bewegen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="273"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>Strg+Links/Rechts, um eine Seite in der Zeitleiste zu springen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="276"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>Str+G, um direkt zu einer bestimmten Seite der Zeitleiste zu springen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="279"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>Strg+1/2/3, um zwischen den Nebenfeeds zu wechseln.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="282"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>Strg+Eingabe, um zu veröffentlichen, wenn Sie das Erstellen einer Mitteilung oder eines Kommentars abgeschlossen haben. Falls die Mitteilung leer ist, können Sie diese mit der Taste ESC verwerfen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="316"/>
        <source>Dianara offers a D-Bus interface that allows some control from other applications.</source>
        <translation>Dianara bietet eine D-Bus-Schnittstelle, die teilweise Kontrolle von anderen Anwendungen aus ermöglicht.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="319"/>
        <source>The interface is at %1, and you can access it with tools such as %2 or %3. It offers methods like %4 and %5.</source>
        <translation>Das Interface befindet sich bei %1, und Sie können darauf mit Werkzeugen wie %2 oder %3 zugreifen. Es bietet Methoden wie %4 und %5.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Kommandozeilenoptionen</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="206"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>Wählen Sie mit den Pfeiltasten aus und drücken Sie die Eingabe-Taste um den Namen zu vervollständigen. Das wird jene Person zur Empfängerliste hinzufügen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="233"/>
        <source>Under the &apos;Neighbors&apos; tab you&apos;ll see some resources to find people, and have the option to browse the latest registered users from your server directly.</source>
        <translation>Im &apos;Nachbarn&apos;-Tab sehen Sie einige Ressourcen, um Leute zu finden, und haben die Möglichkeit die zuletz registrierten Benutzer ihres Servers direkt zu durchstöbern.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="286"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>Während des Erstellens einer Mitteilung können Sie mithilfe der Eingabetaste vom Titel- ins Text-Feld springen. Umgekehrt können Sie mithilfe der Pfeiltaste aufwärts vom Text- zurück ins Titel-Feld springen, wenn sich der Cursor ganz am Anfang des Feldes befindet.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="291"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>Strg+Eingabe um die Empfängerliste im &apos;An&apos; oder &apos;CC&apos;-Feld für einen Beitrag abzuschließen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="304"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>Sie können den Parameter &apos;--config&apos; verwenden um das Programm mit einer anderen Konfiguration auszuführen. Das kann nützlich sein, um zwei oder mehr Konten zu verwenden. Sie können sogar zwei Instanzen Dianaras gleichzeitig ausführen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="309"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Verwenden Sie den Parameter &apos;--debug&apos;, um zusätzliche Informationen über das Verhalten des Programms in Ihrem Terminal anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="312"/>
        <source>If your server does not support HTTPS, you can use the --nohttps parameter.</source>
        <translation>Falls Ihr Server HTTPS nicht unterstützt, können Sie den Parameter &apos;--nohttps&apos; verwenden.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="326"/>
        <source>If you use an alternate configuration, with something like &apos;--config otherconf&apos;, then the interface will be at org.nongnu.dianara_otherconf.</source>
        <translation>Falls Sie eine alternative Konfiguration verwenden, beispielsweise mit &apos;--config otherconf&apos;, befindet sich die Schnittstelle unter &apos;org.nongnu.dianara_otherconf&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="343"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="33"/>
        <source>Image</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="31"/>
        <source>Untitled</source>
        <translation>Unbenannt</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="67"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Speichern unter...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="80"/>
        <source>&amp;Restart</source>
        <comment>Restart animation</comment>
        <translatorcomment>&quot;Repeat&quot;</translatorcomment>
        <translation>Wiede&amp;rholen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="90"/>
        <source>Fit</source>
        <comment>As in: fit image to window</comment>
        <translation>Einpassen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="137"/>
        <source>Rotate image to the left</source>
        <comment>RTL: This actually means LEFT, anticlockwise</comment>
        <translation type="unfinished">Bild linksherum drehen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="147"/>
        <source>Rotate image to the right</source>
        <comment>RTL: This actually means RIGHT, clockwise</comment>
        <translation type="unfinished">Bild rechtsherum drehen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="160"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="296"/>
        <source>Save Image...</source>
        <translation>Bild speichern...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="336"/>
        <source>Close Viewer</source>
        <translation>Betrachter schließen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="377"/>
        <source>Downloading full image...</source>
        <translation>Vollbild herunterladen...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="523"/>
        <source>Error downloading image!</source>
        <translation>Fehler beim Herunterladen des Bildes!</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="525"/>
        <source>Try again later.</source>
        <translation>Versuchen Sie es später wieder.</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="536"/>
        <source>Save Image As...</source>
        <translation>Bild speichern unter...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="539"/>
        <source>Image files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="540"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="551"/>
        <source>Error saving image</source>
        <translation>Fehler beim speichern des Bildes</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="552"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>Ein Fehler ist aufgetreten beim Speichern von %1.

Der Dateiname sollte mit den Erweiterungen &apos;.jpg&apos; oder &apos;.png&apos; enden.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="39"/>
        <source>Members</source>
        <translation>Mitglieder</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="52"/>
        <source>Add Mem&amp;ber</source>
        <translation>Mitglied &amp;hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="60"/>
        <source>&amp;Remove Member</source>
        <translation>Mitglied &amp;löschen</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="69"/>
        <source>&amp;Delete Selected List</source>
        <translation>Ausgewählte Li&amp;ste löschen</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="77"/>
        <source>Add New &amp;List</source>
        <translation>&amp;Neue Liste hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="96"/>
        <source>Create L&amp;ist</source>
        <translation>Liste &amp;erstellen</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="104"/>
        <source>&amp;Add to List</source>
        <translation>Zur Liste hin&amp;zufügen</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="316"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translatorcomment>Lit. &quot;Do you want to delete %1?&quot;</translatorcomment>
        <translation>Sind Sie sich sicher, dass Sie %1 löschen wollen?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="451"/>
        <source>Remove person from list?</source>
        <translation>Person aus der Liste löschen?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="452"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>Sind Sie sich sicher, dass Sie %1 aus der Liste %2 löschen wollen?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="80"/>
        <source>Type a name for the new list...</source>
        <translation>Geben Sie einen Namen für die neue Liste ein...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="89"/>
        <source>Type an optional description here</source>
        <translation>Geben Sie eine optionale Beschreibung hier ein</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="315"/>
        <source>WARNING: Delete list?</source>
        <translation>WARNUNG: Liste löschen?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Ja, löschen</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>Log-Datei</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>Log-Datei &amp;leeren</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="61"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="1099"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;Statusleiste</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2645"/>
        <source>Total posts: %1</source>
        <translation>Beiträge gesamt: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2653"/>
        <source>&amp;Timeline</source>
        <translation>Zeitleis&amp;te</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2655"/>
        <source>The main timeline</source>
        <translation>Die Haupt-Zeitleiste</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2671"/>
        <source>&amp;Activity</source>
        <translation>&amp;Aktivität</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2673"/>
        <source>Your own posts</source>
        <translation>Ihre eigenen Beiträge</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2682"/>
        <source>Your favorited posts</source>
        <translation>Ihre favorisierten Beiträge</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2662"/>
        <source>&amp;Messages</source>
        <translation>&amp;Nachrichten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2664"/>
        <source>Messages sent explicitly to you</source>
        <translatorcomment>Lit. &quot;Messages for you&quot;, a literal translation would be too long</translatorcomment>
        <translation>Nachrichten direkt an Sie</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>&amp;Contacts</source>
        <translation>&amp;Kontakte</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="738"/>
        <location filename="../src/mainwindow.cpp" line="1387"/>
        <source>Initializing...</source>
        <translation>Starten...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="788"/>
        <source>Your account is not configured yet.</source>
        <translation>Ihr Konto ist noch nicht konfiguriert.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="163"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translatorcomment>*** Note by Jan: This shouldn&apos;t need a line break, since the tooltip displaying this message will use word wrapping for any language. But it might be a matter of style, I don&apos;t know ;)</translatorcomment>
        <translation>Neben-Aktivitäten von allen, z.B. Antworten auf Beiträge</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Minor activities addressed to you</source>
        <translation>An Sie gerichtete Neben-Aktivitäten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="198"/>
        <source>Minor activities done by you</source>
        <translation>Neben-Aktivitäten von Ihnen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="429"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>Die Leute denen Sie folgen, diejenigen, die Ihnen folgen, und Ihre Kontaklisten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>&amp;Session</source>
        <translation>&amp;Sitzung</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>Zei&amp;tleisten automatisch aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1039"/>
        <source>Mark All as Read</source>
        <translation>Alles als &apos;gelesen&apos; markieren</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;Mitteilung veröffentlichen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>&amp;Quit</source>
        <translation>&amp;Verlassen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1072"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Locked Panels and Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1075"/>
        <source>Side Panel</source>
        <translation type="unfinished">Seitenleiste</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>&amp;Toolbar</source>
        <translation>Werkzeugleis&amp;te</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1112"/>
        <source>Full &amp;Screen</source>
        <translation>Voll&amp;bild</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>&amp;Log</source>
        <translation>&amp;Log-Datei</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>S&amp;ettings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Edit &amp;Profile</source>
        <translation>&amp;Profil bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>&amp;Account</source>
        <translation>&amp;Konto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Basic &amp;Help</source>
        <translation>&amp;Hilfe zu Grundlagen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <source>Report a &amp;Bug</source>
        <translation>Einen &amp;Fehler melden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1241"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>Pump.io &amp;Leitfaden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1249"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>Einige &amp;Tipps zu Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1257"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>Liste einiger P&amp;ump.io-Benutzer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1265"/>
        <source>Pump.io &amp;Network Status Website</source>
        <translation>Pump.io-&amp;Netzwerkstatus-Webseite</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>Toolbar</source>
        <translation>Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1371"/>
        <source>Open the log viewer</source>
        <translation>Log-Betrachter öffnen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1394"/>
        <source>Auto-updating enabled</source>
        <translation>Automatisches Aktualisieren aktiv</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1401"/>
        <source>Auto-updating disabled</source>
        <translation>Automatisches Aktualisieren inaktiv</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1953"/>
        <source>Proxy password required</source>
        <translation>Proxy-Passwort benötigt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1954"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>Sie haben einen Proxy-Server mit Authentifizierung konfiguriert, aber das Passwort ist nicht eingestellt.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1959"/>
        <source>Enter the password for your proxy server:</source>
        <translation>Geben Sie das Passwort für Ihren Proxy-Server ein:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2108"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>Beginne automatische Aktualisierung der Zeitleisten alle %1 Minuten.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2115"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>Beende automatische Aktualisierung der Zeitleisten.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2376"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation>%1 ältere Beiträge in &apos;%2&apos; empfangen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2416"/>
        <source>1 highlighted</source>
        <comment>singular, refers to a post</comment>
        <translation>1 hervorgehoben</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2421"/>
        <source>%1 highlighted</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 hervorgehobene</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2411"/>
        <source>Direct messages</source>
        <translation>Direktnachrichten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2412"/>
        <source>By filters</source>
        <translation>Nach Filtern</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2453"/>
        <source>1 more pending to receive.</source>
        <comment>singular, one post</comment>
        <translation>Noch 1 zu empfangen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2458"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation>Noch %1 zu empfangen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2470"/>
        <location filename="../src/mainwindow.cpp" line="2886"/>
        <source>Also:</source>
        <translation>Außerdem:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2592"/>
        <source>Last update: %1</source>
        <translation>Zuletzt aktualisiert: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2385"/>
        <location filename="../src/mainwindow.cpp" line="2822"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translatorcomment>(Jan) tried my luck</translatorcomment>
        <translation>&apos;%1&apos; aktualisiert.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="764"/>
        <source>%1 started.</source>
        <comment>1=program name and version</comment>
        <translation type="unfinished">%1 gestartet.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="767"/>
        <source>Running with Qt v%1 on %2.</source>
        <comment>1=Qt version, 2=OS name</comment>
        <translatorcomment>FIXME</translatorcomment>
        <translation type="unfinished">Läuft mit Qt v%1, %2.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2477"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 herausgefiltert.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2482"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 herausgefiltert.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2495"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 gelöscht.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2500"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 gelöscht.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2834"/>
        <source>There is 1 new activity.</source>
        <translation>Es gibt 1 neue Aktivität.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2838"/>
        <source>There are %1 new activities.</source>
        <translation>Es gibt %1 neue Aktivitäten.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2851"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translatorcomment>refers to &quot;Aktivität&quot;</translatorcomment>
        <translation>1 hervorgehobene.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2856"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translatorcomment>refers to &quot;Aktivitäten&quot;</translatorcomment>
        <translation>%1 hervorgehobene.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2890"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to one activity</comment>
        <translation>1 herausgefiltert.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>%1 filtered out.</source>
        <comment>plural, several activities</comment>
        <translation>%1 herausgefiltert.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2964"/>
        <source>No new activities.</source>
        <translation>Keine neuen Aktivitäten.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3040"/>
        <source>Error storing image!</source>
        <translation>Fehler beim Speichern des Bildes!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3042"/>
        <source>%1 bytes</source>
        <translation>%1 Bytes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3105"/>
        <source>Marking everything as read...</source>
        <translation>Markiere alles als gelesen...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3453"/>
        <source>Dianara is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation>Dianara ist freie Software, lizenziert unter der GNU-GPL-Lizenz, und verwendet einige Oxygen-Icons unter der LGPL-Lizenz.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3549"/>
        <source>Closing due to environment shutting down...</source>
        <translation>Schließe wegen Abschaltens der Umgebung...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3572"/>
        <location filename="../src/mainwindow.cpp" line="3677"/>
        <source>Quit?</source>
        <translation>Verlassen?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3573"/>
        <source>You are composing a note or a comment.</source>
        <translation>Sie schreiben gerade eine Mitteilung oder einen Kommentar.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3575"/>
        <source>Do you really want to close Dianara?</source>
        <translation>Wollen Sie Dianara wirklich schließen?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3576"/>
        <location filename="../src/mainwindow.cpp" line="3685"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;Ja, Programm schließen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3576"/>
        <location filename="../src/mainwindow.cpp" line="3685"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3599"/>
        <source>Shutting down Dianara...</source>
        <translation>Schalte Dianara ab...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3678"/>
        <source>System tray icon is not available.</source>
        <translation>Icon fürs System-Benachrichtigungsfeld ist nicht verfügbar.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3680"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>Dianara kann nicht im System-Benachrichtigungsfeld versteckt werden.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3683"/>
        <source>Do you want to close the program completely?</source>
        <translation>Wollen Sie das Programm ganz schließen?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2391"/>
        <source>Timeline updated at %1.</source>
        <translation>Zeitleiste aktualisiert um %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="723"/>
        <source>Press F1 for help</source>
        <translation>Drücken Sie für Hilfe F1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="794"/>
        <source>Click here to configure your account</source>
        <translation>Klicken Sie hier, um Ihr Konto zu konfigurieren</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="934"/>
        <source>Update %1</source>
        <translatorcomment>Unclear: What is %1 referring to?</translatorcomment>
        <translation>%1 aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Show Welcome Wizard</source>
        <translation>Zeige Willkommens-Assistent</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2737"/>
        <source>Your Pump.io account is not configured</source>
        <translation>Ihr Pump.io-Konto ist nicht konfiguriert</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2814"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation>%1 ältere Aktivitäten in &apos;%2&apos; empfangen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2869"/>
        <source>1 more pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation>Noch 1 zu empfangen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2874"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation>Noch %1 zu empfangen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3086"/>
        <source>Link to: %1</source>
        <translation>Link zu: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3436"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>Mit Dianara können Sie Ihre Zeitleisten betrachten, neue Mitteilungen schreiben, Bilder und andere Medien hochladen, mit Beiträgen interagieren, Ihre Kontakte verwalten und neuen Leuten folgen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3442"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Deutsche Übersetzung durch Eugenio M. Vigo und mightyscoopa.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>&amp;Configure Dianara</source>
        <translation>Dianara &amp;einrichten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>&amp;Filters and Highlighting</source>
        <translation>&amp;Filter und Hervorheben</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>Visit &amp;Website</source>
        <translation>&amp;Webseite besuchen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1276"/>
        <source>About &amp;Dianara</source>
        <translation>Über &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2048"/>
        <source>Your biography is empty</source>
        <translation>Ihre Biographie ist leer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2083"/>
        <source>Click to edit your profile</source>
        <translation>Klicken Sie um Ihr Profil zu bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2561"/>
        <source>No new posts.</source>
        <translation>Keine neuen Beiträge.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2680"/>
        <source>Favor&amp;ites</source>
        <translation>Favor&amp;iten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2827"/>
        <source>Minor feed updated at %1.</source>
        <translation>Nebenfeed aktualisiert um %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3490"/>
        <source>&amp;Hide Window</source>
        <translation>Fenster &amp;verstecken</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3501"/>
        <source>&amp;Show Window</source>
        <translation>Fen&amp;ster zeigen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2397"/>
        <source>There is 1 new post.</source>
        <translation>Es gibt 1 neuen Beitrag.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2401"/>
        <source>There are %1 new posts.</source>
        <translation>Es gibt %1 neue Beiträge.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>About Dianara</source>
        <translation>Über Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3432"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>Dianara ist ein Client für das soziale Netzwerk Pump.io.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3449"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>Gedankt sei allen Testern, Übersetzern und Paketerstellern, die dabei helfen, Dianara zu verbessern!</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="70"/>
        <source>Older Activities</source>
        <translation>Ältere Aktivitäten</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="74"/>
        <source>Get previous minor activities</source>
        <translation>Vorherige Nebenaktivitäten abrufen</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="102"/>
        <source>There are no activities to show yet.</source>
        <translation>Es gibt noch keine Aktivitäten zu zeigen.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="452"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation>Empfange %1 neuere</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="69"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>Mit %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="83"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>An: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="90"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>CC: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="266"/>
        <source>Open referenced post</source>
        <translation>Erwähnten Beitrag öffnen</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="315"/>
        <source>bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="671"/>
        <source>Error: Unable to launch browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="672"/>
        <source>The default system web browser could not be executed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="675"/>
        <source>You might need to install the XDG utilities.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>Zu Seite springen</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="35"/>
        <source>Page number:</source>
        <translation>Seitenzahl:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="50"/>
        <source>&amp;First</source>
        <comment>As in: first page</comment>
        <translation>&amp;Erste</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="57"/>
        <source>&amp;Last</source>
        <comment>As in: last page</comment>
        <translation>&amp;Letzte</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="64"/>
        <source>Newer</source>
        <comment>As in: newer pages</comment>
        <translation>Neuere</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="85"/>
        <source>Older</source>
        <comment>As in: older pages</comment>
        <translation>Ältere</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="96"/>
        <source>&amp;Go</source>
        <translation>&amp;Los</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>&amp;Suche:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>Geben Sie hier einen Namen ein, um danach zu suchen</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="102"/>
        <source>Add a contact to a list</source>
        <translation>Einen Kontakt zu einer Liste hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="114"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="1802"/>
        <source>Like</source>
        <translation>Gefällt mir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1801"/>
        <source>Like this post</source>
        <translation>Diese Nachricht favorisieren</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1407"/>
        <source>1 like</source>
        <translation>1x favorisiert</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1476"/>
        <source>1 comment</source>
        <translation>1 Kommentar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1576"/>
        <source>Shared %1 times</source>
        <translation>%1x geteilt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="259"/>
        <source>Shared on %1</source>
        <translation>Geteilt am %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1709"/>
        <source>Edited: %1</source>
        <translation>Bearbeitet: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1000"/>
        <source>In</source>
        <translation>In</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="263"/>
        <location filename="../src/post.cpp" line="952"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>Mit %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="652"/>
        <source>Share</source>
        <translation>Teilen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="678"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1155"/>
        <source>Loading image...</source>
        <translation>Lade Bild...</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1412"/>
        <source>%1 likes</source>
        <translation>%1x favorisiert</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1481"/>
        <source>%1 comments</source>
        <translation>%1 Kommentare</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="690"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="140"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>Beitrag</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="205"/>
        <source>Via %1</source>
        <translation>Via %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="942"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>Am %1 veröffentlicht</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="215"/>
        <location filename="../src/post.cpp" line="436"/>
        <source>To</source>
        <translation>An</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="522"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>Vorläufer</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="531"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>Öffne Vorläuferbeitrag, auf den dieser antwortet</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="632"/>
        <source>If you select some text, it will be quoted.</source>
        <translatorcomment>Lit. &quot;Selected text will be quoted&quot;</translatorcomment>
        <translation>Ausgewählter Text wird zitiert.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="660"/>
        <source>Unshare</source>
        <translation>Nicht mehr teilen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="662"/>
        <source>Unshare this post</source>
        <translation>Diesen Beitrag nicht mehr teilen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="313"/>
        <source>Open post in web browser</source>
        <translation>Diesen Beitrag im Browser öffnen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>Klicken, um den Anhang herunterzuladen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="221"/>
        <location filename="../src/post.cpp" line="453"/>
        <source>Cc</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="322"/>
        <source>Copy post link to clipboard</source>
        <translation>Kopiere den Beitragslink in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="341"/>
        <source>Normalize text colors</source>
        <translation>Normalisiere Schriftfarben</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="359"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="944"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="959"/>
        <source>Modified on %1</source>
        <translation>Am %1 modifiziert</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="627"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>Kommentieren</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>Reply to this post.</source>
        <translation>Auf diesen Beitrag antworten.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="654"/>
        <source>Share this post with your contacts</source>
        <translation>Diesen Beitrag mit Ihren Kontakten teilen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="681"/>
        <source>Modify this post</source>
        <translation>Diesen Beitrag bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="693"/>
        <source>Erase this post</source>
        <translation>Diesen Beitrag löschen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="767"/>
        <source>Join Group</source>
        <translation>Gruppe beitreten</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="773"/>
        <source>%1 members in the group</source>
        <translation>%1 Mitglieder in der Gruppe</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1123"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>Bild ist animiert. Darauf klicken um es abzuspielen.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1133"/>
        <source>Size</source>
        <comment>Image size (resolution)</comment>
        <translation>Bildgröße</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1150"/>
        <source>Couldn&apos;t load image!</source>
        <translation>Konnte Bild nicht laden!</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1173"/>
        <source>Attached Audio</source>
        <translation>Angehängte Audiodatei</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1183"/>
        <source>Attached Video</source>
        <translation>Angehängte Videodatei</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1217"/>
        <source>Attached File</source>
        <translation>Angehängte Datei</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1381"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>%1 gefällt dies</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1386"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translatorcomment>German uses: it +like+dative, so the verb remains in the singular (similar to Spanish)</translatorcomment>
        <translation>%1 gefällt dies</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1537"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 hat dies geteilt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1542"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 haben dies geteilt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1571"/>
        <source>Shared once</source>
        <translation>1x geteilt</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1795"/>
        <source>You like this</source>
        <translation>Du hast dies favorisiert</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1796"/>
        <source>Unlike</source>
        <translation>Gefällt mir nicht</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1957"/>
        <source>Are you sure you want to share your own post?</source>
        <translation>Sind Sie sicher, dass Sie Ihren eigenen Beitrag teilen möchten?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1962"/>
        <source>Share post?</source>
        <translation>Beitrag teilen?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1952"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>Wollen Sie %1s Beitrag teilen?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1965"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;Ja, teilen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1966"/>
        <location filename="../src/post.cpp" line="1986"/>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1982"/>
        <source>Unshare post?</source>
        <translation>Beitrag nicht mehr teilen?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1983"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>Wollen Sie das Teilen von %1s Beitrag rückgängig machen?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1985"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;Ja, nicht mehr teilen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2027"/>
        <source>WARNING: Delete post?</source>
        <translation>WARNUNG: Beitrag löschen?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2028"/>
        <source>Are you sure you want to delete this post?</source>
        <translatorcomment>Lit.: &quot;Are you sure that you want to delete this post?&quot;</translatorcomment>
        <translation>Sind Sie sicher, dass Sie diesen Beitrag löschen wollen?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Ja, löschen</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>Klicken Sie auf das Bild, um es in voller Größe zu sehen</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>Profil-Editor</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="51"/>
        <source>This is your Pump address</source>
        <translation>Dies ist Ihre Pump.io-Adresse</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="53"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>Dies ist die mit Ihrem Konto verknüpfte E-Mail-Adresse, um Benachrichtigungen zu erhalten und für die Passwortwiederherstellung</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="70"/>
        <source>Change &amp;E-mail...</source>
        <translation>Wechsle &amp;E-Mail-Adresse...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="82"/>
        <source>Change &amp;Avatar...</source>
        <translation>&amp;Avatar ändern...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="92"/>
        <source>This is your visible name</source>
        <translation>Dies ist Ihr sichtbarer Name</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>Changing your avatar will create a post in your timeline with it.
If you delete that post your avatar will be deleted too.</source>
        <translation>Wenn Sie Ihren Avatar wechseln, wird ein Beitrag in Ihrer Zeitleiste daraus erstellt.
Falls Sie jenen Beitrag löschen, wird auch Ihr Avatar gelöscht.</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="126"/>
        <source>&amp;Save Profile</source>
        <translation>Profil &amp;speichern</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="133"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="166"/>
        <source>Webfinger ID</source>
        <translation>Webfinger-ID</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="167"/>
        <source>E-mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="169"/>
        <source>Avatar</source>
        <translation>Avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="171"/>
        <source>Full &amp;Name</source>
        <translation>Ganzer &amp;Name</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="172"/>
        <source>&amp;Hometown</source>
        <translation>&amp;Heimatort</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="173"/>
        <source>&amp;Bio</source>
        <translation>&amp;Biographie</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="211"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>Nicht eingestellt</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="284"/>
        <source>Select avatar image</source>
        <translation>Bild für Avatar auswählen</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="286"/>
        <source>Image files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="289"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="312"/>
        <source>Invalid image</source>
        <translation>Ungültige Bilddatei</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="313"/>
        <source>The selected image is not valid.</source>
        <translation>Die ausgewählte Bilddatei ist nicht gültig.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>Proxy-Konfiguration</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>Keinen Proxy verwenden</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>Ihr Proxy-Benutzername</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>Hinweis: Das Passwort wird nicht sicher hinterlegt. Wenn Sie wollen, können Sie das Feld leer lassen, dann werden Sie beim Start nach dem Passwort gefragt.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="75"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="82"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="98"/>
        <source>Proxy &amp;Type</source>
        <translation>Proxy-&amp;Typ</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="100"/>
        <source>&amp;Hostname</source>
        <translation>&amp;Host-Name</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="102"/>
        <source>&amp;Port</source>
        <translation>&amp;Port</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="104"/>
        <source>Use &amp;Authentication</source>
        <translation>&amp;Authentifizierung verwenden</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="106"/>
        <source>&amp;User</source>
        <translation>Ben&amp;utzer</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="108"/>
        <source>Pass&amp;word</source>
        <translation>Pass&amp;wort</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="75"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1038"/>
        <source>Select Picture...</source>
        <translation>Bild auswählen...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1040"/>
        <source>Find the picture in your folders</source>
        <translation>Bild in Ihren Ordnern finden</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="175"/>
        <source>Drafts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="185"/>
        <source>To...</source>
        <translation>An...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="199"/>
        <source>Select who will get a copy of this post</source>
        <translation>Auswählen, wer eine Kopie dieses Beitrags erhalten soll</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="250"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>Sonstiges</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="303"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="306"/>
        <source>Cancel the post</source>
        <translatorcomment>rectified &quot;Beitrags&quot; to &quot;Beitrag&quot;</translatorcomment>
        <translation>Beitrag verwerfen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1033"/>
        <source>Picture not set</source>
        <translatorcomment>&quot;Picture not chosen&quot;</translatorcomment>
        <translation>Bild nicht gewählt</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1063"/>
        <source>Select Audio File...</source>
        <translation>Audiodatei auswählen...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1065"/>
        <source>Find the audio file in your folders</source>
        <translation>Audiodatei in Ihren Ordnern finden</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1058"/>
        <source>Audio file not set</source>
        <translation>Audiodatei nicht gewählt</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1088"/>
        <source>Select Video...</source>
        <translation>Video auswählen...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1090"/>
        <source>Find the video in your folders</source>
        <translation>Video in Ihren Ordnern finden</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1083"/>
        <source>Video not set</source>
        <translation>Video nicht gewählt</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1112"/>
        <source>Select File...</source>
        <translation>Datei auswählen...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1114"/>
        <source>Find the file in your folders</source>
        <translation>Datei in Ihren Ordnern finden</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1107"/>
        <source>File not set</source>
        <translation>Datei nicht gewählt</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1183"/>
        <location filename="../src/publisher.cpp" line="1227"/>
        <source>Error: Already composing</source>
        <translation>Fehler: Bereits am Erstellen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1184"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>Sie können jetzt keinen Beitrag bearbeiten, weil bereits ein anderer Beitrag erstellt wird.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1202"/>
        <source>Update</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1228"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>Sie können jetzt keine Nachricht an %1 erstellen, weil bereits ein anderer Beitrag erstellt wird.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1355"/>
        <source>Draft loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1360"/>
        <source>ERROR: Already composing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1361"/>
        <source>You can&apos;t load a draft at this time, because a post is already being composed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1380"/>
        <source>Draft saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1417"/>
        <source>Posting failed.

Try again.</source>
        <translation>Veröffentlichung fehlgeschlagen.

Erneut versuchen.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1561"/>
        <source>Warning: You have no followers yet</source>
        <translation>Warnung: Sie haben noch keine Anhängerschaft</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1562"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>Sie versuchen nur an Ihre Anhängerschaft zu veröffentlichen, aber Sie haben noch keine.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1566"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>Wenn Sie so veröffentlichen, wird niemand Ihren Beitrag sehen können.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1574"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;Abbrechen, zurück zum Beitrag</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1627"/>
        <source>Updating...</source>
        <translation>Aktualisiere...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1638"/>
        <source>Post is empty.</source>
        <translation>Der Beitrag ist leer.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1642"/>
        <source>File not selected.</source>
        <translation>Datei nicht ausgewählt.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="590"/>
        <source>Select one image</source>
        <translation>Wählen Sie ein Bild aus</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="591"/>
        <source>Image files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="619"/>
        <source>Select one file</source>
        <translation>Wählen Sie eine Datei aus</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="622"/>
        <source>Invalid file</source>
        <translation>Ungültige Datei</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="623"/>
        <source>The file type cannot be detected.</source>
        <translatorcomment>&quot;(...) is not recognized.&quot;</translatorcomment>
        <translation>Der Dateityp wird nicht erkannt.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="635"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="652"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>Da Sie ein Bild hochladen wollen, könnten Sie es ein wenig verkleinern oder es in einem komprimierten Format wie JPG speichern.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="657"/>
        <source>File is too big</source>
        <translation>Die Datei ist zu groß</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="658"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>Dianara begrenzt das Hochladen von Dateien z.Zt. auf 10 MiB pro Beitrag, um mögliche Speicherplatz- oder Netzwerkprobleme der Server zu vermeiden.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="663"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>Dies ist eine vorläufige Maßnahme, da die Server noch nicht ihre eigenen Begrenzungen aufstellen können.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="668"/>
        <source>Sorry for the inconvenience.</source>
        <translation>Entschuldigen Sie die Unannehmlichkeiten.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="837"/>
        <source>File not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="840"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="822"/>
        <source>The selected file cannot be accessed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="829"/>
        <source>It is owned by %1.</source>
        <comment>%1 = a username</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="832"/>
        <source>You might not have the necessary permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="697"/>
        <source>Resolution</source>
        <comment>Image resolution (size)</comment>
        <translation>Bildgröße</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="720"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="722"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1670"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 KiB von %2 KiB hochgeladen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="594"/>
        <source>Invalid image</source>
        <translation>Ungültige Bilddatei</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>Einen Titel einzustellen hilft dabei die &apos;Inzwischen&apos;-Zeitleiste informativer zu machen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="108"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="111"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>Den Anhang verwerfen und zurück zu einer einfachen Mitteilung gehen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="196"/>
        <source>Cc...</source>
        <translation>CC...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="294"/>
        <location filename="../src/publisher.cpp" line="929"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>Veröffentlichen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="420"/>
        <source>Note started from another application.</source>
        <translation>Mitteilung wurde von einer anderen Anwendung angefangen.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="424"/>
        <source>Ignoring new note request from another application.</source>
        <translation>Ignoriere Anfrage für neue Mitteilung durch eine andere Anwendung.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="951"/>
        <source>Attachment upload was cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1213"/>
        <source>Editing post.</source>
        <translatorcomment>&quot;Edit post&quot;</translatorcomment>
        <translation type="unfinished">Bearbeite Beitrag.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1569"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>Wollen Sie den Beitrag öffentlich, anstatt nur für Ihre &apos;Anhängerschaft&apos; machen?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1572"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;Ja, öffentlich machen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1573"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;Nein, nur an &apos;Anhängerschaft&apos; veröffentlichen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="595"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>Das Format der Bilddatei wird nicht erkannt.
Vielleicht ist deren Erweiterung falsch, bspw. eine GIF-Datei die nach &apos;image.jpg&apos; umbenannt wurde o.ä.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="601"/>
        <source>Select one audio file</source>
        <translation>Wählen Sie eine Audiodatei aus</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="602"/>
        <source>Audio files</source>
        <translation>Audiodateien</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="605"/>
        <source>Invalid audio file</source>
        <translation>Ungültige Audiodatei</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="606"/>
        <source>The audio format cannot be detected.</source>
        <translation>Das Format der Audiodatei wird nicht erkannt.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="610"/>
        <source>Select one video file</source>
        <translation>Wählen Sie _eine_ Videodatei aus</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="611"/>
        <source>Video files</source>
        <translation>Videodateien</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="614"/>
        <source>Invalid video file</source>
        <translation>Ungültige Videodatei</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="615"/>
        <source>The video format cannot be detected.</source>
        <translation>Das Format der Videodatei wird nicht erkannt.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1594"/>
        <source>Posting...</source>
        <translation>Veröffentliche...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="80"/>
        <source>Add a brief title for the post here (recommended)</source>
        <translatorcomment>(Jan) This one needs to be updated with the &quot;here&quot;</translatorcomment>
        <translation>Fügen Sie dem Beitrag hier einen kurzen Titel hinzu (empfohlen)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="188"/>
        <source>Select who will see this post</source>
        <translation>Auswählen, wer diesen Beitrags sehen soll</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="229"/>
        <source>Picture</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="236"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="243"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="266"/>
        <source>Ad&amp;d...</source>
        <translation>Hin&amp;zufügen...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="269"/>
        <source>Upload media, like pictures or videos</source>
        <translation>Medien, wie Bilder oder Videos, hochladen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="297"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>Drücken Sie Strg+Eingabe, um mit der Tastatut zu veröffentlichen</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="669"/>
        <source>Creating person list...</source>
        <translation>Erstelle Personenliste...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="692"/>
        <source>Deleting person list...</source>
        <translation>Lösche Personenliste...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="707"/>
        <source>Getting a person list...</source>
        <translation>Rufe eine Personenliste ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="751"/>
        <source>Adding person to list...</source>
        <translation>Füge Person zu Liste hinzu...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="792"/>
        <source>Removing person from list...</source>
        <translation>Entferne Person von Liste...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="840"/>
        <source>Creating group...</source>
        <translation>Erstelle Gruppe...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="870"/>
        <source>Joining group...</source>
        <translation>Trete Gruppe bei...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="895"/>
        <source>Leaving group...</source>
        <translation>Verlasse Gruppe...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="910"/>
        <source>Getting likes...</source>
        <translation>Rufe Favorisierungen ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="937"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation>Die Kommentare für diesen Beitrag können nicht geladen werden, da die Daten auf dem Server fehlen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="942"/>
        <source>Getting comments...</source>
        <translation>Rufe Kommentare ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1053"/>
        <source>Activity</source>
        <translation>Aktivität</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1057"/>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1062"/>
        <source>Meanwhile</source>
        <translation>Inzwischen</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1066"/>
        <source>Mentions</source>
        <translation>Erwähnungen</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1070"/>
        <source>Actions</source>
        <translation>Aktionen</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="998"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Rufe &apos;%1&apos; ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1045"/>
        <source>Timeline</source>
        <translation>Zeitleiste</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1049"/>
        <source>Messages</source>
        <translation>Nachrichten</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1207"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>Lade %1 hoch</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1505"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>HTTP-Fehler</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1517"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>Gateway-Zeitüberschreitung</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1527"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>Dienst nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1556"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translatorcomment>*** this was added temporarily by Jan, due to last minute addition of the string, might be wrong</translatorcomment>
        <translation>Nicht implementiert</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1566"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>Interner Serverfehler</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1586"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>Verschwunden</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1596"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>Nicht gefunden</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1606"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>Verboten</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1616"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>Nicht autorisiert</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1627"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>Falsche Anfrage</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1646"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>Temporär verschoben</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1656"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>Permanent verschoben</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1667"/>
        <source>Error connecting to %1</source>
        <translation>Fehler beim verbinden mit %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1677"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>Unbehandelter HTTP-Fehler-Code %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1790"/>
        <source>Profile received.</source>
        <translation>Profil empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1792"/>
        <source>Followers</source>
        <translation>Anhängerschaft</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1797"/>
        <source>Following</source>
        <translation>Verfolgte</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1813"/>
        <source>Profile updated.</source>
        <translation>Profil aktualisiert.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1887"/>
        <source>Avatar published successfully.</source>
        <translation>Avatar erfolgreich veröffentlicht.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2006"/>
        <source>1 comment received.</source>
        <translation>1 Kommentar empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2010"/>
        <source>%1 comments received.</source>
        <translation>%1 Kommentare empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2037"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>Beitrag von %1 erfolgreich geteilt.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2192"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Folgen von %1 (%2) erfolgreich.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2202"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Folgen von %1 (%2) erfolgreich beendet.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2252"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>Liste &apos;Verfolgte&apos; vollständig empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2259"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>Liste &apos;Verfolgte&apos; teilweise empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2282"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>Liste &apos;Anhängerschaft&apos; vollständig empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2288"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>Liste &apos;Anhängerschaft&apos; teilweise empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2380"/>
        <source>Person list deleted successfully.</source>
        <translation>Personenliste erfolgreich gelöscht.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2400"/>
        <source>Person list received.</source>
        <translation>Personenliste empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2554"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>Datei erfolgreich hochgeladen. Veröffentliche Beitrag...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2619"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translatorcomment>&quot;(as configured)&quot; </translatorcomment>
        <translation>Lade externe Bilddatei von %1 ohne Rücksicht auf SSL-Fehler (wie konfiguriert)...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2817"/>
        <source>OAuth error while authorizing application.</source>
        <translation>OAuth-Fehler während des Autorisierens der Anwendung.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="252"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>Autorisiert für Konto %1. Rufe einleitende Daten ab.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="257"/>
        <source>There is no authorized account.</source>
        <translation>Es gibt kein autorisiertes Konto.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="549"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation>Rufe Liste &apos;Verfolgte&apos; ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="562"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>Rufe Liste &apos;Anhängerschaft&apos; ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="633"/>
        <source>Getting list of person lists...</source>
        <translation>Rufe Liste der Personenlisten ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1947"/>
        <source>Message liked or unliked successfully.</source>
        <translation>Beitrag erfolgreich (de)favorisiert.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1961"/>
        <source>Likes received.</source>
        <translation>Favorisierungen empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1850"/>
        <source>%1 published successfully. Updating post content...</source>
        <comment>%1 is the type of object: note, image...</comment>
        <translation>%1 erfolgreich veröffentlicht. Aktualisiere Beitragsinhalt...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="362"/>
        <source>Updating profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="581"/>
        <source>Getting site users for %1...</source>
        <comment>%1 is a server name</comment>
        <translation>Rufe Benutzer des Servers %1 ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1075"/>
        <source>User timeline</source>
        <translation>Benutzerzeitleiste</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1465"/>
        <source>Error loading timeline!</source>
        <translation>Fehler beim Laden der Zeitleiste!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1476"/>
        <source>Error loading minor feed!</source>
        <translation>Fehler beim Laden des Nebenfeeds!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1488"/>
        <source>Unable to verify the address!</source>
        <translation>Kann die Adresse nicht überprüfen!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1546"/>
        <source>Bad Gateway</source>
        <comment>HTTP 502 error string</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1787"/>
        <source>Server version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1822"/>
        <source>E-mail updated: %1</source>
        <translation>E-Mail-Adresse aktualisiert: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1866"/>
        <source>Untitled post %1 published successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Unbenannter Beitrag %1 erfolgreich veröffentlicht.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1870"/>
        <source>Post %1 published successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Beitrag %1 erfolgreich veröffentlicht.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1919"/>
        <source>Untitled post %1 updated successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Unbenannter Beitrag %1 erfolgreich aktualisiert.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1923"/>
        <source>Post %1 updated successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Beitrag %1 erfolgreich aktualisiert.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1933"/>
        <source>Comment %1 updated successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Kommentar %1 erfolgreich aktualisiert.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1981"/>
        <source>Comment %1 posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Kommentar %1 erfolgreich veröffentlicht.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2068"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>&apos;%1&apos; wurde empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2081"/>
        <source>Adding items...</source>
        <translation>Füge Elemente hinzu...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2164"/>
        <source>Message deleted successfully.</source>
        <translation>Beiträge erfolgreich gelöscht.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2325"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation>Liste der &apos;Listen&apos; empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2345"/>
        <source>List of %1 users received.</source>
        <comment>%1 is a server name</comment>
        <translation>Liste mit %1 Benutzern empfangen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2369"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>Personenliste &apos;%1&apos; erfolgreich erstellt.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2435"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) erfolgreich zu Liste hinzugefügt.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) erfolgreich von Liste entfernt.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2466"/>
        <source>Group %1 created successfully.</source>
        <translation>Gruppe %1 erfolgreich erstellt.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2477"/>
        <source>Group %1 joined successfully.</source>
        <translation>Gruppe %1 erfolgreich beigetreten.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2490"/>
        <source>Left the %1 group successfully.</source>
        <translation>Gruppe %1 erfolgreich verlassen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2563"/>
        <source>Avatar uploaded.</source>
        <translation>Avatar hochgeladen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2607"/>
        <source>SSL errors in connection to %1!</source>
        <translation>SSL-Fehler bei Verbindung zu %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2648"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>Die Anwendung ist noch nicht bei Ihrem Server registriert. Registriere...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2685"/>
        <source>Getting OAuth token...</source>
        <translation>Rufe Oauth-Token ab...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2706"/>
        <source>OAuth support error</source>
        <translation>OAuth-Support-Fehler</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2707"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>Ihre Installation von QOAuth, einer von Dianara verwendeten Bibliothek, scheint HMAC-SHA1 nicht zu unterstützen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2711"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>Sie müssen wahrscheinlich das OpenSSL-Plugin für QCA installieren: %1, %2 o.ä.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2765"/>
        <location filename="../src/pumpcontroller.cpp" line="2821"/>
        <source>Authorization error</source>
        <translation>Autorisierungsfehler</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2766"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>Ein OAuth-Fehler trat auf, während versucht wurde das Autorisierungs-Token abzurufen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2769"/>
        <source>QOAuth error %1</source>
        <translation>QOAuth-Fehler %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2801"/>
        <source>Application authorized successfully.</source>
        <translation>Anwendung erfolgreich autorisiert.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2844"/>
        <source>Waiting for proxy password...</source>
        <translation>Warte auf Proxy-Passwort...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2873"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>Warte weiterhin auf Profil. Versuche es erneut...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2985"/>
        <source>%1 attempts</source>
        <translation>%1 Versuche</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2989"/>
        <source>1 attempt</source>
        <translation>1 Versuch</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2992"/>
        <source>Some initial data was not received. Restarting initialization...</source>
        <translation>Einige einleitenden Daten wurden nicht empfangen. Beginne erneute Initialisierung...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3410"/>
        <source>Can&apos;t follow %1 at this time.</source>
        <comment>%1 is a user ID</comment>
        <translation>Kann %1 momentan nicht folgen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3414"/>
        <source>Trying to follow %1.</source>
        <comment>%1 is a user ID</comment>
        <translation>Versuche %1 zu folgen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3434"/>
        <source>Checking address %1 before following...</source>
        <translation>Überprüfe Adresse %1 bevor gefolgt wird...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2999"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>Einige einleitenden Daten wurden nach mehreren Versuchen nicht empfangen. Vielleicht stimmt etwas mit Ihrem Server nicht. Dennoch ist es möglich, dass Sie den Dienst normal verwenden können.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3009"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>Alle einleitenden Daten empfangen. Initialisierung abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3022"/>
        <source>Ready.</source>
        <translation>Bereit.</translation>
    </message>
</context>
<context>
    <name>SiteUsersList</name>
    <message>
        <location filename="../src/siteuserslist.cpp" line="30"/>
        <source>You can get a list of the newest users registered on your server by clicking the button below.</source>
        <translation>Sie können eine Liste der zuletzt auf ihrem Server registrierten Benutzer abrufen, indem Sie die Schaltfläche unten anklicken.</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="47"/>
        <source>More resources to find users:</source>
        <translation>Weitere Ressourcen um Benutzer zu finden:</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="51"/>
        <source>Wiki page &apos;Users by language&apos;</source>
        <translation>Wiki-Seite &apos;Benutzer nach Sprache&apos;</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="56"/>
        <source>PPump user search service at inventati.org</source>
        <translation>PPump-Benutzer-Suche auf inventati.org</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="61"/>
        <source>List of Followers for the Pump.io Community account</source>
        <translation>Liste der &apos;Anhängerschaft&apos; des Community-Kontos von Pump.io</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="41"/>
        <source>Get list of users from your server</source>
        <translation>Rufe Liste der Benutzer ihres Servers ab</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="87"/>
        <source>Close list</source>
        <translation>Liste schließen</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="143"/>
        <source>Loading...</source>
        <translation>Lade...</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="154"/>
        <source>%1 users in %2</source>
        <comment>%1 = user count, %2 = server name</comment>
        <translation>%1 Benutzer auf %2</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="76"/>
        <source>Welcome to Dianara</source>
        <translation>Willkommen bei Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="78"/>
        <source>Dianara is a &lt;b&gt;Pump.io&lt;/b&gt; client.</source>
        <translation>Dianara ist ein &lt;b&gt;Pump.io&lt;/b&gt;-Client.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="81"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>Falls Sie noch kein Pump.io-Konto haben, können Sie z.B. bei folgenden Adressen eines bekommen:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="88"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>Drücken Sie &lt;b&gt;F1&lt;/b&gt;, falls Sie das Hilfe-Fenster öffnen wollen.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="91"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>Als Erstes sollten Sie Ihr Konto über das Menü &lt;b&gt;Einstellungen - Konto&lt;/b&gt; einrichten.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="94"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>Nachdem der Vorgang abgeschlossen ist, sollten sich Ihr Profil und Ihre Zeitleisten automatisch aktualisieren.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="98"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>Nehmen sich einen Moment, um sich in den Menüs und im Konfigurationsfenster umzuschauen.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="102"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>Sie können auch Profilinformationen und -bild über das Menü &lt;b&gt;Einstellungen - Profil bearbeiten&lt;/b&gt; einstellen.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="106"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>Es gibt überall Tooltips, sodass Sie beim Überfahren einer Schaltfläche oder eines Eingabefeldes wahrscheinlich zusätzliche Informationen sehen.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="113"/>
        <source>Dianara&apos;s blog</source>
        <translation>Dianaras Blog</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="115"/>
        <source>Pump.io User Guide</source>
        <translation>Pump.io-Leitfaden</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="126"/>
        <source>Direct Messages Timeline</source>
        <translation>Direktnachrichten-Zeitleiste</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="127"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>Hier werden Sie speziell an Sie gerichtete Beiträge sehen.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="137"/>
        <source>Activity Timeline</source>
        <translation>Aktivität-Zeitleiste</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="138"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>Hier werden Sie Ihre eigenen Beiträge sehen.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="147"/>
        <source>Favorites Timeline</source>
        <translation>Favoriten Zeitleiste</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="148"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>Beiträge und Kommentare, die Sie favorisiert haben.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="231"/>
        <source>Newest</source>
        <translation>Neueste</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="240"/>
        <source>Newer</source>
        <translation>Neuer</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="263"/>
        <source>Older</source>
        <translation>Älter</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="335"/>
        <source>Requesting...</source>
        <translation>Abfrage...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="506"/>
        <source>Loading...</source>
        <translation>Lade...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="615"/>
        <source>Page %1 of %2.</source>
        <translation>Seite %1 von %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="619"/>
        <source>Showing %1 posts per page.</source>
        <translation>Zeige %1 Beiträge pro Seite.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="622"/>
        <source>%1 posts in total.</source>
        <translation>%1 Beiträge insgesamt.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="626"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>Klicken Sie hier oder drücken Sie Str+G, um zu einer bestimmten Seite zu springen</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="740"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>&apos;%1&apos; kann nicht aktualisiert werde, da gerade ein Kommentar erstellt wird.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="848"/>
        <source>%1 more posts pending for next update.</source>
        <translation>Noch %1 weitere Beiträge warten auf nächste Aktualisierung.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="854"/>
        <source>Click here to receive them now.</source>
        <translation>Klicken Sie hier um diese jetzt abzurufen.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="1139"/>
        <source>There are no posts</source>
        <translation>Es gibt keine Beiträge</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>Ungültiger Zeitstempel!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>Vor einer Minute</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>Vor %1 Minuten</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>Vor einer Stunde</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>Vor %1 Stunden</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>Gerade jetzt</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>In der Zukunft</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>Gestern</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>Vor %1 Tagen</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>Vor einem Monat</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>Vor %1 Monaten</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>Vor einem Jahr</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>Vor %1 Jahren</translation>
    </message>
</context>
<context>
    <name>UserPosts</name>
    <message>
        <location filename="../src/userposts.cpp" line="39"/>
        <source>Posts by %1</source>
        <translation>Beiträge von %1</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="64"/>
        <source>Loading...</source>
        <translation>Lade...</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="69"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="153"/>
        <source>Received &apos;%1&apos;.</source>
        <translation>&apos;%1&apos; wurde empfangen.</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="160"/>
        <source>%1 posts</source>
        <translation>%1 Beiträge</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="169"/>
        <source>Error loading the timeline</source>
        <translation>Fehler beim Laden der Zeitleiste</translation>
    </message>
</context>
</TS>
